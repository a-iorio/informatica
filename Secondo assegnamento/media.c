/**
  \file media.c
  \author Andrea Iorio
  - Si dichiara che il contenuto di questo file e' in ogni sua parte opera originale dell' autore.
  \date 8 Maggio 2015
  \brief File contenente le funzioni richieste dall'assegnamento.
*/
#include <limits.h>
#include <math.h>

/**
  \brief Calcola media e dev. std. dei dati sul file (solo interi accettati).

  Le espressioni usate sono \f$ m=\frac{1}{N} \sum_{i=0}^N x_i \f$ e \f$ s^2 = \frac{1}{N-1} \sum_{i=1}^N (x_i-m)^2 \f$. 
*/
int media_dev(FILE* f, double* pmedia, double* pdev)
{
    int n=0; 
    long int i, sum_m=0; 
    double sum_d=0;

    /* Leggo i dati dal file, ne faccio la somma e aggiorno il contatore */
    while(fscanf(f, "%ld\n", &i)==1){
        sum_m+=i;
        n++;
    }

    *pmedia = (double) sum_m/n;

    rewind(f);

    while(fscanf(f, "%ld\n", &i)==1){ /* Rileggo i dati per calcolare il termine della dev std */
         sum_d+=pow((i-*pmedia),2);
    }

    /* Riempio i puntatori con i valori ricevuti */
    *pdev = (double) sqrt(sum_d/(n-1));

    /* Controllo che i puntatori siano correttamente riempiti e ritorno la funzione. */
    if(pmedia && pdev) return n;
    else return -1;
}

/**
  \brief Calcola in altro modo media e dev. std. dei dati sul file (solo interi accettati).

  Le espressioni usate sono \f$ m=\frac{1}{N} \sum_{i=0}^N x_i \f$ e \f$ s^2 = \frac{1}{N-1} \left[ \sum_{i=1}^N x_i^2 - \frac{1}{N}\left( \sum_{i=1}^N x_i \right )^2 \right ] \f$.
*/
int media_dev2(FILE* f, double* pmedia, double* pdev)
{
    int n=0;
    long int i, sum_m=0, sum_d=0;
    
    /* Leggo i dati dal file. Inoltre verifico che l'aggiunta del prossimo dato non ecceda il valore massimo di long int impostato. */
    while(fscanf(f, "%ld\n", &i)==1){
      if((sum_m > LONG_MAX -i) || (sum_d > LONG_MAX-pow(i,2))) return -1;
      else{
        sum_m+=i;
        sum_d+=pow(i,2);
        n++;
      }
    }

    /* Riempio i puntatori con i valori ricevuti usando la 2da espressione per la dev. std. */
    *pmedia = sum_m/(double)n;
    *pdev = (double) sqrt((sum_d-pow(sum_m,2)/n)/(n-1));

    /* Controllo che i puntatori siano correttamente riempiti e ritorno la funzione. */
    if(pmedia && pdev) return n;
    else return -1;
}

/**  
  \brief Calcola il massimo e il minimo dei dati sul file (tipo double).
*/
int max_min(FILE* f,double* pmax, double* pmin)
{
    typedef enum{FALSE, TRUE} bool;
    bool set=FALSE;
    int n=0;
    double i, max=0, min=0;

    /* Leggo i dati dal file. */
    while(fscanf(f, "%lf\n", &i)==1){
      /* Se la variabile 'set' non e' impostata, riempio le variabili 'max' e 'min' con il primo valore letto */
      /* Altrimenti controllo se il valore immesso sia > o < del max/min e aggiorno */
      if(!set){ max=min=i; set=TRUE; }
      else{
        if(i>max) max=i;
        if(i<min) min=i;
      }
      n++;
    }

    /* Riempio i puntatori */
    *pmax = max;
    *pmin = min;

    /* Faccio un'ultima verifica di sicurezza e ritorno la funzione. */
    if(pmax && pmin && *pmax>=*pmin) return n;
    else return -1;
}

/**  
  \brief Calcola media e dev. std. dei dati sul file (tipo double).

  Algoritmo analogo alla funzione media_dev() ma con variabili di tipo double.
*/
int media_dev_f(FILE* f, double* pmedia, double* pdev)
{
    int n=0;
    double i, sum_m=0, sum_d=0;

    while(fscanf(f, "%lf\n", &i)==1){
      sum_m+=i;
      sum_d+=pow(i,2);
      n++;
    }

    *pmedia = sum_m/(double)n;
    *pdev = (double) sqrt((sum_d-pow(sum_m,2)/n)/(n-1));

    if(pmedia && pdev) return n;
    else return -1; 
}

/**  
  \brief Calcola media \a trimmed \a e la relativa dev. std. dei dati sul file (tipo double).
  Vengono scartati i k valori piu' grandi e piu' piccoli nel calcolo di media e dev. std. 
  Algoritmo utilizzato:
  - Si leggono un prima volta i dati riempendo due array di k elementi con i valori piu' grandi e piu' piccoli letti. 
  - Successivamente si rileggono i dati e si saltano quelli presenti nell'array nel calcolo da effettuare. 
*/
int media_dev_filter_f(FILE* f, double* pmedia, double* pdev, int k)
{
    typedef enum {FALSE, TRUE} bool;
    bool in;
    int n=0, z=0, o, temp_min, temp_max;
    double i, sum_m=0, sum_d=0;
    double del_min[k], del_max[k];

    /* Leggo i dati una prima volta */
    while(fscanf(f, "%lf\n", &i)==1){

      /* Se gli array non sono ancora stati inizializzati, li si inzializza con i primi k valori.  */
      if(z<k){
        del_min[z]=del_max[z]=i;
        z++;
      }
      /* Altrimenti si procede alla verifica che il dato in ingresso non sia maggiore o minore del massimo o del minimo valore dell'array. */
      else{
        /* Trovo max e min dell'array corrente */
        temp_min=temp_max=0;
        for(o=0;o<k;o++){
          if(del_min[temp_min]<del_min[o]) temp_min=o; 
          if(del_max[temp_max]>del_max[o]) temp_max=o; 
        }
        /* Eseguo il controllo e nel caso aggiorno l'array */
        if(i<del_min[temp_min]) del_min[temp_min] = i;
        if(i>del_max[temp_max]) del_max[temp_max] = i;
      }      
    }

    rewind(f);

    /* Rileggo i dati: se il dato e' nell'array, imposto la variabile 'in' su TRUE e lo salto; altrimenti procedo con le somme. */
    while(fscanf(f, "%lf\n", &i)==1){
      in = FALSE;

      for(o=0; o<k; o++){ if(i==del_min[o] || i==del_max[o]){ in = TRUE; } }

      if(!in){
        sum_m+=i;
        sum_d+=pow(i,2);
        n++;
      }
    }

    /* Riempio i puntatori e ritorno la funzione */
    *pmedia = sum_m/(double)n;
    *pdev = (double) sqrt((sum_d-pow(sum_m,2)/n)/(n-1));
    
    if(pmedia && pdev) return n;
    else return -1; 
}