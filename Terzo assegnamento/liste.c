/**
  \file liste.c
  \author Andrea Iorio 505520
  - Si dichiara che il contenuto di questo file e' in ogni sua parte opera originale dell' autore.
  \date 2 Giugno 2015
  \brief File contenente le funzioni richieste dall'assegnamento.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "liste.h"


/* INSCERISCI LISTA */

/** 
  Funzione che inserisce un nuovo elemento con la stringa s prima dell'elemento passato. 
*/

int inserisci_elemento (elem_t ** elem, char *s){

  elem_t * aux;

  if ((aux = malloc(sizeof(elem_t))) == NULL) return -1;

  strcpy(aux->nome, s);
  aux->next = *elem;
  *elem = aux;

  return 0;
}

/** 
  Funzione ricorsiva di appoggio per l'inserimento di un elemento in una lista ordinata.
  Se elem e' alfabeticamente dopo l'elemento da inserire, inserisce l'elemento (prima di elem);
  altrimenti richiama la funzione sull'elemento successivo della lista.
*/

int inscerisci_lista_r (elem_t ** elem, char *s){

  int err;

  if ((*elem == NULL) || strcmp((*elem)->nome, s)>=0) err = inserisci_elemento(elem, s);
  else err = inscerisci_lista_r(&((*elem)->next), s);
  
  return err;
}

/**
  Se la lista e' vuota inserisco l'elemento in testa.
  Altrimenti se la lista non e' ordinata, inserisco l'elemento in testa; se e' ordinata, uso la funzione ricorsiva di appoggio.
*/

int inserisci_lista (lista_t * l , char * s){

  int err;
  
  if (l == NULL) err = inserisci_elemento(&(l->head), s);

  else{
    if (!(l->ord)) err = inserisci_elemento(&(l->head), s);
    else err = inscerisci_lista_r(&(l->head), s);
  }

  return err;
}

/* END */




/* SET NON ORDINATA */

void set_nonordinata (lista_t * l){

  if (l->ord) l->ord = false;
  return;

}

/* END */




/* SET ORDINATA */

/** 
  Per ordinare la lista, creiamo una nuova lista di appoggio in cui inseriamo gli elementi (in modo ordinato) contenuti nella lista da ordinare.
  Dopo di che facciamo puntare la lista originale alla nuova lista.
*/

void set_ordinata (lista_t * l){

  if (!(l->ord)){

    int err;
    lista_t * aux;

    aux = new_lista();
    l->ord = true;
    aux->ord = true;
    
    while (l->head != NULL){
      err = inserisci_lista(aux, l->head->nome);
      if (err != -1) l->head = l->head->next;
    }

    l->head = aux->head;

  }

  return;
}

/* END */




/* FREE LISTA */

/** 
  Procedura ricorsiva che dealloca tutti gli elementi in una lista. 
*/

void free_elem (elem_t * elem){

  if (elem == NULL) return;

  free_elem(elem->next);
  free(elem);

}

/** 
  Se la lista non e' vuota, dealloco tutti gli elementi, dealloco la lista e imposto il puntatore su NULL. 
*/

void free_lista (lista_t ** pl){

  if (*pl == NULL) return;

  free_elem((*pl)->head);
  free(*pl);

  *pl=NULL;

  return;
}

/* END **/




/* CANCELLA ELEMENTO DA LISTA */

/**
  Procedura che dealloca l'elemento elem dalla lista. 
*/

void cancella_elem(elem_t ** elem){

  elem_t * aux;

  if (*elem != NULL){
    aux = *elem;
    *elem = (*elem)->next;
    free(aux);
  }

  return;
}

/**
  Procedura ricorsiva di appoggio che controlla quali elementi della lista siano riempiti con la stringa s e li cancella.
*/

void cancella_lista_ric (elem_t ** elem, char *s){

  if (*elem != NULL){

    if (strcmp((*elem)->nome, s) == 0){ 
      cancella_elem(elem);
      cancella_lista_ric(elem, s);
    }
    else cancella_lista_ric(&((*elem)->next), s);
  
  }

  return;
}


void cancella_lista (lista_t * l, char * s){

  if (l != NULL) cancella_lista_ric(&(l->head), s);
  return;

}

/* END */




/* CANCELLA INTERVALLO LISTA */

/**
  Procedura ricorsiva che controlla se l'elemento corrente e' contenuto nell'intervallo [s1, s2].
  Se non e' contenuto passo all'elemento successivo, altrimenti li elimino finche' questi sono in [s1, s2]. 
*/

void cancellaintervallo_lista_ric (elem_t ** elem, char *s1, char *s2){
  
  if (*elem != NULL){

    if (strcmp((*elem)->nome, s1)>=0 && strcmp((*elem)->nome, s2)<=0){ 
      cancellaintervallo_lista_ric(&((*elem)->next), s1, s2);
      cancella_elem(elem);
    }
    else cancellaintervallo_lista_ric(&((*elem)->next), s1, s2);

  }

  return;
}

void cancellaintervallo_lista (lista_t * l, char * s1, char* s2){

  if (l != NULL) cancellaintervallo_lista_ric(&(l->head), s1, s2);
  return;

}

/* END */

/** Dichiarazione delle ulteriori funzioni utilizzate. */

void free_elem(elem_t * elem);
int inserisci_elemento(elem_t ** elem, char *s);
int inscerisci_lista_r(elem_t ** elem, char *s);
void cancella_elem(elem_t ** elem);
void cancella_lista_ric (elem_t ** elem, char *s);
void cancellaintervallo_lista_ric (elem_t ** elem, char *s1, char *s2);
