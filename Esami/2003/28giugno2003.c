/*

#include <stdio.h>

typedef enum {false, true} boolean;

boolean check(int *vet, int dim);

int main(){
	int vet[10]={4,2,8,3,4,4,2,8,2,8};

	printf("%d\n", check(vet, 10));

	return 0;
}

boolean check(int *vet, int dim){
	boolean trovato=false, ok=true;
	int i=0;
	while(!trovato && i<dim){
		if(vet[i]%2 != 0) trovato = true;
		i++;
	}
	while(ok && i<dim){
		if(vet[i]%2!=0) ok=false;
		i++;
	}
	return ok;
}


#include <stdio.h>

int foo (int *vet, int from, int to);

int main(){
	int vet[5]={1,2,3,4,5};

	printf("%d\n", foo(vet,1,4));
	return 0;
}

int foo (int *vet, int from, int to){
	if(from>=to) return 0;
	else return 2*vet[to-1]+foo(vet, from, to-1);
}

*/
#include <stdio.h>
#include <stdlib.h>

typedef struct El{
	int s; 
	struct El *next;
} ElementoLista;
typedef ElementoLista *ListaDiInteri;

void InsertAfterMax(ListaDiInteri *lista, int el);

int main(){
	ListaDiInteri lista=NULL, aux;
	int i;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->s = (-1)*i;
		aux->next = lista;
		lista = aux;
	}
	InsertAfterMax(&lista, 7);
	while(lista!=NULL){
		printf("%d\n", lista->s);
		lista = lista->next;
	}
}

void InsertAfterMax (ListaDiInteri *lis, int el) {
    ListaDiInteri aux, pmax;

    if(*lis==NULL){
		aux = malloc(sizeof(ElementoLista));
		aux->s = el;
		aux->next = *lis;
		*lis = aux;
    } 

    else{
		pmax = *lis;
		aux = (*lis)->next;

		while(aux != NULL){
		    if (aux->s > pmax -> s) pmax = aux;
		    aux = aux->next;
		}

		aux = malloc(sizeof(ElementoLista));
		aux->s = el;
		aux->next = *lis;
		*lis = aux;
	}

}
