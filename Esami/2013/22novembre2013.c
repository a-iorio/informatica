/*
#include <stdio.h>
#include <string.h>
#include <ctype.h>

typedef enum{false, true} boolean;

int main(){
	boolean ok=true;
	char ch;
	int n, i=0;

	while(ok){
		printf("Inserisci %d sequenza:\n", i+1);
		
		scanf(" %c", &ch);
		scanf("%d", &n);

		if((isalpha(ch) && n>0) || (isdigit(ch) && n==0) || (!isdigit(ch) && !isalpha(ch) && n<0)) i++;
		else ok=false;
	}

	printf("Sequenze lette correttamente: %d\n", i);

	return 0;
}


#include <stdio.h>
#define N 50
void intersezione(int a[], int b[], int c[], int dima, int dimb);

int main(){
	int a[5] = {1,2,3,4,5}, b[3] = {1,2,3};
	int c[N];
	intersezione(a,b,c,5,3);
	printf("%d", c[2]);
}

void intersezione(int a[], int b[], int c[], int dima, int dimb){	
	int i=0, n=0, j=0, dimc;
	
	if(dima>dimb){
		dimc=dimb;
		for(i=0; i<dima; i++){
			for(j=0; j<dimb; j++){
				if(a[i]==b[j]){ c[n]=a[i]; n++;}
			}
		}
	}
	
	else intersezione(b, a, c, dimb, dima);

}

#include <stdio.h>
typedef enum{false, true} boolean;

boolean sommadopom(int vet[], int dim, int m, int n);

int main(){
	int arr[9]={3,0,7,4,8,4,1,8,5};
	printf("%d", sommadopom(arr, 9, 8, 18));
	return 0;
}
	
boolean sommadopom(int vet[], int dim, int m, int n){
	int i=0, som=0;
	if(dim==0) return false;
	else{
		if(vet[0]==m){
			for(i=1; i<dim; i++){
				som=som+vet[i];
			}
			if(n==som) return true;
			else return false;
		}

		else return sommadopom(vet+1, dim-1, m, n);
	}
}
*/

#include <stdio.h>
#include <stdlib.h>

typedef enum{M, L, D, I} tipo;

typedef struct impiegato{
	int codice;
	int punteggio;
	tipo titolo;
	int eta;
	struct impiegato * next;
} Impiegato;

typedef Impiegato * ListaImpiegati;

float etamedia(ListaImpiegati lista);
void inserzioneordinata(ListaImpiegati *lista, int codice, int punteggio, tipo titolo, int eta);
void cancellazione(ListaImpiegati *lista, int codice);
ListaImpiegati Assunzione(ListaImpiegati *lista, int k);

int main(){
	int i=0;
	ListaImpiegati lista=NULL, aux, assunti;
	
	for(i=1; i<6; i++){
		aux = malloc(sizeof(Impiegato));
		aux->codice=1000+i;
		aux->punteggio=10*i;
		aux->titolo = M;
		aux->eta = 11*i;
		aux->next = lista;
		lista=aux;
	}

	printf("%f\n", etamedia(lista));

	inserzioneordinata(&lista, 1008, 99, M, 44);

	cancellazione(&lista, 1001);
	assunti = Assunzione(&lista, 3);


	while(assunti!=NULL){
		printf("Codice: %d, Punteggio %d, Titolo %u, Eta %d\n", assunti->codice, assunti->punteggio, assunti->titolo, assunti->eta);
		assunti=assunti->next;
	}	
	return 0;
}

float etamedia(ListaImpiegati lista){
	int sum=0;
	int n=0;

	while(lista!=NULL){
		sum = sum+lista->eta;
		n++;
		lista=lista->next;
	}

	return sum/(float)n;
}

void inserzioneordinata(ListaImpiegati *lista, int codice, int punteggio, tipo titolo, int eta){
	ListaImpiegati aux, nuovo;
	if(((*lista)->punteggio < punteggio) || ((*lista)->punteggio == punteggio && (*lista)->eta < eta)){
		nuovo = malloc(sizeof(Impiegato));
		nuovo->codice = codice;
		nuovo->punteggio = punteggio;
		nuovo->titolo = titolo;
		nuovo->eta = eta;
		aux = *lista;
		nuovo->next = aux;
		*lista = nuovo;
	}

	else inserzioneordinata(&((*lista)->next), codice, punteggio, titolo, eta);
}

void cancellazione(ListaImpiegati *lista, int codice){
	ListaImpiegati aux;

	if(lista==NULL){
		return;
	}
	else{
		if(*lista==NULL) return;
		else{
			if((*lista)->codice == codice){
				aux = *lista;
				*lista = (*lista)->next;
				free(aux);
				return;
			}

			else cancellazione(&(*lista)->next, codice);
		}
	}
}

ListaImpiegati Assunzione(ListaImpiegati *lista, int k){
	ListaImpiegati assunti=NULL, aux;

	while(*lista!=NULL || k>0){
		aux = malloc(sizeof(Impiegato));
		aux = assunti;
		assunti = *lista;
		assunti->next = aux;
		*lista = (*lista)->next;
		k--;
	}

	return assunti;

}