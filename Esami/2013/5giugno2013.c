/*
#include <stdio.h>
typedef enum{false, true} boolean;

int main(){
	boolean ok;	
	int i=0;
	char ch, prec1, prec2;

	do{
		scanf(" %c", &ch);

		if(ch>='a' && ch<='z'){
			scanf(" %c", &prec1);
			if(prec1<='A' || prec1 >= 'Z') ok = false;
			else{
				scanf(" %c", &prec2);
				if(prec2<='A' || prec2 >= 'Z') ok = false;
				else i++;
			}
		}

	}
	while(ok);
	
	printf("Sequenze lette: %d\n", i);
	return 0;
}


#include <stdio.h>
typedef enum{false, true} boolean;

boolean maggiore(int vet1[], int dim1, int vet2[], int dim2);

int main(){
	int vet1[5]={6,2,8,9,10}, vet2[5]={1,2,3,4,5};

	printf("%d", maggiore(vet1, 5, vet2, 5));
	return 0;
}

boolean maggiore(int vet1[], int dim1, int vet2[], int dim2){
	int i=0, j=0;
	boolean trovato = false, ok=true;

	while(i<dim1 && ok){
		trovato = false;
		for(j=0; j<dim2; j++){
			if(vet1[i]>vet2[j] && !trovato) trovato=true;
		}
		if(!trovato) ok = false;
		else i++;
	}

	return ok;
}


#include <stdio.h>
typedef enum{false, true} boolean;

boolean mediani(int vet[], int dim);

int main(){
	int vet[7]={3,5,2,5,3,0,2};

	printf("%d\n", mediani(vet, 7));
	return 0;
}

boolean mediani(int vet[], int dim){
	int med;

	dim--;
	if(dim==0) return true;
	else{
		if(vet[0]+vet[dim] == vet[dim/2]) return mediani(vet+1,dim-1);
		else return false;
	}

}

*/

#include <stdio.h>
#include <stdlib.h>

typedef struct paziente{
	int codice;
	int urgenza;
	int tempo;
	int eta;
	struct paziente* next;
} Paziente;

typedef Paziente* ListaPazienti;

float etamedia(ListaPazienti lista);

void inserzioneordinata(ListaPazienti *lista, int codice, int urgenza, int tempo, int eta);

int pazienteoperabile(ListaPazienti *lista, int tempomax);

int main(){
	int i=0;
	ListaPazienti lista=NULL, aux;
	
	for(i=1; i<6; i++){
		aux = malloc(sizeof(Paziente));
		aux->codice=1000+i;
		aux->urgenza=10*i;
		aux->tempo = i*100;
		aux->eta = 11*i;
		aux->next = lista;
		lista=aux;
	}


	printf("%f\n", etamedia(lista));
	inserzioneordinata(&lista, 500, 23, 32, 33);
	printf("%d\n", pazienteoperabile(&lista, 350));


	while(lista!=NULL){
		printf("Codice: %d Urgenza: %d Tempo: %d Eta: %d\n", lista->codice, lista->urgenza, lista->tempo, lista->eta);
		lista=lista->next;
	}
	return 0;
}

float etamedia(ListaPazienti lista){

	int n=0, sum=0;

	while(lista!=NULL){
		sum=sum+lista->eta;
		n++;
		lista=lista->next;
	}

	return sum/(float)n;

}

void inserzioneordinata(ListaPazienti *lista, int codice, int urgenza, int tempo, int eta){
	ListaPazienti aux, nuovo;

	if(*lista == NULL || ((*lista)->urgenza < urgenza) || ((*lista)->urgenza == urgenza && (*lista)->eta < eta)){
		nuovo = malloc(sizeof(Paziente));
		nuovo->codice = codice;
		nuovo->urgenza = urgenza;
		nuovo->tempo = tempo;
		nuovo->eta = eta;
		aux = *lista;
		nuovo->next = aux;
		*lista = nuovo;
	}

	else inserzioneordinata(&((*lista)->next), codice, urgenza, tempo, eta);
}

int pazienteoperabile(ListaPazienti *lista, int tempomax){
	ListaPazienti aux;
	int codice;

	if(*lista==NULL) return 0;
	else{
		if((*lista)->tempo <= tempomax){
			codice = (*lista)->codice;
			aux = *lista;
			*lista = (*lista)->next;
			free(aux);		
			return codice;		
		}

		else return pazienteoperabile(&((*lista)->next), tempomax);
	}

}