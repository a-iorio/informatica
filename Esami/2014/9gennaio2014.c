/*
#include <stdio.h>
#define N 9

void stampa(int arr[], int dim);

int main(){
	int array[N]={8,9,-6,7,-1,-6,-3,6,-9};

	stampa(array, N);
	return 0;
}

void stampa(int arr[], int dim){
	if(dim!=0){
		stampa(arr, dim-1);
		if(arr[dim-1]>0) printf("%d ", arr[dim-1]);
		else{
			if(arr[dim-2]>0) printf("%d ", arr[dim-1]);
		}
		
	}

}

#include <stdio.h>
typedef enum{giallo, verde, blu} colore;
int numero_gialli(int num, colore col);

int main(){
	colore col_m=blu;

	printf("%d\n", numero_gialli(8, col_m));
	return 0;
}

int numero_gialli(int num, colore col){
	int tot=0, gap=0;
	if(col==giallo) gap = 0;
	if(col==verde) gap = 2;
	if(col==blu) gap = 1;
	num=num-gap;

	while(num>0){
		tot=tot+num;
		num = num-3;
	}

	return tot;
}

#include <stdio.h>
typedef enum{false, true} boolean;

int main(){
	boolean occ=false, ok=true;

	char c, nuovo;
	int i=0;

	printf("Inserisci il carattere: \n");
	c = getchar();
	getchar();

	printf("Ora inserisci la serie di caratteri: \n");
	while(ok){
		nuovo = getchar();
		getchar();

		if(nuovo==c){
			if(occ) i++;
			else occ=true;
		}
		else{
			if(occ){
				if(nuovo >= 'a' && nuovo <= 'z'){ i++; occ=false; }
				else ok=false; 
			}
		}
	}

	printf("Occorrenze trovate: %d\n", i);

	return 0;
}
*/


#include <stdio.h>
#include <stdlib.h>

typedef enum{false, true} boolean;

typedef struct posto_s{
	boolean occupato;
	struct posto_s* next;
} Posto;

typedef Posto* posto;

typedef struct fila_s{
	posto sottofila;
	int numero_posti;
	struct fila_s* next;	
} Fila;

typedef Fila* fila;

boolean postiliberi(fila lista);

posto primopostolibero(fila lista, int n);

void eliminafile(fila *lista);

int main(){
	int i;
	posto posto=NULL, aux1;
	fila cinema=NULL, aux2;


	for(i=0; i<3;i++){
		aux1 = malloc(sizeof(Posto));
		aux1->occupato = true;
		aux1->next = posto;
		posto = aux1;
	}

	for(i=1; i<4;i++){
		aux2 = malloc(sizeof(Fila));
		aux2->numero_posti = 3;
		aux2->sottofila = posto;
		aux2->next = cinema;
		cinema=aux2;		
	}

	i=1;

	while(cinema!=NULL){
		printf("\nFila: %d: Numero posti liberi: %d\n",  i, cinema->numero_posti);
		while(cinema->sottofila!=NULL){
			printf("-> Sottofila: %u\n", cinema->sottofila->occupato);
			cinema->sottofila=cinema->sottofila->next;
		}
		i++;
		cinema=cinema->next;
	}

	return 0;

}

boolean postiliberi(fila lista){
	int n=0, i=0;
	posto head;
	n=lista->numero_posti;

	head = lista->sottofila;
	while(lista->sottofila!=NULL){
		if(!(lista->sottofila->occupato)) i++;
		lista->sottofila=lista->sottofila->next;
	}
	lista->sottofila=head;


	if(i==n) return true;
	else return false;
	
}


posto primopostolibero(fila lista, int n){
	int i=0; 
	boolean trovato=false;
	posto ret=NULL;



	return ret;

}

void eliminafile(fila *lista){
	posto aux;
	fila aux2;

	if(*lista==NULL) return;

	else{
		if((*lista)->numero_posti==0){

			while((*lista)->sottofila!=NULL){
				aux=(*lista)->sottofila;
				(*lista)->sottofila = (*lista)->sottofila->next;
				free(aux);
			}

			aux2 = (*lista);
			*lista = (*lista)->next;
			free(aux2);

			eliminafile(lista);
		}
		else{
			eliminafile(&(*lista)->next);

		}

	}
}



