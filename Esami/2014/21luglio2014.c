
#include <stdio.h>

int contadec(int vet[], int dim);

int main(){
	int a[11]={2,8,7,5,8,10,9,10,5,4,3};

	printf("%d\n", contadec(a,11));
	return 0;
}

int contadec(int vet[], int dim){
	if(dim==1) return 0;
	else{
		if(dim==2){
			if(vet[0]>vet[1]) return 2+contadec(vet+1, dim-1);
		}
		if(dim>=3){
			if(vet[0]>vet[1] && vet[1]>vet[2]) return 1+contadec(vet+1, dim-1);
			else if(vet[0]>vet[1] && vet[1]<vet[2]) return 2+contadec(vet+1, dim-1);
			else return contadec(vet+1, dim-1);
		}
	}
}