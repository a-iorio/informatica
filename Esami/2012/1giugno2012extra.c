#include <stdio.h>
typedef enum{false, true} boolean;

int main(){
	boolean ok=true, ord=true;
	int pari=0, dispari=0, prec, cor;

	printf("Inserisci numero:\n");
	scanf("%d", &cor);

	if(cor>0){

		if(cor%2==0) pari++;
		else dispari++;

		while(ok){
			prec = cor;
			printf("Inserisci numero:\n");
			scanf("%d", &cor);
			if(cor<0 || cor==prec) ok=false;
			else{
				if(cor%2==0) pari++;
				else dispari++;
				if(cor>=prec && ord) ord=true;
				else ord=false;
			}
		}

		if(pari!=dispari && !ord) printf("0");
		if(pari==dispari && !ord) printf("1");
		if(pari!=dispari && ord) printf("2");
		if(pari==dispari && ord) printf("3");
	
	}
	else{
		printf("Errore\n");
	}
	return 0;
}

/*
#include <stdio.h>

int minimomaggioranti(int vet[], int dim);


int main(){
	int arr[5] = {1,3,5,4,2};

	printf("%d\n", minimomaggioranti(arr, 5));

	return 0;
}

int minimomaggioranti(int vet[], int dim){
	
	int res, act = vet[0]+1;
	if(dim==1) return vet[0]+1;
	
	else{
		res = minimomaggioranti(vet+1, dim-1);
		if(res>act) return res;
		else return act;
	}
	
}


#include <stdio.h>
#include <stdlib.h>

typedef enum{false, true} boolean;
typedef enum{Quadri, Cuori, Picche, Fiori} Seme;

typedef struct carta{
	int valore;
	Seme seme;
} Carta;

typedef struct nodo{
	Carta carta;
	int  carteSuccessive;
	struct nodo* next;
} NodoMazzo;

typedef NodoMazzo* Mazzo;


boolean check(Mazzo mazzo, int valore, Seme seme);

void inserisci(Mazzo *mazzo, int valore, Seme seme);

boolean cancella(Mazzo *mazzo, int valore, Seme seme);

int main(){

	Mazzo mazzo=NULL;
	
	return 0;
}

boolean check(Mazzo mazzo, int valore, Seme seme){

	
	if(mazzo==NULL) return false;
	else{
	
		if(mazzo->carta.seme == seme){
			if(mazzo->carta.valore==valore) return true;
			else{
				if(mazzo->carteSuccessive>0) return check(mazzo->next, valore, seme);
				else return false;	
			}
		}
		else return check(mazzo->next, valore, seme);
		
	}

}

void inserisci(Mazzo *mazzo, int valore, Seme seme){

	Mazzo nuova, aux; 
	
	nuova = malloc(sizeof(NodoMazzo));
	nuova->carta.valore = valore;
	nuova->carta.seme = seme;
	nuova->carteSuccessive = 0;

	if (*mazzo == NULL){
		nuova->next = NULL;
		*mazzo = nuova;
	}

	else if ((*mazzo)->next == NULL){
		(*mazzo)->next = nuova;
		nuova->next = NULL;
	}

	else{
		if( (*mazzo)->carta.seme == seme ){
			nuova->carteSuccessive = (*mazzo)->carteSuccessive+1;
			nuova->next = *mazzo;
			(*mazzo) = nuova;
		}

		else inserisci(&((*mazzo)->next), valore, seme);

	}
}

boolean cancella(Mazzo *mazzo, int valore, Seme seme){
	Mazzo aux;
	if(mazzo==NULL || *mazzo == NULL) return false;
	else{
		if((*mazzo)->carta.seme == seme){
			if((*mazzo)->carta.valore == valore){
				aux = *mazzo;
				*mazzo = (*mazzo)->next;
				free(aux);
				return true;
			}
			else{
				if(cancella(&(*mazzo)->next, valore, seme)){ (*mazzo)->carteSuccessive--; return true;}
			
				else return cancella(&(*mazzo)->next, valore, seme);
			}
			
		}
		else{
			return cancella(&(*mazzo)->next, valore, seme);
		}

	}
}

*/