/*
#include <stdio.h>
#define N 5
typedef enum{false, true} boolean;

boolean trova(int arr[], int dim, int k);
boolean trova_eff(int arr[], int dim, int k);

int main(){
	int vet[N]={2,3,6,8,10};
	
	if(trova(vet, N, 11)== true) printf("Trovato\n");
	else printf("Non trovato\n");

	if(trova_eff(vet, N, 11)== true) printf("Trovato\n");
	else printf("Non trovato\n");

	return 0;
}

boolean trova(int arr[], int dim, int k){
	int i=0, j;
	boolean found=false;

	while(i<dim && !found){
		j=i+1;

		while(j<dim && !found){
			if(k==arr[i]+arr[j]) found = true;
			j++;
		}

		i++;
	}
	return found;
}

boolean trova_eff(int arr[], int dim, int k){
	int i=0, j=dim-1, s;
	boolean found=false;

	while(i<j && !found){
		s = arr[i]+arr[j];
		if(s<k) i++;
		else{
			if(s>k) j--;
			else found = true;
		}
	}
	return found;
}


#include <stdio.h>
#define N 10

typedef enum{false, true} boolean;

boolean check(char vet[], int dim, char ch, int k);

int main(){

	char vet[N]={'f','a','}','a','c','2','i','a','l','e'};

	printf("%u\n", check(vet, N, 'a', 2));

	return 0;
}

boolean check(char vet[], int dim, char ch, int k){
	if(dim!=0){
		if(vet[dim-1]==ch) return check(vet, dim-1, ch, k-1);
		else return check(vet, dim-1, ch, k);	
	}
	else{
		if(k<0) return false;
		else return true;
	}
}

#include <stdio.h>
#include <stdlib.h>

typedef enum{false, true} boolean;
typedef enum{Quadri, Cuori, Picche, Fiori} Seme;

typedef struct carta{
	int valore;
	Seme seme;
} Carta;

typedef struct nodo{
	Carta carta;
	int  carteSuccessive;
	struct nodo* next;
} NodoMazzo;

typedef NodoMazzo* Mazzo;


boolean check(Mazzo mazzo, int valore, Seme seme);

void inserisci(Mazzo *mazzo, int valore, Seme seme);

boolean cancella(Mazzo *mazzo, int valore, Seme seme);

int main(){

	Mazzo mazzo=NULL;
	
	return 0;
}

boolean check(Mazzo mazzo, int valore, Seme seme){

	
	if(mazzo==NULL) return false;
	else{
	
		if(mazzo->carta.seme == seme){
			if(mazzo->carta.valore==valore) return true;
			else{
				if(mazzo->carteSuccessive>0) return check(mazzo->next, valore, seme);
				else return false;	
			}
		}
		else return check(mazzo->next, valore, seme);
		
	}

}

void inserisci(Mazzo *mazzo, int valore, Seme seme){

	Mazzo nuova, aux; 
	
	nuova = malloc(sizeof(NodoMazzo));
	nuova->carta.valore = valore;
	nuova->carta.seme = seme;
	nuova->carteSuccessive = 0;

	if (*mazzo == NULL){
		nuova->next = NULL;
		*mazzo = nuova;
	}

	else if ((*mazzo)->next == NULL){
		(*mazzo)->next = nuova;
		nuova->next = NULL;
	}

	else{
		if( (*mazzo)->carta.seme == seme ){
			nuova->carteSuccessive = (*mazzo)->carteSuccessive+1;
			nuova->next = *mazzo;
			(*mazzo) = nuova;
		}

		else inserisci(&((*mazzo)->next), valore, seme);

	}
}

boolean cancella(Mazzo *mazzo, int valore, Seme seme){
	Mazzo aux;
	if(mazzo==NULL || *mazzo == NULL) return false;
	else{
		if((*mazzo)->carta.seme == seme){
			if((*mazzo)->carta.valore == valore){
				aux = *mazzo;
				*mazzo = (*mazzo)->next;
				free(aux);
				return true;
			}
			else{
				if(cancella(&(*mazzo)->next, valore, seme)){ (*mazzo)->carteSuccessive--; return true;}
			
				else return cancella(&(*mazzo)->next, valore, seme);
			}
			
		}
		else{
			return cancella(&(*mazzo)->next, valore, seme);
		}

	}
}