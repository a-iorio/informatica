/*
#include <stdio.h>

typedef enum{false, true} boolean;

int main(){

	int min1, min2, n;

	scanf("%d", &min1);
	scanf("%d", &min2);

	do{
		scanf("%d", &n);
		if(n!=0){
			if(n<min2 && n>min1) min2=n;
			else if(n<min1 && n>min2) min1=n;
			else if(n<min1 && n<min2){
				if(min1<min2) min2=n;
				else min1=n;
			}
		}
	}

	while(n!=0);

	printf("%d %d\n", min1, min2);
	return 0;
}

#include <stdio.h>

char ricalf(char vet[], int dim);

int main(){

	char ch[8]={'w','j','d','l','c','q','q','e'};

	printf("%c\n", ricalf(ch, 8));
	return 0;
}

char ricalf(char vet[], int dim){
	if(dim==1) return vet[0]-1;
	else{
		if(vet[0]<=vet[dim-1]) return ricalf(vet, dim-1);
		else return ricalf(vet+1, dim-1);
	}
}
*/


#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL* next;
} ElementoLista;

typedef ElementoLista* ListaDiElementi;

void inserisci(ListaDiElementi *lista, int n);
void inseriscimancanti(ListaDiElementi *lista, int primo);


int main(){
	int i;
	ListaDiElementi lista=NULL, aux;

	for(i=0; i<7; i++){
		aux=malloc(sizeof(ElementoLista));
		aux->info = i*3;
		aux->next = lista;
		lista = aux;
	}

	while(lista!=NULL){
		printf("%d\n", lista->info);
		lista=lista->next;
	}
	return 0;
}

void inserisci(ListaDiElementi *lista, int n){
	ListaDiElementi aux;
	if(*lista == NULL){
		aux = malloc(sizeof(ElementoLista));
		aux->info = n;
		aux->next = *lista;
		*lista = aux;
	}
	else{
		if((*lista)->info < n){
			aux = malloc(sizeof(ElementoLista));
			aux->info = n;
			aux->next = *lista;
			*lista = aux;
		}
		else inserisci(&((*lista)->next), n);
	}
	return;
}

void inseriscimancanti(ListaDiElementi *lista, int primo){
	ListaDiElementi aux, aux2;
	int act=primo, final;
	if(*lista==NULL) return;
	else{
		aux=*lista;

		while(aux->info != primo){
			aux=aux->next;
		}

		final = aux->next->info+1;

		while(act!=final){
			aux2=malloc(sizeof(ElementoLista));
			aux2->info = final;
			aux2->next = aux->next;
			aux->next = aux2;
			final++;
		}
	}

	return;
}
