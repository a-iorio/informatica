

/*
Laboratorio 6
Nella propria home directory creare una sottodirectory chiamata es06, in cui
metteremo tutti i file C di oggi.
Esercizi su procedure e funzioni
Nel seguito, quando si dice "riceve un array" o qualcosa di equivalente, si intende
*SEMPRE* che venga passata anche la lunghezza dell'array come parametro, anche
se non viene esplicitamente detto.
Inoltre si richiede che, oltre alla funzione o procedura che e' stata richiesta, scriviate
anche un main che usi tale funzione e ne dimostri *ogni*funzionalita'.
Non e' invece necessario che scriviate le funzioni in file separati, potete lavorare
per ogni esercizio su un unico file.
Per ultimo, se con l'esercizio viene fornita la firma(o prototipo) della
funzione/procedura da scrivere, si richiede che la soluzione rispetti questa firma in
ogni dettaglio
(nome funzione, tipo di ritorno, lista di argomenti).
Ricordate che una procedura e' una funzione con tipo di ritorno void.
Per quanto riguarda l'uso dei booleani utilizzare
#DEFINE FALSE 0
#DEFINE TRUE 1
come visto a lezione.
1)Scrivere un programma che dichiari un array di caratteri di dimensione DIM
(dove DIM e' una costante definita con un DEFINE), lo inizializzi con DIM caratteri
letti dall'input e poi stampi il contenuto dell'array a video.

#include <stdio.h>
#define DIM 10

int main(){
	int i;
	char array[DIM];
	for(i=0;i<DIM;i++){
		printf("Inserisci %d valore: ", i);
		scanf("%c", &array[i]);
		while(getchar() != '\n');
	}

	for(i=0; i<DIM; i++){
		printf("Array[%d]: %c\n", i, array[i]);
	}
	return 0;
}

2)Scrivere una funzione che riceva un array di interi e ritorni l'ultimo elemento
dell'array.

#include <stdio.h>

int last(int array[], int dim);

int main(){
	int prova[5]={1,2,3,4,2};
	printf("%d\n", last(prova, 5));
	return 0;
}	

int last(int array[], int dim){
	return array[dim-1];
}

3) Scrivere una procedura che, ricevuto un array di interi, ritorni la somma di tutti gli
elementi e azzeri tutti gli elementi dispari.

#include <stdio.h>

void sumdel(int array[], int dim);

int main(){
	int prova[5]={1,2,3,4,5}, i;
	sumdel(prova, 5);
	for(i=0; i<5; i++){
		printf("%d\n", prova[i]);
	}
	return 0;
}

void sumdel(int array[], int dim){
	int sum=0, i;
	for(i=0; i<dim; i++){
		sum = array[i]+sum;
	}

	for(i=0; i<dim; i++){
		if(i%2!=0) array[i]=0;
	}

	printf("%d\n", sum);
}

4)Scrivere una funzione che, ricevuto un array di caratteri, ritorni la prima lettera
maiuscola, se c'e'. Se non c'e', allora ritorna '\n'.

WRONG


#include <stdio.h>

char firstmin(char array[], int dim);

int main(){
	int dim, i;

	printf("Inserisci lunghezza array: ");
	scanf("%d", &dim);
	getchar();
	
	char array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d carattere:", i+1);
		array[i] = getchar();
		getchar();
	}
	
	printf("%c", firstmin(array, dim));
	return 0;

}

char firstmin(char array[], int dim){
	int i;
	char fail='\n';
	for(i=0; i<dim; i++){
		if(array[i]>=65 && array[i] <= 90) return array[i];
		else return '\n';
	}
}

5)Scrivere una procedura che ricevuto un array di caratteri, trasformi ogni lettera
minuscola in maiuscola, lasciando inalterati gli altri caratteri

6)Dato un array di interi, scrivere una funzione che controlli che l'array contenga solo
elementi pari.

#include <stdio.h>
#define FALSE 0
#define TRUE 1

int pari(int array[], int dim);

int main(){
	int i, dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}

	printf("Check array (0 no, 1 ok): %d\n", pari(array, dim));
	return 0;

}

int pari(int array[], int dim){
	int i;
	for(i=0; i<dim; i++){
		if(array[i]%2 ==0 ) return TRUE;
		else return FALSE;
	}
}

7)Scrivere una funzione che ricevuto un array di interi, ritorni la somma degli
elementi in posizione dispari meno la somma degli elementi in posizione pari.

#include <stdio.h>

int sumdif(int array[], int dim);

int main(){
	int i, dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}

	printf("Somma: %d\n", sumdif(array, dim));

	return 0;
}

int sumdif(int array[], int dim){
	int i, sum=0;
	for(i=0; i<dim; i++){
		if(array[i]%2 != 0) sum=sum+array[i];
		else sum=sum-array[i];
	}

	return sum;
}


8) Dato un array di interi non vuoto, scrivere una funzione che ritorni il numero
di occorrenze del valore minimo (scorrendo l'array una volta sola).


#include <stdio.h>

int occorrenze(int array[], int dim);

int main(){
	int i, dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}

	printf("Occorrenze: %d\n", occorrenze(array, dim));
	return 0;

}

int occorrenze(int array[], int dim){
	int i, min=array[0], occorrenze=1;
	for(i=1; i<dim; i++){
		if(array[i]<min){
			min = array[i];
			occorrenze = 1;
		}
		else if(array[i]==min){
			occorrenze++;
		}		
	}

	return occorrenze;
}


9)Scrivere una funzione/procedura che ricevuto un array di interi, restituisca la
posizione della prima e dell'ultima occorrenza di un elemento pari nell'array.

#include <stdio.h>

void position(int array[], int dim);

int main(){
	int i, dim, pos[2];
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}

	position(array, dim);

	return 0;

}

void position(int array[], int dim){
	int i, dimp=0;

	for(i=0; i<dim; i++){
		if(array[i]%2==0){
			dimp++;
		}
	}
	
	dimp--;

	int pari[dimp], o=0;

	for(i=0; i<dim; i++){
		if(array[i]%2==0){
			pari[o]=i;
			o++;
		}
	}

	printf("Posizione prima: %d Posizione ultima: %d\n", pari[0]+1, pari[dimp]+1);
}

10)Scrivere una funzione che verifichi che un vettore sia ordinato in senso
descrescente.


#include <stdio.h>

int desc(int array[], int dim);

int main(){
	int i, dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}

	printf("Descrescente? (1 true 0 false): %d\n", desc(array, dim));

	return 0;

}

int desc(int array[], int dim){

	int i, prev=array[0], state;

	for(i=1; i<dim; i++){
		if(array[i]<=prev){
			prev=array[i];
			state=1;
		}
		else{
			state=0;
			break;
		}
	}

	return state;
	
}


11) Scrivere una funzione che riceva una matrice bidimensionale di interi e azzeri
ogni elemento il cui contenuto e' maggiore o uguale alla somma delle sue coordinate.
void azzera(int mat[][5], int numRighe);

#include <stdio.h>
void azzero(int mat[][2], int righe);

int main(){
	int i, righe;
	printf("Righe matrice: ");
	scanf("%d", &righe);
	getchar();

	int mat[righe][2];

	for(i=0; i<righe; i++){
		printf("Elemento %d 1: ", i+1);
		scanf("%d", &mat[i][0]);
		getchar();

		printf("Elemento %d 2: ", i+1);
		scanf("%d", &mat[i][1]);
		getchar();
	}

	printf("Matrice:\n");
	for(i=0; i<righe; i++){
		printf("%d %d\n", mat[i][0], mat[i][1]);
	}

	printf("Matrice azzerata:\n");
	azzero(mat, righe);

	return 0;

}

void azzero(int mat[][2], int righe){
	int i;
	for(i=0; i<righe; i++){
		if(mat[i][0] >= (i+1+1)){
			mat[i][0]=0;
		}
		if(mat[i][1] >= (i+1+2)){
			mat[i][1]=0;
		}
	}

	for(i=0; i<righe; i++){
		printf("%d %d\n", mat[i][0], mat[i][1]);
	}
	
}

12)Esercizio 5: MSS, Maximum Segment Sum
Dato un array di interi positivi e negativi, il segmento di somma massima e' la
porzione contigua dell'array in cui la somma degli elementi ha valore piu' alto. Ad
esempio l'array
[2,-4,2,-1,6-3]
ha come SSM il segmento [2,-1,6] di valore 7. Si chiede di definire due funzioni per
la stampa e per il calcolo di SSM, con i seguenti prototipi :
 stampa l'array s di lunghezza n 
void print_array(int s[], int n);
 calcola SSM sull'array s di lunghezza lung
 \param s array
 \param n lunghezza
 \param s_init puntatore alla variabile che conterra' la posizione di inizio dell'SSM
 \param s_lung puntatore alla variabile che conterra' la lunghezza del segmento di
somma massima
 \retval k la somma degli elementi dell' SSM
 int ssm (int s[], int n, int * s_init, int * s_lung);

*/