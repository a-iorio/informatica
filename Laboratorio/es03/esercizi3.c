
#include <stdio.h>
main(){

	/*
	[4] Scrivere un programma che chiede una sequenza di numeri interi all'utente. Il
	programma termina quando l'utente fornisce un numero minore o uguale a 0, e
	stampa un messaggio che indichi il massimo e il numero di occorrenze del massimo,
	il minimo e il suo numero di occorrenze.

	int max, min, temp, i_max, i_min;
	scanf("%d", &temp);
	max = temp;
	min = temp;
	i_max = 1;
	i_min = 1;
	while(temp>0){
		scanf("%d", &temp);

		if(temp==max){
			i_max++;
		}
		if(temp==min){
			i_min++;
			printf("%d", i_min);
		}
		else{
			if(temp>max){
				max = temp;
				i_max = 1;
			}
			if(temp<min){
				min = temp;
				i_min = 1;
			}
		}

	}
	printf("Max: %d per %d volte \n Min: %d per %d volte \n", max, i_max, min, i_min);
	*/

	/*[5] Scrivere un programma rettangolo che legge due interi positivi A e B e quindi
	stampa un rettangolo di dimensioni AxB usando il carattere '*'. 
	int n1, n2, i, o;
	do{
	scanf("%d", &n1);
	scanf("%d", &n2);
	}
	while(n1 < 0 && n2 < 0);

	for(i=1; i<=n2; i++){
		for(o=1; o<=n1; o++){
			printf("*");
		}
		printf("\n");
	}
	*/
	/*
	[6] Scrivere un programma rappresentazione che legge una sequenza
	i 0 e di 1 di dimensione prefissata K e stampa il numero intero la cui
	rappresentazione in complemento a 2 su K cifre è la sequenza letta.

	/*
	[7] Scrivere un programma isoscele che chieda all'utente un intero n e stampi un
	triangolo isoscele di asterischi, di altezza lunga n e base lunga 2n-1.
	(Se il valore letto è negativo si consideri il suo valore
	assoluto).
	

	int n, i, b, o;
	scanf("%d", &n);
	if(n<0)
		n=-n;

	for(i=1; i<=n; i++){
		for(o=1; o<=n-i; o++){
			printf(" ");
		}

		for(b=1; b<=2*i-1; b++){
			printf("*");
		}
		printf("\n");
	}

	*/
	/*
	[8] Modificare il programma precedente per stampare un rombo
	vuoto.
	Nota: accettare solo numeri dispari come altezza.

	int n, i, j, k;
	do{
		printf("Immetti un numero dispari:");
		scanf("%d", &n);
	}
	while(n%2==0);

	for(i=1;i<=n;i++)
	{
		for(j=1;j<=n;j++){
			if(((i+j)==n/2+2) || ((j-i)==n/2) || ((i-j)==n/2) || ((i+j==n/2+n/2+n/2+2))){
				printf("*");
			}
			else{
				printf(" ");
			}
		}
		printf("\n");
	}
	*/

	/*
	[9] Modificare il programma 6 per stampare un triangolo isoscele di altezza lunga n
	(con n compreso tra 0 e 9 ) e base lunga 2n-1 fatto come sotto.
	Se il valore letto è non è compreso tra 0 e 9 si chieda all'utente un nuovo valore
	finché l'intero immesso non appartenga all'intervallo richiesto.
	
	int n, i, o, b;
	do{
	printf("Immetti un numero compresto tra 0-9:");
	scanf("%d", &n);
	}
	while (n<0 || n>9);

	for(i=1; i<=n; i++){
		for(o=1; o<=n-i; o++){
			printf(" ");
		}

		for(b=i; b>0; b--){
			printf("%d", b);
		}
		for(b=2; b<=i; b++){
			printf("%d", b);
		}
		printf("\n");
	}
	*/

	/*
	11) Esercizio proposto a lezione:
	"Algoritmo di Euclide con i resti per il calcolo del MCD". 
	
	int m, n;

	do{
	printf("Inserisci 2 numeri maggiori di 0:\n");
	scanf("%d", &m);
	scanf("%d", &n);
	}
	while(m<=0 || n<=0);

	while(m != 0 && n != 0){
		if(m>n)
			m = m%n;
		else
			n = n%m;
	}
	if(m!=0)
		printf("MCD è: %d \n", m);
	else
		printf("MCD è: %d \n", n);

	*/

return 0;
}