#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

int main(){

/*
1) Scrivere una funzione che, ricevuti due parametri numerici A e B, verifichi se
A e B sono coprimi (o primi tra loro) ritornando 1 nel caso lo siano, 0 altrimenti.
Due numeri sono coprimi se non hanno divisori in comune a parte 1.
Per verificare se due numeri sono coprimi cercare i loro divisori del piu' grande
nell'intervallo [2, N/2] dove N e' il piu' piccolo fra A e B.
2)Scrivere una funzione che accetti in ingresso tre valori
x, a e b e verifichi che x sia compreso nell'intervallo
[a,b]. Scrivere quindi una funzione main() per collaudare la funzione implementata.
3)Scrivere una funzione con quattro parametri
a, b, c, x reali che restituisce il risultato della funzione ax 2+ bx + c. Scrivere quindi una
funzione
main()per collaudare la funzione implementata.
4)Scrivere una funzione che riceve i coefficienti a, b, c
reali di un'equazione di secondo grado e restituisce
1 se l'equazione ha soluzioni reali e 0 altrimenti. Scrivere quindi una funzione main() per
collaudare la funzione implementata.
5)Scrivere una funzione che, ricevuti il parametro n calcoli l'n-simo numero di
fibonacci.Scrivere quindi una funzione main()per collaudare la funzione implementata.
6)Scrivere una procedura per stampare il bordo di un rettangolo di dimensioni chieste
all'utente che puo' anche scegliere il carattere di riempimento.
Esempio di rettangolo 7x3 con carattere di riempimento *:
*******
* *
*******
Scrivere quindi una funzione main()per collaudare la procedura implementata.
7) Scrivere una procedura per stampare un rombo vuoto con carattere di riempimento
scelto dall'utente.
Nota: accettare solo numeri dispari come altezza.
Esempio di interazione con il programma:
Inserisci l'altezza (dispari): 8
Inserisci l'altezza (dispari): 9
Inserisci carattere esterno : *
Inserisci il carattere di riempimento interno : a
 
 *
 *a*
 *aaa*
 *aaaaa*
*aaaaaaa*
 *aaaaa*
 *aaa*
 *a*
 *
Scrivere quindi una funzione main()per collaudare la procedura implementata.
8)Scrivere una procedura che dato n tra 1 e 100 lo stampi usando la notazione romana:
I
II
III
IV
V
VI
VII
VIII
IX
X
...
XCVIII
XCIX
C
Attenzione alle eccezioni: 88=LXXXVIII ma 99=XCIX
Per informazioni sulla notazione romana
http://it.wikipedia.org/wiki/Numeri_romani
Scrivere quindi una funzione main()che chiesto all'utente un numero m tra 1 e 100
stampi su un unica linea tutti i numeri romani tra 1 e m.
9) Scrivere una funzione massimo comun divisore che realizza la funzione del massimo
comun divisore con uno degli algoritmi visti a lezione.
Scrivere quindi una funzione main()per collaudare la funzione implementata. 
*/

return 0;
}