/*
Esercitazione stdio.h, stringhe, strutture
1) Utilizzare la libreria stdio.h per creare un file misure che contenga 150 numeri interi
nell'intervallo (0,20] generati casualmente con la funzione rand(). I numeri devono essere scritti uno
per linea separati da '\n'.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){
	int i, num;
	FILE * ofp;
	srand(time(NULL));

	ofp = fopen("./outputfile", "w");
	if(ofp==NULL){
		perror("fopen: outputfile");
		return EXIT_FAILURE;
	}

	for(i=0; i<150; i++){
		num = (rand() % 20) +1;
		fprintf(ofp, "%d\n", num);
	}

	return 0;
}


2)Utilizzare il file misure generato nell'esercizio precedente e calcolare il numero di valori generati
che ricadono nei 10 intervalli
(0, 2] (2,4] ..... (18,20]
stampare il numero dei valori per ciascun intervallo sullo standard output.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){

	int num, n=1;
	int intervalli[10]={0};

	FILE *ifp;
	ifp = fopen("./outputfile", "r");

	while(fscanf(ifp, "%d\n", &num)==1){
		while(num>2*n) n++;
		intervalli[n-1]++; 
		n=1;
	}

	for(n=0; n<10; n++){
		fprintf(stdout, "%d intervallo: %d \n", n+1, intervalli[n]);
	}
	return 0;
}

3)Ripetere i due esercizi precedenti considerando numeri reali.
 I numeri reali possono essere generati usando la seguente formula
((double) rand())+ 1.0) / ((((double) RAND_MAX) + 1.0) / 20)
 Avete altre idee? Funzionano? Se no, perche'?
*/



/*
4) Scrivete una funzione C nuovo_mazzo() che crea un mazzo (mischiato!) di 40 carte utilizzando i
seguenti tipi:
/** i valori delle carte 
typedef enum valori{ASSO,DUE,TRE,QUATTRO,CINQUE,SEI,SETTE,FANTE,DONNA,RE}
valori_t;
/** i semi delle carte 
typedef enum semi{CUORI,QUADRI,FIORI,PICCHE } semi_t;
/** una carta e' data da un valore ed un seme 
typedef struct carta {
 /** valore 
 valori_t val;
 /** seme 
 semi_t seme;
} carta_t;
ed una funzione stampa_mazzo() che stampa le carte del mazzo sullo standard output in modo
gradevole. Definite i parametri ed i valori restituiti dale funzioni opportunamente.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_CARTE 40
typedef enum valori{ASSO,DUE,TRE,QUATTRO,CINQUE,SEI,SETTE,FANTE,DONNA,RE} valori_t;
typedef enum semi{CUORI,QUADRI,FIORI,PICCHE} semi_t;

typedef struct carta {
	valori_t val;
	semi_t seme;
} carta_t;

int nuovo_mazzo(carta_t *mazzo[]);

int main(){
	int i, f;	
	carta_t mazzo[NUM_CARTE];
	f = nuovo_mazzo(&mazzo);
	for(i=0; i<NUM_CARTE; i++){
		printf("Carta %d: %d di %d\n", i, mazzo[i].val, mazzo[i].seme);
	}

}

int nuovo_mazzo(carta_t *mazzo){
	int i, o, z=0, num;
	carta_t temp;

	srand(time(NULL));

	for(i=ASSO; i<=RE; i++){
		for(o=CUORI; o<=PICCHE; o++){
				mazzo[z]->val = i;
				mazzo[z]->seme = o;
				z++;
		}
	}

	for(i=0; i<NUM_CARTE; i++){
		num = rand() % NUM_CARTE;
		temp = *mazzo[i];
		*mazzo[i] = *mazzo[num];
		*mazzo[num] = temp;
	}

	return 0;
}



5) Utilizzando le funzioni dell'esercizio precedente realizzate un programma C che gioca a briscola
con un utente. Il programma crea il mazzo di carte, stampa sullo standard output il nome della
briscola e le carte in mano all'utente ed attende sullo standard input la giocata. Il programma puo'
giocare con una strategia semplice a piacere (ad esempio la prima carta della mano). Ad esempio:
$./briscola
Nuova partita, briscola CUORI
Mano #1: Hai in mano
4Fiori 5Picche QCuori
Cosa giochi ?
se digitiamo
4Fiori
io gioco 2Cuori, preso
Mano #2: Hai in mano
KFiori 5Picche QCuori
io gioco 7Picche
Cosa giochi ?
Il gioco continua fino all'esaurimento delle carte. Alla fine il programma stampa il vincitore ed i
punti totalizzati. Ad esempio:
Hai vinto con 87 punti!


6) Definire un nuovo tipo di dato capace di rappresentare una data.
Scrivere poi delle opportune funzioni/procedure che:
- ricevuta una data la aggiorni al giorno successivo (ignorando gli anni bisestili);
ricevute due date verifichino che la prima preceda la seconda;
ricevute due date, ritornino la differenza in anni fra la prima e la seconda.

#include <stdio.h>

typedef struct data{int giorno, mese, anno;} data;

void update(data *d1);
int check(data d1, data d2);
int difference(data d1, data d2);

int main(){
	int err;
	data d1, d2;

	printf("Inserisci prima data: ");
	scanf("%d%*c%d%*c%d%*c", &d1.giorno, &d1.mese, &d1.anno);
	printf("Inserisci seconda data: ");
	scanf("%d%*c%d%*c%d%*c", &d2.giorno, &d2.mese, &d2.anno);

	err = check(d1, d2);
	printf("%d\n", err);

	err = difference(d1, d2);
	printf("%d\n", err);

	update(&d1);
	printf("%d-%d-%d\n", d1.giorno, d1.mese, d1.anno);

	return 0;
}

void update(data *d1){
	if((d1->mese == 4 || d1->mese == 6 || d1->mese == 9 || d1->mese == 11) && d1->giorno==30){
		d1->giorno=1;
		(d1->mese)++;
	}
	else if(d1->mese==2 && d1->giorno == 28){
		(d1->mese)++;
		d1->giorno=1;
	}
	else if(d1->giorno == 31){
		if(d1->mese==12){
			d1->mese=d1->giorno=1;
			(d1->anno)++;
		}
		else{
			(d1->mese)++;
			d1->giorno=1;
		}
	}
	else	(d1->giorno)++;
}

int check(data d1, data d2){
	if(d1.anno < d2.anno) return 1;
	else if(d1.anno == d2.anno && d1.mese < d2.mese) return 1;
	else if(d1.anno == d2.anno && d1.mese == d2.mese && d1.giorno<d2.giorno) return 1;
	else return -1;
}

int difference(data d1, data d2){
	if(d1.anno < d2.anno) return d2.anno-d1.anno;
	else return d1.anno-d2.anno;
}

7)Definire un nuovo tipo di dato capace di rappresentare i dipendenti di una ditta: di tali dipendenti
interessa il codice numerico identificativo unico, la data di assunzione, il grado
raggiunto (un intero) e lo stipendio.
Usare il tipo di dato definito nell'esercizio precedente per tutte le date.
Scrivere le seguenti funzioni/procedure:
•calcolaAnzianita che, dato un dipendente, calcoli il numero di anni trascorsi dalla data di
assunzione al 2102;
•aggiornaStipendio che, dato un dipendente, aumenti del 1% il suo stipendio per ogni anno di
anzianita' accumulato (attenzione l'interesse da calcolare e' un interesse composto).
•superiore, che dati due dipendenti ritorni 1,0 o -1 se il primo e' rispettivamente un superiore, un
pari grado o un sottoposto
del secondo. Notare che a parita' di grado, il superiore e' il dipendente con l'anzianita' maggiore. Se
e' pari anche l'anzianita', i dipendenti sono pari grado.
Scrivere quindi un programma che richieda all'utente di inserire i dati di due impiegati, utilizzi le
due procedure di cui sopra e ne stampi l'esito.

#include <stdio.h>

typedef struct data{int giorno, mese, anno;} Data;
typedef struct dipendente{
	int codice;
	Data data;
	int grado;
	float stipendio;
} Dipendente;

int calcolaAnzianita(Dipendente dipendente);
int superiore(Dipendente d1, Dipendente d2);

void aggiornaStipendio(Dipendente *dipendente);

int main(){
	Dipendente d1, d2;
	printf("Inserisci dati primo impiegato:\n");
	printf("Codice: ");
	scanf("%d", &d1.codice);
	printf("Data: ");
	scanf("%d%*c%d%*c%d", &d1.data.giorno, &d1.data.mese, &d1.data.anno);	
	printf("Grado: ");
	scanf("%d", &d1.grado);	
	printf("Stipendio: ");
	scanf("%f", &d1.stipendio);

	printf("Inserisci dati secondo impiegato:\n");
	printf("Codice: ");
	scanf("%d", &d2.codice);
	printf("Data: ");
	scanf("%d%*c%d%*c%d", &d2.data.giorno, &d2.data.mese, &d2.data.anno);	
	printf("Grado: ");
	scanf("%d", &d2.grado);	
	printf("Stipendio: ");
	scanf("%f", &d2.stipendio);

	printf("%d\n", calcolaAnzianita(d1));
	aggiornaStipendio(&d1);
	printf("%f\n", d1.stipendio);
	printf("Superiore: %d\n", superiore(d1,d2));
	return 0;
}

int calcolaAnzianita(Dipendente dipendente){	
	if(dipendente.data.anno<2012) return 2012-dipendente.data.anno;
	else return 0;
}
int superiore(Dipendente d1, Dipendente d2){
	if(d1.grado>d2.grado) return 1;
	else if(d1.grado==d2.grado){
		if(d1.data.anno < d2.data.anno || ((d1.data.giorno < d2.data.giorno) && (d1.data.anno == d2.data.anno)) || ((d1.data.mese < d2.data.mese) && (d1.data.anno == d2.data.anno))) return 1;
		else if (d1.data.anno == d2.data.anno && d1.data.giorno == d2.data.giorno && d1.data.mese == d2.data.mese) return 0;
		else return -1;
	}
	else return -1;
}

void aggiornaStipendio(Dipendente *dipendente){
	int incremento, anni;
	anni = calcolaAnzianita(*dipendente);
	dipendente->stipendio = dipendente->stipendio+0.1*anni*(dipendente->stipendio);
}


8)Scrivere un programma C che legge una sequenza di studenti dal file anagrafe_studenti. Ogni
studente e' memorizzato su file in una singola linea contenente tre stringhe di caratteri separate da ':'
e terminata da '\n' secondo il formato
cognome:nome:numero_di_matricola
quindi ad esempio
...
Rossi:Mario:234445
Bixio:Nino:435678
Garibaldi:Giuseppe:787899
...
Il programma legge da file gli studenti e memorizza i dati relativi a ciascun studente in un array di
strutture di tipo:
#define N 50
typedef struct {
 char nome[N+1];
 char cognome[N+1];
 unsigned matricola;
 } studente_t;
L'array viene poi ordinato per il campo cognome e nel caso di cognomi uguali per il campo nome e
poi stampato sullo standard output.
Suggerimento: Per la lettura da file usare fscanf() con una opportuna stringa di formattazione
oppure fgets() per leggere fino al primo \n e strchr() per localizzare i caratteri separatori : 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 50

typedef struct {
	char nome[N+1];
	char cognome[N+1];
	unsigned matricola;
} studente_t;

int main(){

	FILE *f;
	f = fopen("./anagrafe_studenti", "r");
	char aux[3*N];
 
	char line[1024];
	int num=0, i, j;

	while(fgets(line,sizeof(line),f) != NULL) num++;
	rewind(f);
	studente_t studenti[num];

	while(fgets (aux, 3*N, f)!=NULL){
		char *ch, *ch2;
		ch = strtok(aux, ":");
		strcpy(studenti[i].cognome, ch);
		ch = strtok(NULL, ":");

		strcpy(studenti[i].nome, ch);
		ch = strtok(NULL, ":");

		studenti[i].matricola = atoi(ch);
		i++;
	}

	studente_t temp;

	for (i=1;  i<num; i++) {
      for (j=1; j<num; j++) {
         if(strcmp(studenti[j-1].cognome, studenti[j].cognome)>0){
            temp = studenti[j-1];
            studenti[j-1] = studenti[j];
            studenti[j] = temp;
         }
         else if(strcmp(studenti[j-1].cognome, studenti[j].cognome)==0){
         	if(strcmp(studenti[j-1].nome, studenti[j].nome)>0){
         		temp = studenti[j-1];
            	studenti[j-1] = studenti[j];
           		studenti[j] = temp;
         	}
         }

      }
   }

   for(i=0; i<num; i++){
   	printf("%s %s %d\n", studenti[i].cognome, studenti[i].nome, studenti[i].matricola);
   }

	return 0;
}
*/