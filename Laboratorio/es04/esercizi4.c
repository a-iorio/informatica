#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>

int main(){

	/*
	[1] Scrivere un programma tipo_elementare.c che, per ciascun tipo elementare
	già visto a lezione, stampa una riga contenente il nome del tipo e la
	dimensione, in byte, di una variabile di quel tipo.
	*/


	/*

	• [2] Modificare il programma area_rettangolo dato nel primo laboratorio per
	calcolare e stampare anche l'area di un quadrato avente lo stesso perimetro ed il
	perimetro di un quadrato avente la stessa area. Usare la funzione sqrt per la
	radice quadrata


	• [3] Scrivere un programma C che legge in input un carattere minuscolo e lo
	trasforma in un carattere maiuscolo. Controllare che il carattere letto sia
	effettivamente una lettera minuscola.
	
	char ch;
	do{
		printf("\nInserisci un carattere minuscolo: ");
	    ch = getchar();
	    getchar();
 	}
   	while(ch >= 'A' && ch <= 'Z');
    ch = ch+'A'-'a';
    printf("Il carattere maiuscolo è: %c \n", ch);

   
	/*

	• [4] Scrivere un programma C che legge un intero X (tipo int) da standard
	input e stampa sullo standard output le cifre della sua rappresentazione in
complemento a due. 
	


		Suggerimento: utilizzare gli operatori bit a bit di shift («,»), complemento(~), and(&),
	or(|), xor (^) per ricavare le cifre dalla rappresentazione interna di X come int senza
	ricalcolarla.
	• [5] Scrivere il programma calcolatrice che legge un valore di tipo double, uno
	di tipo char e poi ancora uno di tipo double. Se il carattere letto è uno dei
	quattro segni di operazioni aritmetiche (+, -, *, /) il programma esegue
	l'operazione sui due valori e stampa il risultato. Altrimenti stampa un
	messaggio di errore. Stampare i risultati con al max 2 cifre decimali.


	• [6] Nella morra due giocatori si sfidano scegliendo un simbolo ciascuno tra
	sasso, forbici e carta: due simboli uguali pareggiano, mentre il sasso batte le
	forbici, le forbici battono la carta, e la carta batte il sasso. Scrivere il
	programma morra che gestisce una sfida tra PC e utente:
	(a) generando un numero casuale da 1 a 3 così definiti:1: sasso, 2: forbici, 3:
	carta (utilizzare il costrutto #define per rendere leggibile l'associazione tra il
	numero e il simbolo)
	(b) leggendo un carattere ('s': sasso, 'f': forbici, 'c': carta)
	(c) stampando l'esito del confronto.
	Se l'utente immette un carattere diverso da 's', 'f' e 'c' allora perde comunque.
	
	*/
	/*

	• [7] La media pesata dei voti di uno studente si calcola moltiplicando ogni voto
	per il suo peso in crediti, sommando tutti questi valori e dividendo per la
	somma del numero di crediti.
	Quindi uno studente con due esami, fisica 1 (voto: 24, crediti:15) e informatica
	(voto: 28, crediti:6), avra' una media pesata circa uguale a
	(24*15 + 28*6)/(15+6) = 25,14.
	Scrivere il programma calcola_la_media che chiede ad uno studente i voti degli
	esami e il loro peso in crediti, uno per volta.
	Lo studente dovrà inserire 0 per segnalare che ha terminato
	l'inserimento.
	Il programma quindi calcola e stampa la sua media pesata sui crediti.
	Nota: si tenga conto che la votazione del singolo esame e il numero di crediti
	sono interi.
	Inoltre sono votazioni valide per il superamento di un esame solo quelle
	comprese tra 18 e 30 (estremi inclusi) e il numero di crediti di un esame
	deve essere maggiore di 0.
	

	int n=30, i=0, end, tot1=0, tot2=0, stop=1;
	float media;
	int voto[n], crediti[n];
	
	while(stop!=0){
		printf("Iserisci il voto (0 termina): ");
		scanf("%d", &voto[i]);	
		if(voto[i]==0){stop=0; break;}
		while(voto[i]<18 || voto[i]>30){
			printf("Reinserisci il voto (0 termina): ");
			scanf("%d", &voto[i]);
		} 
		printf("Iserisci il credito (0 termina): ");
		scanf("%d", &crediti[i]);
		if(crediti[i]==0){stop=0; break;}
		while(crediti[i]<0){
			printf("Reinserisci il credito (0 termina): ");
			scanf("%d", &crediti[i]);
		} 

		i++;
	}	
	
	end = i;
	for(i=0; i<=end; i++){
		tot1 = voto[i]*crediti[i]+tot1;
		tot2 = crediti[i]+tot2;
	}
	media = tot1/(float)tot2;
	printf("Media: %.2f\n", media);

	/*
	• [8] Modificare il programma precedente per chiedere il numero di crediti
	del prossimo esame che lo studente deve sostenere.
	Il programma quindi calcola quale voto lo studente dovrebbe prendere perchè 
	la media migliori raggiungendo la votazione (intera) immediatamente superiore
	alla media attuale. Se tale voto fosse maggiore di 30, il programma deve
	stampare il messaggio "Mi dispiace, non si puo'".
		
	int n=30, i=0, end, tot1=0, tot2=0, stop=1, voto_n, credito_n;
	float media;
	int voto[n], crediti[n];
	
	while(stop!=0){
		printf("Iserisci il voto (0 termina): ");
		scanf("%d", &voto[i]);	
		if(voto[i]==0){stop=0; break;}
		while(voto[i]<18 || voto[i]>30){
			printf("Reinserisci il voto (0 termina): ");
			scanf("%d", &voto[i]);
		} 
		printf("Iserisci il credito (0 termina): ");
		scanf("%d", &crediti[i]);
		if(crediti[i]==0){stop=0; break;}
		while(crediti[i]<0){
			printf("Reinserisci il credito (0 termina): ");
			scanf("%d", &crediti[i]);
		} 

		i++;
	}	
	
	end = i;
	for(i=0; i<=end; i++){
		tot1 = voto[i]*crediti[i]+tot1;
		tot2 = crediti[i]+tot2;
	}
	media = tot1/(float)tot2;

	if((int)media+1>30){
		printf("Mi dispiace non si può\n");
	}
	else{
		printf("Quanti crediti è il prossimo esame? ");
		scanf("%d", &credito_n);
		voto_n = (((int)media+1)*(tot2+credito_n)-tot1)/credito_n;
		printf("Devi prendere almeno %d\n", voto_n);
	}


	• [9] Scrivere un programma alfabeto che chiede all'utente una sequenza di
	caratteri alfabetici minuscoli verificando che ogni carattere letto sia maggiore
	o uguale ai precedenti (secondo l'ordine alfabetico).
	Il primo carattere inserito può essere un qualsiasi carattere minuscolo.
	La sequenza termina quando l'utente immette un carattere non
	alfabetico o maiuscolo oppure se immette un carattere minore di uno di quelli
	letti precedentemente.
	Terminata la lettura dei caratteri il programma deve stampare il numero di
	caratteri minuscoli diversi appartenenti alla sequenza (il carattere che causa la
	terminazione non è considerato parte della sequenza).
	Se la sequenza è vuota, cioè non viene immesso alcun carattere minuscolo,
	allora il programma stampa solo un avvertimento.
	Esempi di esecuzione:
	Dammi un carattere: X
	La sequenza di lettere minuscole e' vuota
	Dammi un carattere: a
	Dammi un carattere: r
	Dammi un carattere: r
	Dammi un carattere: f
	Totale lettere minuscole ordinate e diverse: 2
	Dammi un carattere: a
	Dammi un carattere: a
	Dammi un carattere: a
	Dammi un carattere: d
	Dammi un carattere: z
	Dammi un carattere: 4
	Totale lettere minuscole ordinate e diverse: 3



	• [10] Un triangolo rettangolo puo' avere tutti i lati di lunghezza intera.
	Un insieme di tre valori interi per i lati di un triangolo rettangolo e'
	chiamato Tripletta Pitagorica. Questi tre lati devono soddisfare
	la condizione che la somma del quadrato di due dei lati deve essere uguale al
	quadrato del terzo.
	Scrivere un programma che trovi e stampi tutte le triplette
	pitagoriche in cui tutti e tre i lati sono minori di 500.
	Suggerimento: usare tre for annidati ed eseguire la ricerca in modo esaustivo,
	cioe' coprendo tutte le possibili triple.

	int i, o, k, z, n=50, p=0, state=0;
	int terne[n][3];

	for(i=1; i<=n; i++){
		for(o=1; o<=n; o++){
			for(k=1; k<=n; k++){
				if((i*i+o*o)==k*k && i+o > k){

					for(z=0; z<=p; z++){
						if((i == terne[z][0] && o == terne[z][1]) || (o == terne[z][0] && i == terne[z][1])){
							state=1;
							break;
						}
						else{
							state=0;
						}
					}

					if(state==0){
						terne[p][0] = i;
						terne[p][1] = o;
						terne[p][2] = k;
						p++;
					}

					state=0;
				}
			}
		}
	}

	for(i=0; i<p; i++){
			printf("%d %d %d ", terne[i][0], terne[i][1], terne[i][2]);
			printf("\n"); 
	}


	• [11] Esercizio proposto a lezione:
	scrivi un programma che stampa la tavola pitagorica (come matrice quadrata,
	con righe da 1 a 10 e colonne da 1 a 10).
	Sulle slide dell'ultima lezione c'è una possibile soluzione, chi
	non se la ricorda provi a ricostruirla.
	Riuscite a stampare la tavola correttamente spaziata?
	(Suggerimento: guardate i dettagli del formato di printf).
	Usate la barra verticale | per separare ogni colonna dalla successiva
	e sequenze di segni meno - per separare ogni riga dalla successiva.
	A lezione è stata proposta una soluzione con due cicli for annidati.
	Riuscite a scrivere il programma con un unico ciclo for?
	Ne vale la pena? Quali sono gli svantaggi di quest'ultima soluzione?

	*/
	
return 0;
}