/*
Esercizi su funzioni e procedure ricorsive
Quando si ha a che fare con array in funzioni o procedure
ricorsive, si possono usare due metodi: il primo utilizza
l'aritmetica dei puntatori per aggiornare ogni volta la testa
dell'array, il secondo invece si porta dietro un indice per
tenere traccia della posizione raggiunta. Ad ogni modo e' sempre
necessario portarsi dietro la dimensione (residua o totale, a
seconda del caso.
Ricordate che oltre alla funzione o procedura indicata
dall'esercizio, si richiede che scriviate anche un main che usi
tale funzione e ne dimostri *ogni* funzionalita'.
Inoltre, se con l'esercizio viene fornita la firma della
funzione/procedura da scrivere, si richiede che la soluzione
rispetti questa firma in ogni dettaglio.
Ricordate infine che una procedura e' una funzione con tipo di
ritorno void.
Esercizi

1) Scrivere una funzione ricorsiva potenza che riceva due interi,
base e esponente e ritorni il valore della base elevata alla
potenza esponente.
int potenza(int base, int esponente);

#include <stdio.h>

int potenza(int base, int esponente);

int main(){
	int base, esponente;

	printf("Inserisci base: ");
	scanf("%d", &base);
	printf("Inserisci esponente: ");
	scanf("%d", &esponente);

	printf("%d^%d = %d\n", base, esponente, potenza(base,esponente));
	return 0;

}

int potenza(int base, int esponente){
	int ris;	
	if(esponente==0) ris=1;
	else ris=base*potenza(base, esponente-1);
	return ris;
}

2) Scrivere una funzione ricorsiva fattoriale che riceva un intero
e ritorni il fattoriale di quel valore.
int fattoriale(int val);

#include <stdio.h>

int fattoriale(int val);

int main(){
	int val;

	printf("Inserisci numero: ");
	scanf("%d", &val);
	printf("%d! = %d\n", val, fattoriale(val));
	return 0;

}

int fattoriale(int val){
	int ris;
	if(val==0) ris=1;
	else ris = val*fattoriale(val-1);
	return ris;
}


3) Scrivere una procedura ricorsiva che riceva un array a lo
stampi a video.
Scrivere la procedura con i due metodi:
- con l'aritmetica dei puntatori
void stampa1 (int vet[], int dim);
- con un indice
void stampa2 (int vet[], int i, int dim);


#include <stdio.h>

void stampa1(int vet[], int dim);

int main(){
	int i, dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}	

	stampa1(array, dim);
	return 0;

}

void stampa1(int vet[], int dim){
	if (dim != 0) {
		stampa1(vet, dim-1);
		printf("%d\n", vet[dim-1]);
	}
}


4) Modificare le procedure ricorsive dell'esercizio precedente per
stampare l'array in senso inverso.
void stampaInverso1 (int vet[], int dim);
void stampaInverso2 (int vet[], int i, int dim);

#include <stdio.h>

void stampaInverso1(int vet[], int dim);
void stampaInverso2(int vet[], int i, int dim);

int main(){
	int i, dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}	

	stampaInverso1(array, dim);
	stampaInverso2(array, 0, dim);
	return 0;

}

void stampaInverso1(int vet[], int dim){
	if (dim != 0) {
		printf("%d\n", vet[dim-1]);
		stampaInverso1(vet, dim-1);
	}
}

void stampaInverso2(int vet[], int i, int dim){
  	if (dim != 0) {
		printf("%d\n", vet[dim-1]);
		stampaInverso1(vet, dim-1);
	}
	printf("%d ", vet[dim-(i+1)]);
	stampaInverso2(vet, i+1, dim); 
}



5) Scrivere una funzione ricorsiva che calcoli la somma di due
numeri senza usare altra operazione matematica che l'incremento o
il decremento di uno.
int somma(int a, int b);


#include <stdio.h>

int somma(int a, int b);

int main(){
	int a, b;

	printf("Inserisci a: ");
	scanf("%d", &a);
	printf("Inserisci b: ");
	scanf("%d", &b);

	printf("%d+%d = %d\n", a, b, somma(a,b));
	return 0;
}

int somma(int a, int b){
	int ris;
	if(b==0) ris = a;
	else ris = 1+somma(a, b-1);
	return ris;
}

6)Scrivere una funzione che calcoli ricorsivamente il numero di
elementi pari di un array passato.

#include <stdio.h>

int numpari(int vet[], int dim);

int main(){
	int i, dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}	

	printf("Numero di pari: %d", numpari(array, dim));
	return 0;

}

int numpari(int vet[], int dim){
	int ris=0;

	if (dim==0) ris=0;
	else
		if(vet[0]%2==0)
			ris=1+numpari(vet+1, dim-1);
		else
			ris=numpari(vet+1, dim-1);

	return ris;
}


7) Scrivere una funzione che controlli che l'array di caratteri
passato per argomento sia palindromo. 
*/


/*
8)Scrivere una funzione ricorsiva che azzeri tutti gli elementi
pari, raddoppi gli elementi dispari e ritorni il minimo elemento
di un array passato.
*/


/*
9) Scrivere una funzione che chieda un valore N all'utente e
quindi calcoli ricorsivamente l'N-esimo termine della successione
di Fibonacci.
F(n) = F(n-1) + F(n-2)

#include <stdio.h>
int fibonacci(int val);

int main(){
	int val;

	printf("Inserisci numero: ");
	scanf("%d", &val);
	printf("F(%d) = %d\n", val, fibonacci(val));
	return 0;

}

int fibonacci(int val){
	int ris;
	if(val==0) ris=0;
	else if(val==1) ris=1;
	else ris = fibonacci(val-1)+fibonacci(val-2);
	return ris;
}


10) Scrivere una funzione ricorsiva che riceva un array e due
indici e calcoli la somma degli elementi dell'array compresi fra
quegli indici.
int sumVet(int v[], int from, int to);

#include <stdio.h>

int sumVet(int v[], int from, int to);

int main(){
	int i, dim, from, to;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}	

	printf("From: ");
	scanf("%d", &from);
	printf("To: ");
	scanf("%d", &to);

	printf("%d\n", sumVet(array, from, to));
	return 0;

}

int sumVet(int v[], int from, int to){
	
	int somma=0;
	if (from==to)
		somma = v[from-1];
	else
		somma = v[from-1] + sumVet(v, from+1, to);
	return somma;

}

11) Scrivere una procedura ricorsiva che inverte la porzione di un
array individuata dagli indici from e to.
void inverti(int v[], int from, int to);

#include <stdio.h>

void inverti(int v[], int from, int to);

int main(){
	int i, dim, from, to;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	int array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d numero: ", i+1);
		scanf("%d", &array[i]);
		getchar();
	}	

	printf("From: ");
	scanf("%d", &from);
	printf("To: ");
	scanf("%d", &to);

	inverti(array, from, to);
	return 0;

}

void inverti(int v[], int from, int to){
	if(from==to)
		printf("%d\n", v[to-1]);
	else{
		printf("%d\n", v[to-1]);
		inverti(v, from, to-1);
	}
}

12) La funzione di Ackermann e' una delle piu' semplici funzioni
totalmente computabili a non essere ricorsiva primitiva.
http://it.wikipedia.org/wiki/Funzione_di_Ackermann
In pratica, la funzione cresce piu' velocemente di qualsiasi
funzione ricorsiva primitiva (compreso qualsiasi esponenziale).
La funzione e' definita ricorsivamente per casi (sui naturali):
- A(m,n) = n+1 (se m=0)
- A(m,n) = A(m-1,1) (se m>0 e n=0)
- A(m,n) = A(m-1, A(m, n-1)) (se m>0 e n>0)
Scrivere una funzione che calcoli la funzione di Ackermann.
unsigned long Ackermann(unsigned long m, unsigned long n);
Quanto vale Ackermann(3,10)? Quanto vale Ackermann(4,1)?
Quanto tempo ci mette a calcolare questi valori?
Avvertenza: non andate oltre questi limiti (soprattutto su m) o
l'esecuzione
potrebbe non terminare in tempo per il fine settimana...

#include <stdio.h>

unsigned long Ackermann(unsigned long m, unsigned long n);

int main(){
	unsigned long m, n;

	printf("Inserisci m: ");
	scanf("%lu", &m);
	printf("Inserisci n: ");
	scanf("%lu", &n);

	printf("A(%lu,%lu) = %lu\n", m, n, Ackermann(m,n));
	return 0;
}

unsigned long Ackermann(unsigned long m, unsigned long n){
	if(m==0)	return n+1;

	else if(m>0 && n==0) return Ackermann(m-1,1);

	else if(m>0 && n>0) return Ackermann(m-1, Ackermann(m, n-1));
}

13) Scrivere una procedura ricorsiva che riceva un array di
caratteri e stampi la prima meta' su una riga e la seconda meta'
su una riga diversa.
Se l'array ha un numero dispari di elementi, la lettera centrale
dovra' essere stampata su una riga a parte, tra la prima meta' e
la seconda meta'.
*/

#include <stdio.h>

void stampaSuDueRighe(char vet[], int dim);

int main(){

	int i, *dim;
	printf("Lunghezza array: ");
	scanf("%d", &dim);
	getchar();

	char array[dim];

	for(i=0; i<dim; i++){
		printf("Inserisci %d carattere: ", i+1);
		scanf("%c", &array[i]);
		getchar();
	}	

	stampaSuDueRighe(array, &dim);
}

void stampaSuDueRighe(char vet[], int dim) {
  
  if(dim==0) {
    printf("\n");
    return;
  }
  
  if(dim==1) {
    printf("\n%c\n", vet[0]);
    return;
  }
  
  printf("%c ", vet[0]);
  stampaSuDueRighe(vet+1, dim-2);
  printf("%c ", vet[dim-1]);

}
/*
14) Riuscite a scrivere la funzione fibonacci ottimizzando
(riducendo al minimo) il numero di chiamate ricorsive?
Suggerimento: Per verificare quante chiamate vengono eseguite,
usate una variabile globale che viene incrementata ad ogni
chiamata.
Suggerimento 2: potete sfruttare il fatto che per calcolate F(n-1)
dovete sapere F(n-2)?
Suggerimento 3: una funzione in C non puo' ritornare piu' di un
valore ma se riceve dei puntatori puo' modificare i valori delle
variabili originali.

15) Scrivere una funzione che ricevuto un array di interi, calcoli
e stampi ricorsivamente tutte le permutazioni dell'array. 
*/