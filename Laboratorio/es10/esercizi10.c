/*
Esercitazione 10-11
Nella propria home directory creare una sottodirectory chiamata
es9, in cui
metteremo tutti i file C di oggi.
Quando dovete usare o ritornare dei valori booleani, usate la
seguente definizione:
typedef enum {false, true} boolean;
Inoltre quando un esercizio vi chiede di scrivere un "predicato",
dovete scrivere
una funzione che ritorni un valore di tipo boolean.
Ricordate che oltre alla funzione o procedura indicata dall'esercizio,
si richiede che scriviate anche un main che usi tale funzione e ne
dimostri *ogni* funzionalità.
In alcuni degli esercizi che seguono (quando richiesto) si utilizzi la
seguente definizione di tipo "lista di interi":
typedef struct El {
int info;
struct El *next;
} ElementoListaInt;
typedef ElementoListaInt* ListaDiInteri;
Inoltre, dove c'è bisogno di allocazione dinamica della memoria,
dovete assicurarvi di deallocare SEMPRE tutta la memoria allocata.
Per verificarlo potete usare valgrind se correttamente installata sul
vostro computer.
Questo strumento permette fra l'altro di capire se tutte le variabili
sono inizializzate prima del loro uso, se accediamo a memoria gia'
deallocata o mai allocata e situazioni similari
Per fare questo procedere come segue:
• compilare il file da verificare con opzione -g per includere le
informazioni di debugging. Ad esempio se il mio file si
chiama main.c posso compilare con
bash$ gcc -Wall -pedantic -g -o prova main.c
• eseguire
bash$ valgrind ./prova
in questo modo, a schermo verranno riportare le infrazioni
rilevate. Ad esempio, invalid read o invalid write sono accessi in
lettura o scrittura a memoria non allocata o gia' deallocata.
valgrind contiene moltissime opzioni, invitiamo gli studenti
interessati ad esplorarle partendo dal sito http://valgrind.org/.
Ricordate infine che dovete includere la libreria stdlib.h per la
gestione della memoria dinamica.
Esercizi introduttivi:

1) Scrivere una funzione stampaLista che ricevuta
una ListaDiInteri, la stampi a video in questo
modo:
1 -> 2 -> 3 -> 4 -> / /
Usare questa funzione per verificare le funzioni
dei prossimi esercizi.

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

void stampaLista(ListaDiElementi lista);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	stampaLista(lista);

	return 0;
}

void stampaLista(ListaDiElementi lista){
	while(lista!=NULL){
		printf("%d -> ", lista->info);
		lista = lista->next;
	}
	printf("//");
}

2)Scrivere una procedura che, data una lista di
interi, restituisca l'i-esimo elemento della lista
stessa.

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

void ElementoPos(ListaDiElementi lista, int pos);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	ElementoPos(lista, 3);

	return 0;
}

void ElementoPos(ListaDiElementi lista, int pos){
	int i=1;
	while(lista!=NULL){
		if(i==pos) printf("-> %d ", lista->info);
		else lista = lista->next;
		i++;
	}
	printf("//");
}



3)Definire una funzione (fornirne due versioni,
una iterativa e una ricorsiva) lunghezzaLista che
dat una ListaDiInteri, restituisca la sua
lunghezza.

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

void lunghezzaLista(ListaDiElementi lista);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	lunghezzaLista(lista);

	return 0;
}

void lunghezzaLista(ListaDiElementi lista){
	int i=0;
	while(lista!=NULL){
		lista = lista->next;
		i++;
	}
	printf("%d \n", i);
}

4)Si consideri una lista di interi, definire una
funzione che inserisce un valore intero in coda ad
una lista e restituisce la lista modificata.
5)Si consideri adesso lo stesso problema del punto
4) e si supponga di dover scrivere una procedura
che modifica la lista passata inserendo l'elemento
in coda.

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

void InserisciFineLista(ListaDiElementi *lista, int elem);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	InserisciFineLista(&lista, 8);

	while(lista!=NULL){
		printf("%d -> ", lista->info);
		lista = lista->next;
	}
	printf("//\n");

	return 0;
}

void InserisciFineLista(ListaDiElementi *lista, int elem){
	ListaDiElementi aux, ultimo;
	ultimo = *lista;

	aux = malloc(sizeof(ElementoLista));
	aux->info = elem;
	aux->next = NULL;

	if(*lista == NULL) *lista = aux;
	else{
		ultimo = *lista;
		while(ultimo->next!=NULL){
			ultimo=ultimo->next;
		}
		ultimo->next = aux;
	}
}

6) Si consideri una lista di interi. Scrivere una
funzione trovaIntero, che dato un intero
restituisce true se l'elemento è presente
all'interno della
lista. False in tutti gli altri casi(lista vuota,
elemento non presente). La funzione termina
immediatamente se l'intero è presente nella lista.


#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;
typedef enum{false, true} boolean;

boolean trovaIntero(ListaDiElementi lista, int elem);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	printf("%u\n", trovaIntero(lista, 9));

	return 0;
}

boolean trovaIntero(ListaDiElementi lista, int elem){
	boolean trovato = false;
	while(lista!=NULL && !trovato){
		if(lista->info==elem){trovato=true; return trovato;}
		else lista = lista->next;
	}
	return trovato;
}


7)Scrivere una funzione (una versione iterativa ed
una ricorsiva) in cui dato un valore intero x ed
una lista di interi, conti le occorrenze di x
nella lista.


#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

int contaOccorrenze(ListaDiElementi lista, int elem);
int contaOccorrenzeRicorsivo(ListaDiElementi lista, int elem);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	printf("%d\n", contaOccorrenze(lista, 1));
	printf("%d\n", contaOccorrenzeRicorsivo(lista, 1));

	return 0;
}

int contaOccorrenze(ListaDiElementi lista, int elem){
	int occorrenze=0;
	while(lista!=NULL){
		if(lista->info==elem) occorrenze++;
		lista = lista->next;
	}
	return occorrenze;
}

DA SISTEMARE:
int contaOccorrenzeRicorsivo(ListaDiElementi lista, int elem){
	int occorrenze=0;
	if(lista==NULL) return occorrenze;
	else{
		if(lista->info==elem) occorrenze++;
		contaOccorrenzeRicorsivo(lista->next, elem);
	}
}

8)Definire una procedura stampaInverso, che data
una lista di interi la stampi in maniera
invertita.

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

void stampaInverso(ListaDiElementi lista);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	stampaInverso(lista);

	return 0;
}

void stampaInverso(ListaDiElementi lista){
	if(lista!=NULL){
		stampaInverso(lista->next);
		printf("%d -> ", lista->info);
	}
}

9)Definire una funzione primoPari che data una
lista di interi, restituisca il primo elemento
pari nella lista, oppure NULL in caso di lista
vuota o senza elementi pari.


#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

int primoPari(ListaDiElementi lista);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	printf("%d \n", primoPari(lista));

	return 0;
}

int primoPari(ListaDiElementi lista){
	if(lista==NULL) return 0;
	else{
		while(lista!=NULL){
			if((lista->info) % 2==0) return lista->info;
			else if((lista->info) %2!=0){lista = lista->next;}
			else{ return 0; }
		}
	}
}

10)Definire una procedura (sia iterativa che
ricorsiva) 'elimina' che ricevuta una
ListaDiInteri e un intero X, elimini i primi X
elementi e restituisca la lista modificata.

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

void elimina(ListaDiElementi *lista, int n);
void eliminaRic(ListaDiElementi *lista, int n);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	eliminaRic(&lista, 3);

	return 0;
}

void elimina(ListaDiElementi *lista, int n){
	ListaDiElementi aux;
	int i;
	for(i=0; i<n; i++){
		aux=*lista;
		*lista=(*lista)->next;
		free(aux);
	}
}

void eliminaRic(ListaDiElementi *lista, int n){
	ListaDiElementi aux;
	if(n==0) return;
	else{
		aux=*lista;
		*lista=(*lista)->next;
		free(aux);
		eliminaRic(lista, n-1);
	}
}

11) Definire una funzione (sia iterativa che
ricorsiva) minimoPari che data una
ListaDiInteri,restituisca il minimo elemento pari
nella lista (restituisce NULL se la lista e' vuota
o non contiene
elementi pari).

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;
typedef enum{false, true} boolean;

int minimoPari(ListaDiElementi lista);
int minimoPariRic(ListaDiElementi lista);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	printf("%d\n", minimoPari(lista));
	printf("%d\n", minimoPariRic(lista));

	return 0;
}

int minimoPari(ListaDiElementi lista){
	int minimo=lista->info;
	lista = lista->next;
	while(lista!=NULL){
		if(lista->info < minimo) minimo = lista->info;
		lista = lista->next;
	}
	return minimo;
}

DA RIVEDERE:
int minimoPariRic(ListaDiElementi lista){
	int minimo;
	boolean set=false;

	if(lista==NULL) return minimo;
	else{
		if(!set){minimo=lista->info; set=true;}
		else{
			if(lista->info < minimo) minimo = lista->info;
			lista = lista->next;
			minimoPariRic(lista);
		}
	}
}


12) Definire una procedura (sia iterativa che
ricorsiva) inserisciQuarto che data una
ListaDiInteri
ed un intero P, inserisca P dopo il terzo elemento
di lista. Quest'ultima viene lasciata inalterata
se non contiene almeno tre elementi.


#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;
typedef enum{false, true} boolean;

void inserisciQuarto(ListaDiElementi *lista, int num);
void inserisciQuartoRic(ListaDiElementi *lista, int num);

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=5; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i;
		aux->next=lista;
		lista=aux;
	}

	inserisciQuartoRic(&lista, 32);
	while(lista!=NULL){
		printf("%d -> ", lista->info);
		lista = lista->next;
	}
	printf("//");
	return 0;
}

void inserisciQuarto(ListaDiElementi *lista, int num){


	ListaDiElementi aux, testa;
	int i;

	if( (aux = malloc(sizeof(ElementoLista)) ) == NULL ) return;

	testa = *lista;

	for(i=1; i<3; i++){ *lista = (*lista)->next; }

	if(*lista != NULL){
		aux->info = num;
		aux->next=(*lista)->next;
		(*lista)->next = aux; 
	}

	*lista = testa;

	return;
}

DA RIVEDERE:
void inserisciQuartoRic(ListaDiElementi *lista, int num){

	int i=0;
	ListaDiElementi aux, testa;

	if(i==0){ 
		testa = *lista;

	}
	if(i==3){
		if( (aux = malloc(sizeof(ElementoLista)) ) == NULL ) return;
		aux->info = num;
		aux->next=(*lista)->next;
		(*lista)->next = aux; 
		*lista = testa;
		return;
	}

	else{
		i++;
		inserisciQuartoRic(&((*lista)->next), num);
	}


}


13)Data una lista di interi, scrivere una
procedura che crei una nuova lista contenente solo
i valori che nella prima lista compaiono due
volte. La prima lista
deve rimanere intatta.
*/




/*
14) Definire una funzione ordinaLista che modifica
una ListaDiInteri data ordinandola in modo
crescente.
La funzione non deve usare allocazione dinamica
della memoria (malloc e free). Suggerimento: la
testa puo' cambiare? Quindi come deve essere
passata la lista alla funzione?


15) Definire una procedura 'merge' che date due
ListaDiInteri ordinate, restituisca una nuova
ListaDiInteri ordinata contenente tutti gli
elementi
delle due liste. Le liste originali devono restare
immutate.
*/