#include <stdio.h>
#include <stdlib.h>
/*
23) Scrivere una funzione stampaAlbero che ricevuta la radice di un albero, 
stampi a video una sua rappresentazione. 
Ad esempio
         1
    2          3
 4    5     6     7
8 9 10 11 12 13 14 15

-----------------------------

Mentre la soluzione di questo esercizio presentata precedentemente e' adeguata,
quella soluzione sicuramente non implementa la stampa dell'albero mostrata 
nell'esempio. 

Purtroppo per stampare l'albero in quel modo, data la linearita' dell'output a
schermo, e' necessaria una visita in ampiezza, il cui algoritmo e' tutt'altro
che banale.

In questo file quindi viene presentato un algoritmo di visita in ampiezza di 
un albero binario che usa la programmazione dinamica (ossia una struttura dati 
aggiuntiva di tipo lista) per stampare nell'ordine giusto i nodi dell'albero.

Notate che l'allienamento preciso dei nodi a schermo richiede che ogni numero
sia composta da una sola cifra.

L'algoritmo di visita in ampiezza e' il seguente: 
1) inizializza la lista con il solo nodo radice e la sua profondita' (0);
2) imposta la profondita' attuale a 0 e l'offset al numero giusto di spazi;
3) estrai il primo nodo dalla lista, chiamiamolo act;
4) aggiungi in fondo alla lista i nodi figli di act, prima il sinistro e poi il
destro, annotandoli con la profondita' di act aumentata di 1;
5) se la profondita' di act e' maggiore di quella attuale, incrementa la profondita'
attuale, dimezza l'offset e stampa un invio a capo;
6) stampa il valore del campo info di act con l'offset di spazi attuale;
7) ricomincia da 3 e continua finche' la lista non e' vuota.

Inserendo i nuovi nodi in coda alla lista, questi vengono stampati nell'ordine
giusto, livello per livello. 
L'uso della profondita' e dell'offset e' richiesto per esigenze di allineamento 
a schermo.


*/

/* Albero */
typedef struct nodoalbero {
  struct nodoalbero* sx;
  struct nodoalbero* dx;
  int info;
} nodoAlbero;
typedef nodoAlbero* AlberoBinario;

/* Per commenti su questa funzione, vedere 20-21-24-25-AlberoBinario.c */
int altezzaAlbero(AlberoBinario alb);


int pow(int a, int b) {
  int res=1,i;
  for(i=1; i<=b; i++) 
    res *= a;
  return res;
}

/* Lista di nodi di albero con annotazione di profondita' allegata */
typedef struct nodoListaNodi {
  struct nodoListaNodi* next;
  AlberoBinario nodo;
  int prof;
} NodoListaNodi;
typedef NodoListaNodi* ListaNodi;

/* Funzione d'utilita' che crea un nuovo nodo della lista con i parametri passati */
ListaNodi creaUno(AlberoBinario elem, int prof) {
    ListaNodi lista = malloc(sizeof(NodoListaNodi));
    lista->nodo = elem;
    lista->prof = prof;
    lista->next = NULL;
    return lista;
}

void stampaAlbero(AlberoBinario root) {

  int prof=0, off;
  ListaNodi last, lista, act;
  
  if(root!=NULL) {
    /* l'offset in spazi per stampare la radice */
    off = pow(2, (altezzaAlbero(root)-1)) - 1;
    
    /* la lista viene inizializzata con solo la radice */
    lista = creaUno(root, prof);
    last = lista;
    
    /* continuiamo finche' la lista non e' vuota */
    while(lista!=NULL && last!=NULL) {
    
      /* estraiamo il primo elemento della lista */
      act = lista;
      lista = lista->next;
      
      if(act->nodo!=NULL) {
      
        /* se ha i figli, li agganciamo in fondo alla lista */
        if(act->nodo->sx!=NULL) {
          last->next = creaUno(act->nodo->sx, act->prof+1);
          last = last->next;
        }
        if(act->nodo->dx!=NULL) {
          last->next = creaUno(act->nodo->dx, act->prof+1)
          last = last->next;
        }
        
        /* se questo nodo e' su una riga diversa da quella in cui stavamo stampando
        dobbiamo stampare un invio a capo, aumentare la profondita' attuale e
        dimezzare l'offset per mantenere l'allineamento */
        if(act->prof > prof) {
          printf("\n");
          prof++;
          off /= 2;
        }
        
        if(off>0) /* necessario perche' printf stampa almeno un carattere (anche con off=0) */
          printf("%*c", off, ' ');
        /* l'offset dopo l'elemento e' maggiore per tenere conto della stampa 
        dell'elemento della riga precedente */
        printf("%d%*c", act->nodo->info, (off+1), ' ');
      }
      /* abbiamo fatto tutto quello che serviva con questo nodo, possiamo liberarlo */
      free(act);
      
    }
  }
  printf("\n");
}

/* Per commenti su queste funzioni, vedere 20-21-24-25-AlberoBinario.c */
AlberoBinario creaAlbero(int arr[], int dim);
void deallocaAlbero(AlberoBinario alb);

main() {
  
  AlberoBinario root;
  int array[15] = {1,2,3,4,5,6,7,8,9,8,7,6,5,4,3};
  
  root = creaAlbero(array, 15);
  
  printf("Altezza: %d\n", altezzaAlbero(root));
  
  stampaAlbero(root);
  printf("\n");
  
  deallocaAlbero(root);
  
}


int max(int a, int b) {
  return a>b ? a : b;
}

int altezzaAlbero(AlberoBinario alb) {
  if(alb==NULL) return 0;
  return 1 + max( altezzaAlbero(alb->sx), altezzaAlbero(alb->dx) );
}

void deallocaAlbero(AlberoBinario alb) {
  if(alb==NULL) return;
  
  deallocaAlbero(alb->sx);
  deallocaAlbero(alb->dx);
  
  free(alb);
}

AlberoBinario creaAlbero_ric(int arr[], int dim, int k) {
  AlberoBinario alb = NULL;
  if((k-1)<dim) {
    alb = malloc(sizeof(nodoAlbero));
    alb->info = arr[k-1];
    alb->sx = creaAlbero_ric(arr,dim,(2*k));
    alb->dx = creaAlbero_ric(arr,dim,(2*k+1));
  }
  
  return alb;  
}

AlberoBinario creaAlbero(int arr[], int dim) {
  return creaAlbero_ric(arr,dim,1);
}
