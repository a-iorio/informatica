Laboratorio 10 

Nella propria home directory creare una sottodirectory chiamata es10, 
in cui metteremo tutti i file C di oggi. 


Note

Una stringa e' un array di caratteri TERMINATO da un carattere \0.
Questo significa che la lunghezza di una stringa e' sempre minore della lunghezza 
dell'array che la contiene, perche' in questo deve stare anche il carattere 
terminatore. Inoltre e' possibile che un array molto grande (ad es, di 100 
caratteri) contenga una stringa molto corta (o vuota): basta cambiare la posizione 
del carattere terminatore. Se, ad esempio, il \0 e' il primo carattere dell'array, 
la stringa contenuta in quell'array sara' una stringa vuota.
Essendo sempre terminate da un carattere speciale, quando una stringa viene 
passata ad una funzione non c'e' bisogno di includere la dimensione.

Ricordate inoltre che non potete usare le funzioni di libreria per le stringhe ma
implementare tale funzionalita' da soli.

Come al solito, e' richiesto che, oltre alla funzione o procedura indicata 
dall'esercizio, scriviate anche un main che usi tale funzione e ne dimostri 
*ogni* funzionalita'.

In alcuni degli esercizi che seguono (quando richiesto) si utilizzi la seguente 
definizione di tipo "lista di interi":

typedef struct El {
  int info;
  struct El *next;
} ElementoListaInt;
typedef ElementoListaInt* ListaDiInteri;

Inoltre, dove c'e' bisogno di allocazione dinamica della memoria, dovete 
assicurarvi di deallocare SEMPRE tutta la memoria allocata: un esercizio 
funzionante ma che non deallochi tutta la memoria allocata sara' considerato 
scorretto.

Per verificare che il programma 'progesempio' deallochi tutta la memoria, usare 
il comando valgrind, che invoca il programma passato e controlla che la memoria 
sia tutta libera all'uscita:
  valgrind ./progesempio

Ricordate infine che dovete includere la libreria stdlib.h per la gestione della 
memoria dinamica.



Esercizi introduttivi su stringhe

1) Scrivere un programma che chieda all'utente il proprio nome e lo salvi in una
stringa (lunghezza massima consentita 100 caratteri).
Suggerimento: in printf e scanf esiste il segnaposto %s.

2) Scrivere una funzione che ricevuta una stringa s, ritorni la sua lunghezza.

3) Scrivere una funzione che ricevuta una stringa s, ne ritorni una copia allocata
dinamicamente.
Suggerimento: una volta calcolata la lunghezza della stringa ricevuta, quanto deve
essere grande l'array che allocate dinamicamente?

4) Scrivere una funzione che ricevuta una stringa s, ritorni il numero di vocali 
nella stringa.

5) Scrivere una funzione che ricevuta una stringa s, la accorci ai suoi primi tre
caratteri. Se la stringa ha meno di tre caratteri, rimane immutata.

6) Scrivere una funzione che ricevute due stringe s e t, ritorni una stringa 
nuova ottenuta concatenando le due stringhe passate.

7) scrivere una funzione che ricevuti una stringa s e un numero k, ritorni un 
array di stringhe contenente k copie della stringa s



Esercizi su stringhe 

8) Scrivere una funzione che ricevuta una stringa, verifichi se e' palindroma, 
ritornando 1 in caso lo sia, 0 altrimenti.
  int palindroma(const char* stringa);

9) Scrivere una procedura che ricevuta una stringa, la inverta.
  void inverti(char* stringa);

10) Scrivere una funzione ricorsiva che presa una stringa, sostituisca tutte le 
occorrenze del carattere A con il carattere B.
  void replace(char *s, char A, char B);
Suggerimento: la funzione verifichi se il primo carattere e' da sostituire e quindi 
ricorra sulla parte restante del vettore, individuata da s+1.
Suggerimento 2: pensate bene a qual'e' la condizione di terminazione della ricorsione.



Esercizi su strutture dati complesse (liste bidirezionali)

11) Si consideri la definizione del tipo di dati "lista di interi" usata finora.
Ispirandosi a questa definizione, definire un nuovo tipo di dati che rappresenta 
una lista di interi bidirezionale ( ListaBidir ).
In questa lista ogni nodo dovra' avere un puntatore al nodo precedente (prev) e 
un puntatore al nodo successivo della lista (next), oltre al campo informativo 
(info, un intero).
Naturalmente se il nodo precedente o il nodo successivo non esistesse, il
corrispondente puntatore sarebbe NULL.
Si noti che a differenza delle liste semplici, per manipolare una lista 
bidirezionale basta avere un puntatore a uno qualunque dei suoi nodi, non 
necessariamente alla testa.
Scrivere quindi una funzione stampaLista che accetti e stampi una ListaBidir in 
questo modo: \\ <--> 1 <--> 2 <--> 3 <--> 4 <--> \\

12) Scrivere una procedura insInListaBidir che ha come parametri una ListaBidir 
e un intero, e inserisce l'intero nella lista.
Come quale modalita' deve essere passata la ListaBidir?

13) Definire una funzione creaListaBidir (eventualmente sfruttando insInListaBidir 
dell'esercizio precedente) che chieda all'utente di fornire una sequenza di numeri 
interi terminati da un numero negativo e restituisca una ListaBidir che contenga 
i numeri inseriti.

14) Scrivere una funzione lungListaBidir che abbia come parametro un puntatore a 
un nodo di una ListaBidir (non necessariamente il primo) e restituisca la lunghezza 
della lista.

15) Scrivere una procedura deallocaListaBidir che riceva come parametro l'indirizzo 
di un puntatore a un nodo di una ListaBidir (non necessariamente il primo) e 
deallochi tutta la lista.

16) Scrivere una funzione makeBidir che prende come parametro una ListaInt 
(la usuale lista semplice di interi) e ritorna una nuova ListaBidir contenente 
gli stessi elementi.

17) Scrivere una procedura raddoppiaLista che dopo ogni elemento della ListaBidir 
data aggiunga un nuovo elemento con lo stesso campo info del precedente. 
Esempio:
� // <--> 4 <--> 5 <--> 6 <--> 7 <--> //
deve diventare
� // <--> 4 <--> 4 <--> 5 <--> 5 <--> 6 <--> 6 <--> 7 <--> 7 <--> //



Esercizi di ricorsione su liste semplici

18) Si consideri la dichiarazione del tipo ListInt (la usuale lista semplice di 
interi). Scrivere una funzione ricorsiva bigsRic che restituisce il numero di 
elementi della lista il cui valore sia strettamente maggiore della somma di tutti 
quelli che lo precedono. Esempi:
� 1 --> // restituisce 1
� 0 --> // restituisce 0
� 1 --> 5 --> 3 --> 10 --> // restituisce 3
� 1 --> 2 --> 3 --> 4 --> // restituisce 2

19) Facendo riferimento all'esercizio 11 della scorsa esercitazione (Laboratorio 9), 
si definisca una procedura ricorsiva mergeRic che date due ListaInt ordinate, 
restituisca una ListaInt ordinata contenente tutti gli elementi delle due liste.
A differenza dell'esercizio originale, comunque, questa volta le due liste
originali possono essere modificate.



Esercizi su strutture dati complesse (alberi binari)

20) Ispirandosi alle definizioni di lista di interi e lista bidirezionale,
definire un nuovo tipo di dato albero binario AlberoBinario che contenga un intero 
(campo info) e due puntatori a nodi dell'albero, rispettivamente i sotto alberi 
sinistro (sx) e destro (dx).
Le foglie dell'albero avranno ovviamente entrambi questi campi impostati a NULL.

21) Scrivere una funzione altezzaAlbero che ricevuta la radice di un albero ne 
calcoli ricorsivamente l'altezza.

22) Scrivere una funzione ricorsiva azzera che ricevuto un albero binario, azzeri 
tutti i nodi con profondita' maggiore di 3.

23) Scrivere una funzione stampaAlbero che ricevuta la radice di un albero, 
stampi a video una sua rappresentazione. 
Ad esempio
         1
    2          3
 4    5     6     7
8 9 10 11 12 13 14 15

E' possibile farlo visitando l'albero una volta sola? E chiamando la funzione 
altezza prima della visita?
Che tipo di visita sara' necessario utilizzare?

24) Scrivere una funzione che ricevuto un albero, lo deallochi tutto correttamente.

25) Scrivere una funzione ricorsiva creaAlbero che ricevuto un array di interi,
costruisca l'albero che contiene gli stessi valori nell'ordine di una visita 
breadth-first (ampiezza).

26) Scrivere una funzione ricorsiva appiattisciAlbero che ricevuta la radice di 
un albero ritorni la lista di interi corrispondente ottenuta visitando l'albero 
con una visita in profondita' anticipata.


