#include <stdio.h>

/*
10) Scrivere una funzione ricorsiva che presa una stringa, sostituisca tutte le 
occorrenze del carattere A con il carattere B.
  void replace(char *s, char A, char B);
Suggerimento: la funzione verifichi se il primo carattere e' da sostituire e quindi 
ricorra sulla parte restante del vettore, individuata da s+1.
Suggerimento 2: pensate bene a qual'e' la condizione di terminazione della ricorsione.

*/

void replace(char *s, char A, char B) {

  /* Condizione di terminazione */
  if (s[0]=='\0') 
    return;
  
  /* Lavoro effettivo: eventuale sostituzione del primo carattere. */
  if(s[0]==A) 
    s[0]=B;
  
  /* Ricorsione sulla restante parte della stringa. */ 
  replace(s+1, A, B);

}

main() {
  
  char stringa[] = "Aureliano Rama";
  
  printf("La stringa originale e': %s\n", stringa);
  
  replace(stringa, 'a', 'z');
  
  printf("La stringa modificata e': %s\n", stringa);
  
}


