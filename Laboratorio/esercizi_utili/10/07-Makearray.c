#include <stdio.h>
#include <stdlib.h>
/*
7) scrivere una funzione che ricevuti una stringa s e un numero k, ritorni un 
array di stringhe contenente k copie della stringa s

*/

/* Per commenti su queste funzioni, vedere 02-03-LungCopiaStringa.c */
int lung(char* str);
char* copia(char* str);

/* 
  La funzione deve ritornare un array di stringhe, ossia un array di char* 
  cioe' un doppio puntatore a char, char**
  La copia vera e propria e' effettuata dalla funzione copia(), questa funzione
  si occupa solamente di creare l'array della dimensione giusta e di salvarci
  i puntatori restituiti da copia()
*/
char** makearray(char* sa, int k) {
  char** ret = NULL;
  int i;
  
  if(k>0) {
    ret = malloc(k*sizeof(char*)); /* l'array da riempire e restituire */
    for(i=0; i<k; i++) {
      ret[i] = copia(sa); /* gli elementi dell'array sono i puntatori restituiti 
                             da copia() */
    }
  }
  
  return ret;  
}

#define HOWMANY 5
main() {

  char *str = "ciao ciao", **array;
  int i;

  array = makearray(str, HOWMANY);

  for(i=0; i<HOWMANY; i++) 
    printf("Copia %d: %s\n", i, array[i]);
    
  for(i=0; i<HOWMANY; i++) 
    free(array[i]);
  
  free(array);

}

int lung(char* str) {
  int i=0;
  while(str[i]!='\0') 
    i++;
  return i;
}

char* copia(char* str) {
  int i, l;
  char *p;
  l = lung(str);
  p = malloc(sizeof(char)*(l+1));
  for(i=0; i<l; i++) {
    p[i] = str[i];
  }
  p[i]='\0';
  return p;
}

