#include <stdio.h>
#include <stdlib.h>

/*
20) Ispirandosi alle definizioni di lista di interi e lista bidirezionale,
definire un nuovo tipo di dato albero binario AlberoBinario che contenga un intero 
(campo info) e due puntatori a nodi dell'albero, rispettivamente i sotto alberi 
sinistro (sx) e destro (dx).
Le foglie dell'albero avranno ovviamente entrambi questi campi impostati a NULL.

21) Scrivere una funzione altezzaAlbero che ricevuta la radice di un albero ne 
calcoli ricorsivamente l'altezza.

24) Scrivere una funzione che ricevuto un albero, lo deallochi tutto correttamente.

25) Scrivere una funzione ricorsiva creaAlbero che ricevuto un array di interi,
costruisca l'albero che contiene gli stessi valori nell'ordine di una visita 
breadth-first (ampiezza).
*/

/*
La struttura dati e' quella ovvia che deriva dalla descrizione, un campo info intero 
e due campi puntatore, che puntano il primo al figlio sinistro, il secondo
al figlio destro.
*/
typedef struct nodoalbero {
  struct nodoalbero* sx;
  struct nodoalbero* dx;
  int info;
} nodoAlbero;
typedef nodoAlbero* AlberoBinario;

int max(int a, int b) {
  return a>b ? a : b;
}

/*
Questa semplice funzione visita l'albero in modo anticipato e ricorre sui 
sotto-alberi per calcolare l'altezza. Ovviamente l'altezza di una foglia (un albero
nullo) e' settata a 0.
*/
int altezzaAlbero(AlberoBinario alb) {
  if(alb==NULL) return 0;
  return 1 + max( altezzaAlbero(alb->sx), altezzaAlbero(alb->dx) );
}

/*
La funzione di stampa effettua una visita anticipata dell'albero e lo stampa su 
un'unica riga, usando le parentesi quadre come separatori dei sotto-alberi.

*/
void stampaAlbero(AlberoBinario alb) {
  if(alb==NULL) 
    printf("[]");
  else {
    printf("[%d ", alb->info);
    stampaAlbero(alb->sx);
    printf(",");
    stampaAlbero(alb->dx);
    printf("]");
  }
}

/*
Prima di deallocare il nodo, dobbiamo assicurarci che i suoi sotto-alberi siano
deallocati: la funzione quindi implementa una visita posticipata.
*/
void deallocaAlbero(AlberoBinario alb) {
  if(alb==NULL) return;
  
  deallocaAlbero(alb->sx);
  deallocaAlbero(alb->dx);
  
  free(alb);
}

/*
Questa funzione di creazione sfrutta il fatto che su ogni riga dell'albero ci
sono 2^h elementi (dove h e' l'altezza della riga). Quindi i figli dell'elemento
di posizione k sono quelli in posizione 2k e 2k+1. 
*/
AlberoBinario creaAlbero_ric(int arr[], int dim, int k) {
  AlberoBinario alb = NULL;
  if((k-1)<dim) {
    alb = malloc(sizeof(nodoAlbero));
    alb->info = arr[k-1];
    alb->sx = creaAlbero_ric(arr,dim,(2*k));
    alb->dx = creaAlbero_ric(arr,dim,(2*k+1));
  }
  
  return alb;  
}

/* Funzione di utilita' per chiamare correttamente la vera funzione di costruzione */
AlberoBinario creaAlbero(int arr[], int dim) {
  return creaAlbero_ric(arr,dim,1);
}

main() {
  
  AlberoBinario root;
  int array[15] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  
  root = creaAlbero(array, 15);
  
  printf("Altezza: %d\n", altezzaAlbero(root));
  
  stampaAlbero(root);
  printf("\n");
  
  deallocaAlbero(root);
  
}
