#include <stdio.h>
#include <stdlib.h>
/*
6) Scrivere una funzione che ricevute due stringe s e t, ritorni una stringa 
nuova ottenuta concatenando le due stringhe passate.

*/

/* per commenti a questa funzione, vedere 02-03-LungCopiaStringa.c */
int lung(char* str);

/* Notate l'uso di due diverse notazioni per copiare la prima e la seconda stringa
nella stringa risultato: ovviamente sarebbe stato possibile usare l'una o l'altra
in modo esclusivo senza alcun problema. */
char* concat(char* sa, char* sb) {
  int i, j, len;
  char *res;
  
  /* la lunghezza della nuova stringa e' la somma delle due lunghezze, ovviamente */
  len = lung(sa) + lung(sb);
  
  /* ma, come sempre, dobbiamo allocare un carattere in piu' per il terminatore */
  res = malloc(sizeof(char)*(len+1));
  
  /* contatore della stringa di destinazione */
  i=0;
  
  /* copio la prima stringa carattere per carattere usando la notazione per array */
  j=0;
  while(sa[j]!='\0') {
    res[i++]=sa[j];
    j++;
  }
  
  /* copio la seconda stringa carattere per carattere usando la notazione per puntatori*/
  while(sb!='\0') {
    res[i++]=*sb;
    sb++;
  }
  
  /* aggiungo il terminatore */
  res[i]='\0';
  
  return res;  
}

main() {

  char *str1 = "ciao ciao ", *str2 = "mondo mondo", *nuova;

  nuova = concat(str1, str2);

  printf("stringa concatenata: %s\n", nuova);

  free(nuova);
}

int lung(char* str) {
  int i=0;
  while(str[i]!='\0') 
    i++;
  return i;
}
