#include <stdio.h>
#include <stdlib.h>

/*
11) Si consideri la definizione del tipo di dati "lista di interi" usata finora.
Ispirandosi a questa definizione, definire un nuovo tipo di dati che rappresenta 
una lista di interi bidirezionale ( ListaBidir ).
In questa lista ogni nodo dovra' avere un puntatore al nodo precedente (prev) e 
un puntatore al nodo successivo della lista (next), oltre al campo informativo 
(info, un intero).
Naturalmente se il nodo precedente o il nodo successivo non esistesse, il
corrispondente puntatore sarebbe NULL.
Si noti che a differenza delle liste semplici, per manipolare una lista 
bidirezionale basta avere un puntatore a uno qualunque dei suoi nodi, non 
necessariamente alla testa.
Scrivere quindi una funzione stampaLista che accetti e stampi una ListaBidir in 
questo modo: \\ <--> 1 <--> 2 <--> 3 <--> 4 <--> \\

12) Scrivere una procedura insInListaBidir che ha come parametri una ListaBidir 
e un intero, e inserisce l'intero nella lista.
Come quale modalita' deve essere passata la ListaBidir?

13) Definire una funzione creaListaBidir (eventualmente sfruttando insInListaBidir 
dell'esercizio precedente) che chieda all'utente di fornire una sequenza di numeri 
interi terminati da un numero negativo e restituisca una ListaBidir che contenga 
i numeri inseriti.

14) Scrivere una funzione lungListaBidir che abbia come parametro un puntatore a 
un nodo di una ListaBidir (non necessariamente il primo) e restituisca la lunghezza 
della lista.

15) Scrivere una procedura deallocaListaBidir che riceva come parametro l'indirizzo 
di un puntatore a un nodo di una ListaBidir (non necessariamente il primo) e 
deallochi tutta la lista.

*/


typedef struct nodobidir {
  int info;
  struct nodobidir *prev;
  struct nodobidir *next;
} NodoBidir;

typedef NodoBidir *ListaBidir;

/*
  Questa funzione aggiunge un nuovo nodo alla lista passata.
  Poiche' e' una lista bidirezionale, l'aggiunta potrebbe avvenire sia prima che 
  dopo il nodo puntato da plista. 
  Qui si e' scelto di aggiungerlo dopo: cosi' facendo, questa funzione implementa 
  un inserimento in coda se viene ripetutamente chiamata sullo stesso puntatore.
*/
void insInListaBidir(ListaBidir* plista, int P) {
  /* Se plista e' NULL, c'e' qualcosa di molto sbagliato e non facciamo nulla:
     non dovrebbe succedere mai a meno di non passare esplicitamente NULL.
     Comunque poiche' e' tecnicamente possibile, bisogna controllare. */
  ListaBidir nuovo;
  
  if(plista != NULL) {
    nuovo = malloc(sizeof(NodoBidir));
    nuovo->info = P;
    
    if(*plista != NULL) {
      /* questo e' il caso di lista preesistente non vuota
         e nuovo viene agganciato dopo quello passato */
      nuovo->prev = *plista;
      nuovo->next = (*plista)->next;
      
      /* se il successivo esiste, ci agganciamo il nodo nuovo */ 
      if( (*plista)->next != NULL) 
        (*plista)->next->prev = nuovo;
      
      (*plista)->next = nuovo;
        
    }
    else {
      /* questo e' il caso di lista preesistente vuota 
         e quindi nuovo diventa l'unico elemento */
      nuovo->prev = NULL;
      nuovo->next = NULL;
    }
    
    /*
      Questo aggiorna il puntatore passato: in realta' servirebbe solo nel caso 
      di lista vuota in cui alla fine plista deve puntare al nuovo elemento.
      Negli altri casi, essendo agganciato correttamente a elementi gia' esistenti, 
      non sarebbe necessario. Ovviamente pero' il comportamento della procedura 
      (nonche' il tipo dei suoi parametri) deve essere coerente, e quindi lo 
      facciamo in tutti i casi.
    */
    *plista = nuovo;
  }
}

/*
  Funzione molto semplice che chiede all'utente di inserire valori interi
  (terminati da un numero negativo) e usa la precedente funzione per creare e 
  quindi ritornare una lista con questi valori.
*/
ListaBidir creaListaBidir() {
  int p;
  ListaBidir testa=NULL, cursore=NULL;
  
  do {
    scanf("%d", &p);
    
    if(p>=0) {
      /* 
        Notare che passando cursore per indirizzo, il nodo puntato cambia ogni volta.
        Questo, unito al fatto che insInListaBidir inserisce dopo il nodo passato,
        provoca un inserimento in coda (cursore punta sempre all'ultimo elemento della lista).
      */
      insInListaBidir(&cursore, p);
      if(testa==NULL)
        testa = cursore;
    }
    
  } while(p>=0);
  
  return testa;
}

/*
  Queste funzione dealloca la lista bidirezionale.
  Per farlo:
  - si salva i puntatori al precedente e al successivo del nodo passato
  - dealloca il nodo passato
  - dal precedente, risale la lista fino alla testa deallocando ogni nodo via via
  - dal successivo, scende la lista fino alla coda deallocando ogni nodo via via
*/
void deallocaListaBidir(ListaBidir nodo) {
  ListaBidir up, down, temp;
  if(nodo!=NULL) {
    /* salvo i puntatori al precedente e al successivo del nodo passato */
    up = nodo->prev;
    down = nodo->next;
    /* dealloco il nodo passato */
    free(nodo);
    
    /* dal precedente, risalgo la lista fino alla testa deallocando ogni nodo via via */
    while(up!=NULL) {
      temp = up->prev;
      free(up);
      up = temp;
    }    
    
    /* dal successivo, scendo la lista fino alla coda deallocando ogni nodo via via */
    while(down!=NULL) {
      temp = down->next;
      free(down);
      down = temp;
    }
  }
}

/*
  Questa funzione risale la lista a partire dal nodo passato fino alla testa
  e quindi la scorre fino alla coda stampando tutti i nodi.
  Devo risalire fino all'inizio per poter stampare correttamente la lista come 
  richiesto.
*/
void stampaListaBidir(ListaBidir nodo) {
  ListaBidir testa;
  if(nodo!=NULL) {
    
    /* risalgo fino all'inizio */
    testa = nodo;
    while(testa->prev != NULL)
      testa = testa->prev;
    
    /* stampo il primo elemento */
    printf("// <-- %d ", testa->info);
    testa = testa->next;
    
    /* stampo tutti i successivi */
    while (testa != NULL) {
      printf("<--> %d ", testa->info);
      testa = testa->next;
    }
    /* e quindi chiudo la stampa */
    printf("--> //\n");
  }
}


int main() {
  
  ListaBidir l = creaListaBidir();
  
  printf("Stampo la lista dalla testa: ");
  stampaListaBidir(l);

  printf("Stampo la lista dal successivo: ");
  stampaListaBidir(l->next);

  printf("Stampo la lista dal successivo ancora: ");
  stampaListaBidir(l->next->next);

  deallocaListaBidir(l);
  
  return 0;
}
