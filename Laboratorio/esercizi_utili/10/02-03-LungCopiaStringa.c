#include <stdio.h>
#include <stdlib.h>
/*
2) Scrivere una funzione che ricevuta una stringa s, ritorni la sua lunghezza.

3) Scrivere una funzione che ricevuta una stringa s, ne ritorni una copia allocata
dinamicamente.
Suggerimento: una volta calcolata la lunghezza della stringa ricevuta, quanto deve
essere grande l'array che allocate dinamicamente?

*/

/* La funzione scorre la stringa finche' non trova il terminatore, contanto i 
caratteri via via. */
int lung(char* str) {
  int i=0;
  while(str[i]!='\0') 
    i++;
  return i;
}

/* La funzione calcola lo spazio necessario per la copia (compreso il terminatore),
lo alloca e copia carattere per carattere la vecchia stringa nella nuova. Alla fine
aggiunge il terminatore alla nuova stringa. */
char* copia(char* str) {
  int i, len;
  char *res;
  len = lung(str);
  res = malloc(sizeof(char)*(len+1));
  for(i=0; i<len; i++) {
    res[i] = str[i];
  }
  res[i]='\0';
  return res;
}

main() {

  char nome[101], *cp;

  scanf("%100s", nome);

  printf("lunghezza: %d\n", lung(nome));

  cp = copia(nome);

  printf("copia: %s\n", cp);

  free(cp);
}
