#include <stdio.h>

/*
1) Scrivere un nuovo tipo di dato coppia di interi. 
Inizializzare tre istanze di coppie con i primi tre numeri naturali e i loro doppi.

*/

typedef struct coppia {
  int x;
  int y;
} Coppia;

main(){
  Coppia a, b ,c;

  a.x=1;
  a.y=2;

  b.x=2;
  b.y=4;

  c.x=3;
  c.y=6;

}

