#include <stdio.h>
#include <stdlib.h>

/*
11) Definire una procedura 'merge' che date due ListaDiInteri ordinate, restituisca 
una nuova ListaDiInteri ordinata contenente tutti gli elementi delle due liste.
Le liste originali devono restare immutate.

*/


typedef struct El {
  int info;
  struct El *next;
}  ElementoListaInt;
typedef ElementoListaInt *ListaDiInteri;


/*   Per i commenti su ordinaLista, vedere 09-ordinaLista.c */
ListaDiInteri ordinaLista(ListaDiInteri lista);


/*
  Questa funzione ausiliaria crea un nuovo elemento inizializzandolo con lo stesso
  campo info del primo elemento della lista passata e poi lo ritorna.
*/
ListaDiInteri dupElement(ListaDiInteri el) {
  ListaDiInteri ret = NULL;
  if(el!=NULL) {
    ret = malloc(sizeof(ElementoListaInt));
    ret->info=el->info;
    ret->next=NULL;
  }
  return ret;
}

/*
  Questa funzione prima di tutto duplica e concatena le due liste passate e quindi
  ritorna la nuova lista, dopo averla ordinata.
*/
ListaDiInteri merge(ListaDiInteri l1, ListaDiInteri l2) {
  
  ListaDiInteri ret=NULL, aux, last=NULL;
  
  /* copiamo la prima lista */
  while(l1!=NULL) {
    aux = dupElement(l1);
    
    if(ret==NULL) /* prima iterazione, salviamo la testa */
      ret=aux;
    else /* se non e' la prima iterazione, last e' sicuramente settato */
      last->next = aux;
    
    /* aggiorniamo l'ultimo e avanziamo la lista */
    last = aux;
    l1 = l1->next;
  }
  
  /* copiamo la seconda lista */
  while(l2!=NULL) {
    aux = dupElement(l2);
    
    if(ret==NULL) /* prima iterazione, la lista l1 era vuota, salviamo la testa */
      ret=aux;
    else /* se non e' la prima iterazione, last e' sicuramente settato */
      last->next = aux;
    
    /* aggiorniamo l'ultimo e avanziamo la lista */
    last = aux;
    l2 = l2->next;
  }
  
  /* ret contiene tutti gli elementi delle due liste originali e va quindi ordinata. */
  return ordinaLista(ret);
}


/*
  Per i commenti su queste funzioni di base, vedere 01-02-03-ListaBase.c
*/
ListaDiInteri creaListaInTesta();
ListaDiInteri creaListaInCoda();
void deallocaLista(ListaDiInteri testa);
void stampaLista(ListaDiInteri lista);


int main() {

  ListaDiInteri lista1, lista2, lista3;

  lista1 = creaListaInCoda();
  printf("Lista 1: ");
  stampaLista(lista1);
  
  lista2 = creaListaInCoda();
  printf("Lista 2: ");
  stampaLista(lista2);
  
  lista3 = merge(lista1, lista2);
  printf("Lista 3: ");
  stampaLista(lista3);
  
  printf("Lista 1: ");
  stampaLista(lista1);
  printf("Lista 2: ");
  stampaLista(lista2);

  deallocaLista(lista1);
  deallocaLista(lista2);
  deallocaLista(lista3);

  return 0;

}

ListaDiInteri creaListaInTesta() {
  ListaDiInteri testa=NULL, nuovo;
  int intero;

  do {
    printf("Inserire il prossimo elemento della lista: ");
    scanf("%d", &intero);
    if (intero>=0) {
      nuovo = malloc(sizeof(ElementoListaInt));
      nuovo->info = intero;
      nuovo->next = testa;
      testa = nuovo;
    }
  } while(intero>=0);

  return testa;
}

ListaDiInteri creaListaInCoda() {
  ListaDiInteri testa=NULL, last=NULL, nuovo;
  int intero;

  do {
    printf("Inserire il prossimo elemento della lista: ");
    scanf("%d", &intero);
    if (intero>=0) {
      nuovo = malloc(sizeof(ElementoListaInt));
      nuovo->info = intero;
      nuovo->next = NULL;

      if(last!=NULL)
        last->next = nuovo;
      else
        testa = nuovo;

      last = nuovo;

    }
  } while(intero>=0);

  return testa;
}

void deallocaLista(ListaDiInteri testa) {
  ListaDiInteri temp;
  while(testa!=NULL) {
    temp = testa->next;
    free(testa);
    testa = temp;
  }
}

void stampaLista(ListaDiInteri lista)
{
  while (lista != NULL)
  {
    printf("%d -->", lista->info);
    lista = lista->next;
  }
  printf("//\n");
}

ListaDiInteri ordinaLista(ListaDiInteri lista) {
  ListaDiInteri ret; /* nuova testa, elemento di ritorno */
  ListaDiInteri act, prec; /* elemento attuale e suo precedente */
  ListaDiInteri aux, paux; /* cursore di ricerca e suo precedente */
  ListaDiInteri min, pmin; /* elemento minimo e suo precedente */
  ListaDiInteri temp; /* temporaneo per lo swap */
  
  /* iniziamo dalla testa, ovviamente */
  ret = NULL;
  act = lista;
  prec = NULL;
  
  while(act!=NULL) {
    
    /* Il minimo e' il primo elemento, all'inizio */
    min = act;
    pmin = prec;

    /* e quindi il cursore con cui cerchiamo il nuovo minimo parte dal successivo */
    paux = act;
    aux = act->next; /* esiste perche' siamo entrati nello while */

    
    /* scansione della lista per ricerca del nuovo minimo */
    while(aux!=NULL) {
      if(aux->info < min->info) {
        min = aux;
        pmin = paux;
      }
      /* Scorriamo il cursore sull'elemento successivo, questo possibile perche' 
      siamo entrati nello while. Equivale ad i++ nei vettori. */
      paux = aux;
      aux = aux->next;
    }
    
    /* se abbiamo trovato un nuovo minimo, lo scambiamo con act */ 
    if(min!=act && min!=NULL && act!=NULL) {
      /* sistemo i precedenti */
      if(prec!=NULL)
        prec->next = min;
      
      if(pmin!=NULL)
        pmin->next = act;
      
      /* swap dei next di min e act*/
      temp = min->next;
      min->next = act->next;
      act->next = temp;
      
      /* e quindi adesso min e' il nuovo elemento corrente */
      act=min;
      
    }
    
    /* la prima volta che arriviamo qui act e' il minimo assoluto, quindi e' la 
    nuova testa della futura lista ordinata e quindi la salviamo */
    if(ret==NULL)
      ret = act;
    
    /* alla fine iteriamo a partire dall'elemento seguente  */
    prec = act;
    if(act!=NULL)
      act = act->next;
    
  }
  
  return ret;
}

