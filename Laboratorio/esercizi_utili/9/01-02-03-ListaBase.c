#include <stdio.h>
#include <stdlib.h>
/*
1) Scrivere una funzione stampaLista che ricevuta una ListaDiInteri, la stampi a 
video in questo modo:
1 -> 2 -> 3 -> 4 -> //
Usare questa funzione per verificare le funzioni dei prossimi esercizi. 

2) Definire una funzione creaLista che legga dal'utente i numeri che compongono 
una lista ListaDiInteri e restituisca al programma chiamante un puntatore al 
primo elemento della lista. 
La lista e' di lunghezza indefinita, la lettura termina quando l'utente 
inserisce un numero negativo. 
Se l'utente inserisce come primo numero un numero negativo, la funzione dovra'
restituire una lista vuota (un puntatore inizializzato a NULL).
Usare questa funzione per testare le funzioni dei prossimi esercizi.

3) Definire una funzione deallocaLista che riceve una ListaDiInteri e la dealloca 
completamente. 
Usare questa funzione per la deallocazione delle liste nei prossimi esercizi.

*/

typedef struct El {
  int info;
  struct El *next;
} ElementoListaInt;
typedef ElementoListaInt* ListaDiInteri;


/*
  Questa funzione scorre la lista, tenendo da parte ogni volta il puntatore al
  successivo e liberando la testa, che viene poi aggiornata all'elemento successivo
  tenuto da parte.
*/
void deallocaLista(ListaDiInteri testa) {
  ListaDiInteri temp;
  while(testa!=NULL) {
    temp = testa->next;
    free(testa);
    testa=temp;
  }
}

/*
  Questa funzione crea la lista aggiungendo ogni volta il nuovo valore in testa
  alla lista gia' creata e quindi costruendo una lista invertita rispetto all'ordine
  di inserimento.
*/
ListaDiInteri creaListaInTesta() {
  ListaDiInteri testa = NULL, aux;
  int n;

  do {
    scanf("%d", &n);
    if(n>=0) {
      aux = malloc(sizeof(ElementoListaInt));
      aux->info = n;
      aux->next = testa; /* agganciato in testa */
      testa = aux; /* e quindi diventa la nuova testa */
    }
  } while(n>=0);

  return testa;
}

/*
  Questa funzione crea la nuova lista aggiungendo ogni volta il nuovo elemento
  in coda alla lista gia' creata. In questo modo la lista risultante ha lo 
  stesso ordine di inserimento ma per farlo deve tenere traccia dell'ultimo 
  elemento, oltre che della testa.
*/
ListaDiInteri creaListaInCoda() {
  ListaDiInteri testa=NULL, ultimo=NULL, aux;
  int n;

  do {
    scanf("%d", &n);
    if(n>=0) {
      aux = malloc(sizeof(ElementoListaInt));
      aux->info = n;
      aux->next = NULL;

      if(ultimo!=NULL) { /* abbiamo gia' inserito un elemento nella lista */
        ultimo->next=aux; /* quello nuovo viene quindi agganciato a questo */
        ultimo = aux;/* e poi diventa il nuovo ultimo */
      }
      else {
        testa = aux; /* altrimenti il primo elemento e' contemporaneamente il primo */ 
        ultimo = aux; /* e l'ultimo della lista */
      }
    }
  } while(n>=0);

  return testa;
}

/* scorre la testa dall'inizio alla fine (NULL), stampando ciascun elemento */
void stampaLista(ListaDiInteri testa) {

  while(testa!=NULL) {
    printf("%d -> ", testa->info);
    testa = testa->next;
  }
  printf("//\n");

}

main() {
  ListaDiInteri lista1, lista2;
  
  printf("Inserisci la prima lista (-1 per terminare):\n");
  lista1 = creaListaInTesta();
  stampaLista(lista1);

  printf("\nInserisci la seconda lista (-1 per terminare):\n");
  lista2 = creaListaInCoda();
  stampaLista(lista2);
  
  deallocaLista(lista1);
  
  deallocaLista(lista2);
}
