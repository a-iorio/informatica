#include <stdio.h>
#include <stdlib.h>
/*
6) Definire una procedura (sia iterativa che ricorsiva) 'elimina' che ricevuta una 
ListaDiInteri e un intero X, elimini i primi X elementi e ritorni il puntatore 
alla testa della lista modificata.
Suggerimento: dovendo modificare la testa della lista, come deve essere passata 
la lista alla funzione?
------------
Come al solito, quando dobbiamo modificare la testa della lista abbiamo due 
possibilita': ricevere la testa per riferimento e modificarle se serve, oppure
riceverla per valore e ritornare la nuova testa, modificata o meno che sia.

Abbiamo quindi quattro versioni di questa funzione: due iterative (una per valore
e una per riferimento) e due ricorsive (di nuovo, una per valore e una per 
riferimento).

*/

typedef struct El {
  int info;
  struct El *next;
}  ElementoListaInt;
typedef ElementoListaInt *ListaDiInteri;


/*
  Questa funzione scorre iterativamente la lista, salvandosi di volta in volta 
  il puntatore al successivo e quindi liberando la preesistente testa. 
  Quando ne ha liberati il numero richiesto, ritorna la nuova testa al chiamante.
*/
ListaDiInteri elimina_iter(ListaDiInteri testa, int x) {
  ListaDiInteri aux;
  
  while(testa!=NULL && x>0) {
    aux = testa->next;
    free(testa);
    testa = aux;
    x--;
  }
  
  return testa;
}

/*
  Questa e' la stessa funzione di prima, modificata per ricevere la testa per 
  riferimento e modificarla direttamente, invece di ritornare la nuova testa.
  Notate il nuovo controllo sulla validita' del riferimento passato e 
  l'aggiornamento del riferimento quando si modifica la testa.
*/
void elimina_iter_riferimento(ListaDiInteri* p_testa, int x) {
  ListaDiInteri aux, testa;
  if(p_testa!=NULL) {
    testa = *p_testa;
    while(testa!=NULL && x>0) {
      aux = testa->next;
      free(testa);
      testa = aux;
      *p_testa = aux;
      x--;
    }
  }
}


/*
  Questa funzione ricorsiva libera il primo elemento se e solo se gli viene 
  passato un numero maggiore di 0.
  In questo caso si salva il puntatore al successivo, elimina la testa e ricorre 
  passando il nuovo puntatore e il valore x decrementato.
  Quando invece gli viene passato 0 ritorna semplicemente la testa della lista 
  passata, che viene quindi ritornata da tutte le chiamate precedenti, chiudendo
  la ricorsione.  
*/
ListaDiInteri elimina_ric(ListaDiInteri testa, int x) {

  ListaDiInteri aux;

  if(x>0 && testa!=NULL) {
    aux = testa->next;
    free(testa);
    return elimina_ric(aux, x-1);
  }
  
  return testa;
}

/*
  Questa e' la stessa funzione di prima, modificata per ricevere la testa per 
  riferimento e modificarla direttamente, invece di ritornare la nuova testa.
  Notate il nuovo controllo sulla validita' del riferimento passato e 
  l'aggiornamento del riferimento quando si modifica la testa.
*/
void elimina_ric_riferimento(ListaDiInteri* p_testa, int x) {

  ListaDiInteri aux;
  
  if(x>0 && p_testa!=NULL && (*p_testa)!=NULL) {
    aux = (*p_testa)->next;
    free(*p_testa);
    *p_testa = aux;
    elimina_ric_riferimento(p_testa, x-1);
  }
}


/*
  Per i commenti su queste funzioni di base, vedere 01-02-03-ListaBase.c
*/
ListaDiInteri creaListaInTesta();
ListaDiInteri creaListaInCoda();
void deallocaLista(ListaDiInteri testa);
void stampaLista(ListaDiInteri lista);


int main() {
  
  ListaDiInteri lista = creaListaInCoda();

  stampaLista(lista);
  
  lista = elimina_iter(lista, 2);

  stampaLista(lista);
  
  elimina_iter_riferimento(&lista, 2);

  stampaLista(lista);

  lista = elimina_ric(lista, 3);
  
  stampaLista(lista);

  elimina_ric_riferimento(&lista, 3);
  
  stampaLista(lista);

  deallocaLista(lista);

  return 0;

}

void deallocaLista(ListaDiInteri testa) {
  ListaDiInteri temp;
  while(testa!=NULL) {
    temp = testa->next;
    free(testa);
    testa=temp;
  }
}

ListaDiInteri creaListaInTesta() {
  ListaDiInteri testa = NULL, aux;
  int n;

  do {
    scanf("%d", &n);
    if(n>=0) {
      aux = malloc(sizeof(ElementoListaInt));
      aux->info = n;
      aux->next = testa;
      testa = aux;
    }
  } while(n>=0);

  return testa;
}

ListaDiInteri creaListaInCoda() {
  ListaDiInteri testa=NULL, ultimo=NULL, aux;
  int n;

  do {
    scanf("%d", &n);
    if(n>=0) {
      aux = malloc(sizeof(ElementoListaInt));
      aux->info = n;
      aux->next = NULL;

      if(ultimo!=NULL) {
        ultimo->next=aux;
        ultimo = aux;
      }
      else {
        testa = aux;
        ultimo = aux;
      }
    }
  } while(n>=0);

  return testa;
}

void stampaLista(ListaDiInteri testa) {

  while(testa!=NULL) {
    printf("%d -> ", testa->info);
    testa = testa->next;
  }
  printf("//\n");

}



