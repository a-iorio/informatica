#include <stdio.h>
#include <stdlib.h>

/*
9) Definire una funzione ordinaLista che modifica una ListaDiInteri data 
ordinandola in modo crescente.
La funzione non deve usare allocazione dinamica della memoria (malloc e free).
Suggerimento: la testa puo' cambiare? Quindi come deve essere passata la lista 
alla funzione?
-----
Anche qui e' possibile fare due scelte, su come passare la lista alla funzione,
passarla per riferimento e modificare la testa direttamente o passarla per valore
e ritornare la nuova testa.
Questa soluzione implementa la seconda di queste scelte ma come e' possibile 
vedere dalla soluzione dell'esercizio 6, e' molto facile passare da un caso
all'altro.

*/


typedef struct El {
  int info;
  struct El *next;
}  ElementoListaInt;
typedef ElementoListaInt *ListaDiInteri;

/*
  La funzione ordinaLista implementa un classico algoritmo di ordinamento 
  (selection sort) con complessita' O(n^2). 
  L'algoritmo e' lo stesso usato in precedenza per ordinare vettori:
    ad ogni iterazione, a partire dall'inizio
      1) trova l'elemento minore a destra dall'elemento attuale
      2) lo scambia con l'elemento attuale
      3) avanza la posizione attuale  
  Poiche' qui abbiamo a che fare con liste invece che con vettori, per ogni 
  elemento (attuale, minimo, cursore) dovremo tenerci sia il puntatore 
  all'elemento che quello al suo precedente.
  Inoltre ci serve di tenerci da parte la testa della lista riordinata. 
  E un puntatore temporaneo per lo swap.
  Totale: 8 puntatori. 
  E' sicuramente possibile scrivere una versione assai piu' compatta di questa
  ma in questo modo vengono messe in evidenza le similarita' con l'algoritmo 
  originale su vettori.
*/
ListaDiInteri ordinaLista(ListaDiInteri lista) {
  ListaDiInteri ret; /* nuova testa, elemento di ritorno */
  ListaDiInteri act, prec; /* elemento attuale e suo precedente */
  ListaDiInteri aux, paux; /* cursore di ricerca e suo precedente */
  ListaDiInteri min, pmin; /* elemento minimo e suo precedente */
  ListaDiInteri temp; /* temporaneo per lo swap */
  
  /* iniziamo dalla testa, ovviamente */
  ret = NULL;
  act = lista;
  prec = NULL;
  
  while(act!=NULL) {
    
    /* Il minimo e' il primo elemento, all'inizio */
    min = act;
    pmin = prec;

    /* e quindi il cursore con cui cerchiamo il nuovo minimo parte dal successivo */
    paux = act;
    aux = act->next; /* esiste perche' siamo entrati nello while */

    
    /* scansione della lista per ricerca del nuovo minimo */
    while(aux!=NULL) {
      if(aux->info < min->info) {
        min = aux;
        pmin = paux;
      }
      /* Scorriamo il cursore sull'elemento successivo, questo possibile perche' 
      siamo entrati nello while. Equivale ad i++ nei vettori. */
      paux = aux;
      aux = aux->next;
    }
    
    /* se abbiamo trovato un nuovo minimo, lo scambiamo con act */ 
    if(min!=act && min!=NULL && act!=NULL) {
      /* sistemo i precedenti */
      if(prec!=NULL)
        prec->next = min;
      
      if(pmin!=NULL)
        pmin->next = act;
      
      /* swap dei next di min e act*/
      temp = min->next;
      min->next = act->next;
      act->next = temp;
      
      /* e quindi adesso min e' il nuovo elemento corrente */
      act=min;
      
    }
    
    /* la prima volta che arriviamo qui act e' il minimo assoluto, quindi e' la 
    nuova testa della futura lista ordinata e quindi la salviamo */
    if(ret==NULL)
      ret = act;
    
    /* alla fine iteriamo a partire dall'elemento seguente  */
    prec = act;
    if(act!=NULL)
      act = act->next;
    
  }
  
  return ret;
}

/*
  Per i commenti su queste funzioni di base, vedere 01-02-03-ListaBase.c
*/
ListaDiInteri creaListaInTesta();
ListaDiInteri creaListaInCoda();
void deallocaLista(ListaDiInteri testa);
void stampaLista(ListaDiInteri lista);


int main() {

  ListaDiInteri lista = creaListaInCoda();

  printf("Lista originale: ");
  stampaLista(lista);
  
  lista = ordinaLista(lista);
  
  printf("Lista ordinata: ");
  stampaLista(lista);

  deallocaLista(lista);

  return 0;

}

ListaDiInteri creaListaInTesta() {
  ListaDiInteri testa=NULL, nuovo;
  int intero;

  do {
    printf("Inserire il prossimo elemento della lista: ");
    scanf("%d", &intero);
    if (intero>=0) {
      nuovo = malloc(sizeof(ElementoListaInt));
      nuovo->info = intero;
      nuovo->next = testa;
      testa = nuovo;
    }
  } while(intero>=0);

  return testa;
}

ListaDiInteri creaListaInCoda() {
  ListaDiInteri testa=NULL, last=NULL, nuovo;
  int intero;

  do {
    printf("Inserire il prossimo elemento della lista: ");
    scanf("%d", &intero);
    if (intero>=0) {
      nuovo = malloc(sizeof(ElementoListaInt));
      nuovo->info = intero;
      nuovo->next = NULL;

      if(last!=NULL)
        last->next = nuovo;
      else
        testa = nuovo;

      last = nuovo;

    }
  } while(intero>=0);

  return testa;
}

void deallocaLista(ListaDiInteri testa) {
  ListaDiInteri temp;
  while(testa!=NULL) {
    temp = testa->next;
    free(testa);
    testa = temp;
  }
}

void stampaLista(ListaDiInteri lista)
{
  while (lista != NULL)
  {
    printf("%d -->", lista->info);
    lista = lista->next;
  }
  printf("//\n");
}



