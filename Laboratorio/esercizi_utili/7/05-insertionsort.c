#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
5) Scrivere una funzione che riceva un array di interi e lo ordini, usando 
l'algoritmo di Insertion Sort.
----------

*/

#define LUNG 100

void insertionsort(int vet[], int dim);

/*
  Un main semplice che crea un vettore casuale, lo stampa, lo ordina e poi lo stampa nuovamente.
*/
main() {
  int vett[LUNG], i, n;

  srand(time(NULL));

  printf("Inserire la lunghezza dell'array (max %d): ", LUNG);
  i=scanf("%d", &n);
  if(i==0 || n>LUNG) return;

  for(i=0; i<n; i++) {
    vett[i]=rand()%100;
    printf("%d ", vett[i]);
  }
  printf("\n");


  insertionsort(vett, n);

  for(i=0; i<n; i++)
    printf("%d ", vett[i]);
  printf("\n");

}


/*
Funzione per eseguire lo shift verso destra degli elementi di un array.
Ritorna l'elemento che "sfora" ossia l'ultimo elemento dell'array originale
Il primo elemento rimane immutato (e quindi e' presente in due copie dopo lo shift).
*/
int shift_right(int vet[], int dim) {
  int i, temp=vet[dim-1];
  for(i=dim-1; i>0; i--) 
    vet[i]=vet[i-1];
  return temp;
}

/*
La funzione insertionsort implementa l'algoritmo omonimo.
Per ogni elemento dell'array, cerca nella parte precedente dell'array la sua 
posizione.
Quindi in ogni momento l'array e' diviso in due parti, la parte a sinistra,
prima dell'indice i, che e' gia' ordinata e la parte a destra che e' ancora
da ordinare.
Quando i raggiunge la fine dell'array, l'array e' completamente ordinato.
*/
void insertionsort(int vet[], int dim) {
  int i,j, temp;

  for(i=1; i<dim; i++) {
    
    j=0; 
    while(j<i && vet[j]<vet[i]) {
      j++;
    }
    
    if(j<i) {
      temp=shift_right(vet+j, i-j+1); /* shifto gli elementi maggiori a destra */
      vet[j]=temp; /* e sistemo il nuovo elemento alla posizione giusta */
    }
    
    
  }

}

