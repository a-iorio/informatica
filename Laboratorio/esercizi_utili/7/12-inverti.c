#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
12) Scrivere una funzione ricorsiva che, dato un array di interi, inverta le 
posizioni dei suoi elementi.
Ad esempio l'invocazione inverti(a), dove a e' l'array {1, 2, 3, 4}, deve 
modificare l'array in {4, 3, 2, 1}. 
----------

Un esercizio molto semplice in cui dobbiamo ricorrere sul sotto-array dato 
dall'array originale meno gli estremi, che sono stati scambiati e sono quindi
gia' corretti.

*/

#define LUNG 20

void inverti(int vet[], int dim);

/*
  Un main semplice che crea un vettore casuale, lo stampa, lo inverte e poi lo stampa nuovamente.
*/
main() {
  int vett[LUNG], i, n;

  srand(time(NULL));

  printf("Inserire la lunghezza dell'array (max %d): ", LUNG);
  i=scanf("%d", &n);
  if(i==0 || n>LUNG) return;

  for(i=0; i<n; i++) {
    vett[i]=rand()%100;
    printf("%d ", vett[i]);
  }
  printf("\n");


  inverti(vett, n);

  for(i=0; i<n; i++)
    printf("%d ", vett[i]);
  printf("\n");

}

/*
*/
void inverti(int vet[], int dim) {
  int temp;
  
  if(dim<2) return;
  
  /* scambiamo il primo e l'ultimo elemento dell'array
  */
  temp=vet[0];
  vet[0]=vet[dim-1];
  vet[dim-1]=temp;
  
  /* ricorriamo sull'array escluso il primo e l'ultimo elemento */
  inverti(vet+1, dim-2);

}

