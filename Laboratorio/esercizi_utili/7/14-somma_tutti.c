#include <stdio.h>
/*
14) Scrivere una funzione ricorsiva che, ricevuti due interi N1 ed N2,
restituisca la somma di tutti gli interi compresi tra N1 ed N2.
----------

Esercizio alquanto ovvio, basta stare attenti a qual'e' il valore maggiore 
inviato alla funzione. 

*/

int somma_tutti(int n1, int n2);

/*
  Un main semplice che chiede due valori all'utente e li passa alla funzione
*/
main() {
  int a,b,i;


  printf("Inserire il primo intero");
  i=scanf("%d", &a);
  if(i==0) return;

  printf("Inserire il secondo intero");
  i=scanf("%d", &b);
  if(i==0) return;

  somma_tutti(a,b);
  
}


int somma_tutti(int n1, int n2){

  if(n1==n2) return 0;
  
  if(n1>n2) return n1+somma_tutti(n1-1, n2);
  
  if(n2>n1) return n2+somma_tutti(n1, n2-1);

  return 0;
}

