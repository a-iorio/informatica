#include <stdio.h>
/*
4) Scrivere una funzione ricorsiva che riceva un intero e stampi ricorsivamente 
un numero corrispondente di punti '.'

15) Riprendere l'esercizio 4. Riuscite a far stampare ricorsivamente punti '.' 
alternati a '_'? E punti '.' alternati a '_' e a '='?
Qual'e' il metodo generale? 
Esempio di interazione col programma
  Inserire un numero: 7
  7 -> ._._._.

  Inserire un numero: 9
  9 -> ._.=._.=.
----------

Notate che la stampa deve sempre partire con un punto: questo vi costringe a una 
fra queste due soluzioni:
 - ricorrere sempre sulla chiusura, invece che sull'apertura, altrimenti non 
 siete in grado di dire quando siete all'inizio;
 - usare una funzione ausiliaria che chiami lo worker (la funziona che fa il 
 lavoro vero e proprio) con un parametro in piu', inizialmente settato a 0.
 
Nel primo caso abbiamo una funzione sola ma essendo invertita non pu' stampare
l'invio a capo, che deve essere stampato dal main.
Nel secondo caso la funzione fa tutto ma ha bisogno di una seconda funzione 
ausiliaria che la invochi nel modo giusto (passando il secondo parametro a 0).

Qual'e' il metodo generale? L'unica cosa che potete fare, in una funzione ricorsiva,
per distinguere le varie invocazioni e' contare: finche' c'e' un ordine chiaro,
la funzione ricorsiva puo' stampare la stringa senza problemi.
In questo esercizio, quindi, distinguiamo fra pari e dispari e, nei pari, fra i
multipli di 4 e no.

*/

void stampa_punti(int n);
void stampa_punti_linee(int n);
void stampa_punti_linee2(int n);
void stampa_punti_linee_uguale(int n);
/*
  Un main semplice che chiede un valore all'utente e lo passa alle funzioni
*/
main() {
  int a,i;


  printf("Inserire un intero: ");
  i=scanf("%d", &a);
  if(i==0) return;

  stampa_punti(a);
  printf("\n");

  stampa_punti_linee(a);
  printf("\n");

  stampa_punti_linee_uguale(a);
  printf("\n");
  
  stampa_punti_linee2(a);

}

void stampa_punti(int n) {
  if(n==0) return;
  
  stampa_punti(n-1);
  
  printf(".");
}


void stampa_punti_linee(int n){
  if(n==0) return;
  
  stampa_punti_linee(n-1);
  
  if(n%2==0) printf("_");
  else printf(".");
}


void stampa_punti_linee_worker(int a, int n) {
  if(a==n) {
    printf("\n");
    return;
  }
  
  if(a%2==0) printf(".");
  else printf("_");
  
  stampa_punti_linee_worker(a+1,n);
}

void stampa_punti_linee2(int n) {
  stampa_punti_linee_worker(0,n);
}


void stampa_punti_linee_uguale(int n){
  if(n==0) return;
  
  stampa_punti_linee_uguale(n-1);
  
  if(n%2==0) {
    if(n%4==0) printf("=");
    else printf("_");
  }
  else printf(".");
}


