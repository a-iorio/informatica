#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
8) Scrivere una funzione ricorsiva che riceva un array di interi e lo ordini, 
usando l'algoritmo di Merge Sort.
----------

*/

#define LUNG 100

void mergesort(int vet[], int dim);

/*
  Un main semplice che crea un vettore casuale, lo stampa, lo ordina e poi lo stampa nuovamente.
*/
main() {
  int vett[LUNG], i, n;

  srand(time(NULL));

  printf("Inserire la lunghezza dell'array (max %d): ", LUNG);
  i=scanf("%d", &n);
  if(i==0 || n>LUNG) return;

  for(i=0; i<n; i++) {
    vett[i]=rand()%100;
    printf("%d ", vett[i]);
  }
  printf("\n");


  mergesort(vett, n);

  for(i=0; i<n; i++)
    printf("%d ", vett[i]);
  printf("\n");

}

/*
Funzione per eseguire lo shift verso destra degli elementi di un array.
Ritorna l'elemento che "sfora" ossia l'ultimo elemento dell'array originale
Il primo elemento rimane immutato (e quindi e' presente in due copie dopo lo shift).
*/
int shift_right(int vet[], int dim) {
  int i, temp=vet[dim-1];
  for(i=dim-1; i>0; i--) 
    vet[i]=vet[i-1];
  return temp;
}

/*
  La funzione merge esegue la seconda parte dell'algoritmo di mergesort, la 
  riunificazionedei due sotto-array ordinati in un'unico array ordinato.
  Notare che:
  - poiche' i due sotto array sono ordinati, l'elemento piu' piccolo in assoluto
  e' uno dei due elementi in testa ai sotto-array
  - poiche' dobbiamo fare spazio per il nuovo array che stiamo compilando,
  quando l'elemento piu' piccolo e' il primo del secondo array, dobbiamo shiftare 
  tutto il primo array verso destra (con la funzione precedente)
  - quando invece l'elemento piu' piccolo e' in testa al primo array, allora e'
  gia' in posizione e basta ridurre le dimensioni del primo array
*/
void merge(int vet1[], int dim1, int vet2[], int dim2) {
  int temp;
  
  if(dim1==0 || dim2==0) return;

  if(vet2[0]<vet1[0]){
    /* l'elemento piu' piccolo e' in testa al secondo segmento */
    
    /* quindi lo mettiamo in testa a tutto e shiftiamo il primo segmento a destra.
    Questo funziona perche' i due segmenti sono contigui.
    */
    temp=shift_right(vet1, dim1); /* in temp ci viene l'elemento che prima era in fondo a vet1 */
    vet1[0]=vet2[0];
    vet2[0]=temp;
    
    /* si riduce il secondo segmento e il primo resta uguale ma shiftato a destra */
    merge(vet1+1, dim1, vet2+1, dim2-1);
  }
  else { 
    /* l'elemento piu' piccolo e' gia' in testa al primo segmento 
    quindi si riduce il primo segmento e il secondo resta uguale */
    merge(vet1+1, dim1-1, vet2, dim2);
  }
}

/*
  Banale funzione di divisione: ogni volta si suddivide l'array in due pezzi 
  uguali si ricorre su questi.
  Quando le due chiamate ricorsive ritornano, e' garantito che i due sotto-array
  siano ordinati e quindi basta completare il tutto con una chiamata a merge
  che sistema i due sotto array.
*/
void mergesort(int vet[], int dim) {
  int i;

  if(dim<2) return;

  i=dim/2;

  mergesort(vet, i);
  mergesort(vet+i, i+(dim%2));

  merge(vet, i, vet+i, i+(dim%2));

}

