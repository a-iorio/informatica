#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
9) Scrivere una funzione (sia iterativa che ricorsiva) che riceva un array 
ordinato (per esempio dopo l'esecuzione di una delle funzioni degli esercizi 
precedenti), chieda all'utente un valore e ricerchi il valore nell'array usando 
l'algoritmo di ricerca binaria spiegato in alto.
Usare una variabile globale per contare il numero di chiamate alla funzione 
effettuate e verificare la logaritmicita' con il numero di elementi dell'array.
----------

Notate l'uso della funzione di ordinamento preso dalle soluzioni degli esercizi
del laboratorio 5.

Notate anche come con al massimo 100 elementi, la ricerca avvenga sempre con al
massimo 8 iterazioni (sarebbero 7 ma il contatore conta anche quella di chiusura
della ricorsione). 
Infatti 2^7=128 > 100.

*/

#define LUNG 100

int counter=0;

void ordina(int vet[], int dim);
int ricerca_binaria(int vet[], int dim, int n);

/*
  Un main semplice che crea un vettore casuale, lo ordina e lo stampa.
  Quindi chiede all'utente un valore e lo cerca nel vettore.
*/
main() {
  int vett[LUNG], i, n, r;

  srand(time(NULL));

  printf("Inserire la lunghezza dell'array (max %d): ", LUNG);
  i=scanf("%d", &n);
  if(i==0 || n>LUNG) return;

  for(i=0; i<n; i++) {
    vett[i]=rand()%100;
  }
  ordina(vett, n);
  for(i=0; i<n; i++)
    printf("%d ", vett[i]);
  printf("\n");
  
  printf("Inserire il numero da cercare: ");
  i=scanf("%d", &r);
  if(i==0) return;
  
  if(ricerca_binaria(vett, n, r)==0) 
    printf("Non trovato!\n");
  else
    printf("Trovato!\n");
    
  printf("\nNumero di ricorsioni per la ricerca: %d\n", counter);
  

}

/*
  l'algoritmo di ricerca binaria e' molto semplice: 
  - controlla il valore centrale, se e' quello cercato, ha finito;
  - altrimenti continua la ricerca in uno dei due sotto-array: quello superiore
  se l'elemento cercato e' maggiore di quello centrale, l'altro altrimenti.
  Ovviamente ci sono due casi base: vettore vuoto, in cui sicuramente l'elemento 
  non c'e', e vettore con un elemento solo, in cui si ritorna direttamente
  il risultato del confronto del valore da cercare con quel solo elemento.
*/
int ricerca_binaria(int vet[], int dim, int n) {
  int i;
  counter++;
  
  if(dim==0) return 0;
  
  if(dim==1) return (n==vet[0]);
  
  i=dim/2;
  
  if(n==vet[i]) return 1;
  
  if(n>vet[i]) 
    return ricerca_binaria(vet+i, dim-i, n);
  else 
    return ricerca_binaria(vet, dim-i, n);
  
}


/*
  Ordinamento di un vettore per selection sort iterativo, preso dalle soluzioni
  degli esercizi del laboratorio 5.
*/
void ordina(int vett[], int dim) {
  int act, cur, min, pos;
  for(act=0; act<dim; act++) { 
    min = vett[act];
    pos = act;
    for(cur=act+1; cur<dim; cur++) {
      if(vett[cur]<min) {
        min = vett[cur];
        pos = cur;
      }
    }
    if(pos>act) {
      vett[pos]=vett[act];
      vett[act]=min;
    }
  }
}