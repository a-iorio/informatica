#include <stdio.h>

/*
  L'unica difficolta' di questo esercizio e' quella di inizializzare 
  correttamente i cursori per le printf.
  Guardando come e' fatto il triangolo, si nota che c'e' un triangolo 
  rovesciato di spazi prima degli asterischi.
  Poiche' stiamo stampando su schermo dove necessariamente dobbiamo 
  stampare in modo consecutivo (non possiamo tornare indietro o andare avanti 
  senza stampare), allineamo gli asterischi con il numero giusto di spazi.
  
     *
    ***
   *****
  *******
 *********
***********

*/

main() {
  
  int altezza;
  int riga,colonna;
  int ret;
  
  printf("Inserisci l'altezza: ");
  ret=scanf("%d", &altezza);
  if(ret!=1) { printf("Errore!\n"); return; }

  /* per ogni riga */
  for(riga=0 ; riga<altezza ; riga++) {
  
    /* prima gli spazi (che diminuiscono andando verso il basso) */
    for(colonna=0 ; colonna<(altezza-riga-1) ; colonna++)
      printf(" ");
      
    /* poi gli asterischi (che aumentano di 2 ogni volta andando verso il basso) */
    for(colonna=0 ; colonna<(2*riga+1) ; colonna++)
      printf("*");
      
    /* e poi termino la riga */
    printf("\n");
  }  

}
