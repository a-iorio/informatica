#include <stdio.h>

/*
  In questa versione alternativa, ho eseguito un'operazione di loop unrolling 
  ossia ho preso il ciclo della versione standard e l'ho "svoltolato" una 
  volta, tirando fuori dal ciclo la prima operazione di lettura.
  Questo mi obbliga ad eseguire le successive operazioni di lettura in fondo 
  al ciclo ma mi aiuta ad evitare di dover eseguire i controlli di terminazione 
  (n>0) piu' volte.
  
  Per il resto, e' lo stesso identico codice.
*/
main() {

  int n, max=-1, min=-1, ret;
  
  printf("Inserisci un numero (0 per terminare): ");
  ret = scanf("%d", &n);
  if(ret!=1) { printf("Errore!\n"); return; }
  
  while(n>0) {
    
    if(min==-1 || n<min) min=n;
    if(max==-1 || n>max) max=n;

    printf("Inserisci un numero (0 per terminare): ");
    ret = scanf("%d", &n);
    if(ret!=1) { printf("Errore!\n"); return; }

  }
  
  if(min!=-1 && max!=-1)
    printf("Massimo=%d, Minimo=%d\n", max, min);
  else
    printf("Sequenza vuota\n");
  
}
