#include <stdio.h>

/*
  La conversione in numeri romani e' relativamente facile: le cifre romane 
  sono addittive e non posizionali, quindi ogni volta che stampo una cifra 
  tolgo quel valore dal valore restante della cifra da stampare. Continuo 
  finche' non ho raggiunto 0.
  Ovviamente, per esempio, quando scendo sotto 10 sicuramente non aggiungero' 
  altre X al numero.
  Notare che per I, X e C uso uno while, visto che possono comparire piu' volte 
  (in questo esempio C compare sicuramente solo una volta ma in generale potrebbe), 
  mentre le altre cifre compaiono al massimo una volta sola.
*/

main() {

  int i, num;
  
  for(i=1; i<=100; i++) {
    num = i;
    while(num>=100) {
      printf("C");
      num-=100;
    }
    if(num>=90) {
      printf("XC");
      num-=90;
    }
    if(num>=50) {
      printf("L");
      num-=50;
    }
    if(num>=40) {
      printf("XL");
      num-=40;
    }
    while(num>=10) {
      printf("X");
      num-=10;
    }
    if(num>=9) {
      printf("IX");
      num-=9;
    }
    if(num>=5) {
      printf("V");
      num-=5;
    }
    if(num>=4) {
      printf("IV");
      num-=4;
    }
    while(num>=1) {
      printf("I");
      num-=1;
    }
    printf("\n");
  }

}


