#include <stdio.h>

/*
  La difficolta' aggiuntiva di questo esercizio rispetto al 17 e' che non 
  dobbiamo stampare piu' solo un asterisco ma dobbiamo stampare una cifra 
  ben precisa in ogni posizione.
  Notare che in ogni riga la prima cifra e' il numero della riga stessa, 
  quindi diminuisce fino ad 1 e poi ricresce fino al numero della riga stessa.
  
    1
   212
  32123
 4321234
543212345
*/

main() {
  
  int altezza=-1;
  int riga,colonna;
  int ret;  

  do {
    printf("Dammi un intero positivo compreso fra 0 e 9: ");
    ret=scanf("%d", &altezza);
    if(ret!=1) { printf("Errore!\n"); return; }
  } while(altezza<0 || altezza>9);

  /* per ogni riga */
  for(riga=0; riga<altezza; riga++) {
  
    /* prima gli spazi (che diminuiscono andando verso il basso) */
    for(colonna=0;colonna<(altezza-riga-1);colonna++)
      printf(" ");
      
    /* poi le cifre decrescenti (parto dal numero di riga e scendo fino a 1) */
    for(colonna=riga+1;colonna>0;colonna--)
      printf("%d",colonna);
      
    /* quindi le cifre crescenti (riparto da dopo l'uno e salgo fino al numero di riga) */
    for(colonna=1; colonna<riga+1; colonna++)
      printf("%d",colonna+1);
      
    /* e infine il fine riga */
    printf("\n");
  }

}
