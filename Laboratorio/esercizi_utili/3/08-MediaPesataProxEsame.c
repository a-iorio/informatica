#include <stdio.h>
#include <math.h>

/*
  In questo esercizio calcolo la media pesata dei voti di uno studente e cerco 
  di calcolare il voto che deve prendere nel prossimo esame per alzarla al 
  successivo intero.
  Di nuovo (come nell'esercizio 6) non usare i break mi costringe ad eseguire 
  i controlli di terminazione due volte.
  Inoltre li eseguo separatamente perche' voglio messaggi di errore diversi 
  (o nessun errore, semplice terminazione).
  
*/

main() {
  
  /* somma parziale del prodotto fra ogni voto e il suo peso in crediti */
  float somma_voto_crediti=0.0;
  
  float somma_crediti=0.0;
  float media;
  float obiettivo;
  float nuovo_voto;
  
  /* valori di input */
  int voto, crediti;
  
  /* valore di ritorno di scanf */
  int ret;
  
  /* prima il ciclo di lettura dei voti preesistenti */
  do {
    printf("Inserisci il voto (0 per uscire): ");
    ret=scanf("%d", &voto);
    if(ret!=1) { printf("Errore!\n"); return; }
    
    /* controlli di terminazione e di coerenza per il voto */
    if(voto>0) {
      if(voto>=18 && voto<=30) {
        printf("Inserisci i crediti (0 per uscire): ");
        ret=scanf("%d", &crediti);
        if(ret!=1) { printf("Errore!\n"); return; }
        
        if(crediti>0) {
          /* sono qui solo se tutto e' a posto */
          somma_voto_crediti += voto*crediti;
          somma_crediti += crediti;
        }
        else 
          printf("Errore: il numero di crediti deve essere maggiore di 0\n");
      }
      else 
        printf("Errore: il voto deve essere compreso fra 18 e 30.\n");
    }
  } while(voto>=18 && voto<=30 && crediti>0);
  
  /* faccio il resto solo se ho letto almeno un voto valido */
  if(somma_crediti>0 && somma_voto_crediti>0) {
  
    media = somma_voto_crediti / somma_crediti;
    printf("La tua media e': %.2f\n", media);
    /* il programma per l'esercizio 7 finiva qui. */
    
    /* ovviamente voglio aumentare la media solo se e' possibile */
    if(media<30) {
      obiettivo = floor(media) + 1; 
      printf("Vediamo se riesci a raggiungere %.0f\n", obiettivo);
      
      /* chiedo il numero di crediti in un ciclo: in questo caso permetto 
      all'utente di inserire numeri non validi e in quel caso continuo a chiedere. 
      Potevo fare come prima che se il numero era sbagliato terminato e sarebbe 
      andato bene lo stesso. */
      do {
        printf("Inserisci il numero di crediti del prossimo esame: ");
        ret=scanf("%d", &crediti);
        if(ret!=1) { printf("Errore!\n"); return; }
        
        if(crediti<=0)
          printf("Valore inserito non valido!\n");
          
      } while(crediti<=0);
      
      /* adesso ho tutti i dati, posso eseguire i conti */
      
      /* questa formula inverte la sommatoria per il voto pesato */
      nuovo_voto = (obiettivo * (somma_crediti + crediti) - somma_voto_crediti) / crediti;
      
      /* questo non dovrebbe mai accadere, visto che il voto minore 
      accettato precedentemente e' 18. */
      if(nuovo_voto<18) nuovo_voto = 18;
      
      if(nuovo_voto>30) 
        printf("Mi dispiace, non e' possibile!\n");
      else
        printf("Per ottenere la media del %.0f devi prendere %.0f al prossimo esame\n", obiettivo, ceil(nuovo_voto));
    }
  }

}

