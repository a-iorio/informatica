#include <stdio.h>

/* 
  In questo esercizio elementare, all'utente viene chiesta una sequenza 
  di numeri terminata da 0.
  Ho scelto di dare a max e min un valore "illecito" cosi' da sapere se sono 
  alla prima iterazione.
  Nel caso questo non sia possibile, basta usare una variabile aggiuntiva che 
  indica la prima iterazione.
  
  Il ciclo quindi legge un carattere e controlla che sia valido.
  Se lo e', lo confronta con min e max precedenti ed eventualmente li aggiorna
  (a meno che non sia la prima iterazione, nel qual caso esegue comunque 
  l'aggiornamento).
  
  Notate che non usare break nel corpo del ciclo mi costringe ad eseguire piu' 
  controlli.

*/

main() {

  int n, max=-1, min=-1, ret;
  
  do {
    /* classica operazione di lettura e verifica */
    printf("Inserisci un numero (0 per terminare): ");
    ret = scanf("%d", &n);
    if(ret!=1) { printf("Errore!\n"); return; }
    
    /* se ho letto un carattere della sequenza */
    if(n>0) {
      /* aggiorno min (e max sotto) se siamo alla prima iterazione 
      o se ho un nuovo minimo (massimo) */ 
      if(min==-1 || n<min) min=n; 
      if(max==-1 || n>max) max=n;
    }
  } while(n>0);
  
  /* potrebbe essere una sequenza vuota e devo controllare. */
  if(min!=-1 && max!=-1)
    printf("Massimo=%d, Minimo=%d\n", max, min);
  else
    printf("Sequenza vuota\n");
  
}
