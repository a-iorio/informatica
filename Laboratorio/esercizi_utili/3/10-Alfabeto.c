#include <stdio.h>

/*
  Lettura di una sequenza di lettere alfabetiche.
  Notate che durante il ciclo non mi serve ricordare molto, mi basta sapere 
  quante lettere minuscole diverse ci sono state finora (all'inizio 0 ovviamente), 
  l'ultimo carattere letto e quello appena precedente.
  Ovviamente devo inizializzare il carattere precedente ad un valore minore 
  del primo valore lecito ('a').
  Notare che proseguo nel ciclo se leggo un carattere uguale al precedente ma 
  aggiorno il contatore solo se ne ho letto uno maggiore.

*/

main() {
  
  int counter = 0;
  char prec, act;
  int ret;
  
  prec = 'a' - 1;
  
  printf("Dammi un carattere: ");
  ret=scanf(" %c", &act);
  if(ret!=1) { printf("Errore!\n"); return; }
  
  /* il carattere deve essere minuscolo e non minore del precedente */
  while('a'<=act && act <='z' && act>=prec) {
    /* se il carattere e' maggiore del precedente, lo conto e aggiorno prec */
    if(act>prec) {
      counter++;
      prec = act;
    }

    printf("Dammi un carattere: ");
    ret=scanf(" %c", &act);
    if(ret!=1) { printf("Errore!\n"); return; }
  }
  
  if(counter>0)
    printf("Totale lettere minuscole ordinate e diverse: %d\n", counter);
  else
    printf("La sequenza di lettere minuscole e' vuota.\n");

}

