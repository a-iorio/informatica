Laboratorio 1 - Esercizi Proposti

Nella propria home directory creare una sottodirectory chiamata es01, 
in cui metteremo tutti i file C di oggi. 

1) Scrivere un programma nome che stampa il proprio nome e sulla riga 
successiva il proprio cognome.

2) Scrivere un programma cornice che stampa il proprio nome su una riga 
racchiuso da una cornice, cos�:
*************
* Aureliano *
*************
Notate lo spazio prima e dopo il nome.
Riuscite a scrivere il programma utilizzando un solo comando di output?

3) Scrivere un programma tipo_elementare che, per ciascun tipo elementare 
gi� visto a lezione, stampa una riga contenente il nome del tipo e la 
dimensione, in byte, di una variabile di quel tipo.

4) Scrivere un programma area_rettangolo che dichiara due variabili che 
rappresentano i lati di un rettangolo, assegna a tali variabili due valori 
e stampa il perimetro e l'area del rettangolo risultante. 

5) Modificare il programma precedente in modo da leggere dall'input i valori 
delle dimensioni del rettangolo. 
Eseguire il programma per un rettangolo di dimensioni 3 x 2.

6) Scrivere un programma valore che calcola e stampa il valore di una 
villetta composta da due piani, ciascuno dei quali include:
 - salotto (dimensioni 3m x 5m)
 - cucina (dimensioni 4m x 4m)
 - camera da letto (dimensioni 5m x 5m)
 - bagno (dimensioni 2m x 3m)
sapendo che il costo per metro quadro di quella zona  � di 1250 euro.

Per rendere leggibile il codice, definire opportune costanti METROQUADRO, 
SALOTTO, CUCINA, CAMERA e BAGNO.
Suggerimento: inizializzare le costanti SALOTTO, CUCINA, CAMERA e BAGNO 
in funzione della costante METROQUADRO.
(Il valore calcolato dovrebbe essere 155000). 

