#include <stdio.h>

main() {
  
  int base, altezza;
  
  printf("Inserire la base del rettangolo: ");
  scanf("%d", &base);

  printf("Inserire l'altezza del rettangolo: ");
  scanf("%d", &altezza);
  
  printf("Il perimetro del rettangolo e': %d\n", (base+altezza)*2);
  printf("L'area del rettangolo e': %d\n", base*altezza);
  
}
