#include <stdio.h>

/*
17) Scrivere una procedura che ricevuto un array di caratteri modifichi il vettore 
in modo che ogni vocale venga sostituita dal simbolo '$'.
void dollarize(char arr[], int dim);
----------
Ovvio esercizio di modifica di un vettore.
Lo scorro e quando incontro una vocale, sostituisco il carattere $.

*/

void dollarize(char arr[], int dim) {
  int i;
  char el;
  for(i=0; i<dim; i++) { /* scorro tutto il vettore */ 
    el = arr[i];
    if('A'<=el && el<='Z') /* trasformo le maiuscole in minuscole */
      el = el - 'A' + 'a';
    
    if(el=='a' || el=='e' || el=='i' || el=='o' || el=='u') {
      /* se incontro una vocale, la sostituisco */
      arr[i]='$';
    }
  }
}

#define LUNG 6

/* un semplice main che crea un array, lo stampa a video 
lo passa alla procedura e quindi stampa a video l'array modificato */
main() {

  char vet[LUNG]= {'g', 'a', 'm', 'D', 'U', 'e' };
  int i;
  
  printf("vet = [ ");
  for(i=0; i<LUNG; i++)
    printf("%c ", vet[i]);
  printf("]\n");
  
  dollarize(vet, LUNG);

  printf("vet = [ ");
  for(i=0; i<LUNG; i++)
    printf("%c ", vet[i]);
  printf("]\n");

}
