#include <stdio.h>

/*
6) Scrivere una funzione che, ricevuto un array di caratteri, ritorni la prima 
lettera maiuscola, se c'e'. Se non c'e', allora ritorna '\n'.
----------

In questa semplice funzione eseguo una scansione lineare del contenuto
interrompendola nel caso trovi quello che serve (cioe' una maiuscola).
Se arrivo in fondo restituisco il valore di default, \n

*/

char primaMaiuscola(char vett[], int dim) {
  int i=0;
  char ret = '\n'; 
  /* in questo caso vale \n sia come ritorno di default 
  che come controllo per verificare se e' stata trovata una maiuscola.
  Scorro il vettore con uno while poiche' potrei dovermi fermare 
  prima della fine.
  */
  while(i<dim && ret=='\n') {
    if('A' <= vett[i] && vett[i] <= 'Z')
      ret = vett[i];
    i++;
  }
  return ret;
}

#define LUNG1 6
#define LUNG2 4

/* un semplice main che crea due array, li stampa a video 
e li passa alla funzione, stampando il valore ritornato */
main() {
  int i;
  char vet1[LUNG1]= {'g', 'f', 'm', 'D', 'h', 'E' }; /* con maiuscole */
  char vet2[LUNG2]= {'r', 'w', 's', 'a'}; /* senza maiuscole */

  printf("vet1 = [ ");
  for(i=0; i<LUNG1; i++)
    printf("%c ", vet1[i]);
  printf("]\n");

  printf("vet2 = [ ");
  for(i=0; i<LUNG2; i++)
    printf("%c ", vet2[i]);
  printf("]\n");

  
  printf("La prima maiuscola di vet1 e': %c\n", primaMaiuscola(vet1, LUNG1));
  printf("La prima maiuscola di vet2 e': %c\n", primaMaiuscola(vet2, LUNG2));

}
