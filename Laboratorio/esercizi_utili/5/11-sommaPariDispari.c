#include <stdio.h>

/*
11) Scrivere una funzione che ricevuto un array di interi, ritorni la somma degli 
elementi in posizione dispari meno la somma degli elementi in posizione pari.
----------

un esercizio molto semplice in cui si scorre tutto il vettore passato (con un for, quindi)
per eseguire un calcolo sui vari elementi, distinguendo fra posizioni pari e dispari

*/

int sommaPariDispari(int vett[], int dim) {
  int i, pari=0, dispari=0;
  
  /* scorro tutto l'array e distinguo fra posizioni pari e dispari, 
  sommando all'accumulatore corrispondente */
  for(i=0; i<dim; i++) 
    if(i%2==0)
      pari += vett[i];
    else
      dispari += vett[i];
  
  /* alla fine il risultato e' la differenza dei due accumulatori */
  return (dispari - pari);
}

#define LUNG 11

/* un semplice main che crea un array, lo stampa a video 
e lo passa alla funzione, stampando il valore ritornato */
main() {

  int vet[LUNG] = {5,6,7,3,4,5,8,2,1,0,6}; /* deve fare -15 */
  int i;

  printf("vet = [ ");
  for(i=0; i<LUNG; i++)
    printf("%d ", vet[i]);
  printf("]\n");


  printf("La somma degli elementi in posizione dispari meno la somma degli elementi in posizione pari e': %d\n", sommaPariDispari(vet, LUNG));
  
}
