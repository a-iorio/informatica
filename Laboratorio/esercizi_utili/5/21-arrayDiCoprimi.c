#include <stdio.h>

/*

20) Scrivere una funzione che, ricevuti due parametri numerici A e B, verifichi se 
A e B sono coprimi (o primi tra loro) ritornando 1 nel caso lo siano, 0 altrimenti.
Due numeri sono coprimi se non hanno divisori in comune a parte 1.
Per verificare se due numeri sono coprimi cercare i loro divisori del piu' grande
nell'intervallo [2, N/2] dove N e' il piu' piccolo fra A e B.

21) Dato un array di interi, definire una funzione che ritorna 1 se l'array contiene
solo numeri primi tra loro o 0 altrimenti, usando la funzione prima definita.
----------

Questo esercizio richiede prima di creare una funzione astratta che ritorni 1 o 0 
a seconda che due numeri siano coprimi. Siccome stiamo implementando una funzione
astratta ben chiara, non c'e' bisogno (in questa fase) di preoccuparsi del vettore
dell'esercizio successivo.

Nella seconda parte, quindi, prendiamo la funzione coprimi come un dato di fatto,
senza preoccuparci ulteriormente di come funzioni internamente (ossia di come sia 
implementata). L'unica attenzione e' quindi a come verificare la proprieta' su 
tutto il vettore, chiamando la funzione per ogni possibile coppia di elementi.

*/

/* funzione ausiliaria per ottenere il valore minimo fra due */
int minimo(int a, int b) {
  if(a<b) return a;
  return b;
}

/* 
La soluzione dell'esercizio 20.
Notate che il limite superiore di ricerca viene calcolato con l'uso della 
funzione ausiliaria "minimo" prima definita.
 */
int coprimi(int a, int b) {
  int i, ret=1, upperLimit;
  
  /* se il minimo fra a e b e' pari, upperLimit e' la sua meta' precisa. 
  Se e' dispari, la sua meta' approssimata per eccesso. */
  upperLimit = (minimo(a,b)+1) / 2;
  
  /* intervallo [2, N/2] */
  i=2;
  while(i<=upperLimit && ret==1) {
    /* se i due numeri sono entrambi divisibili per i, non sono coprimi */
    if(a%i==0 && b%i==0)
      ret = 0;
    
    i++;
  }
  
  return ret;
}

/*
Soluzione dell'esercizio 21. 
Data la funzione coprimi, questa e' un semplice esercizio di confronto
a due a due di tutti gli elementi del vettore.
Per maggior efficienza, evito di fare i confronti che ho gia' fatto:
il ciclo interno parte dalla posizione del contatore esterno.
E' comunque una complessita' quadratica ma risparmio circa meta' confronti.
*/
int arrayDiCoprimi(int vett[], int dim) {
  int i, j;
  int ret = 1;
  for(i=0; i<dim; i++) {
    for(j=i+1; j<dim; j++) {
      /* l'esercizio vuole verificare se tutti gli elementi dell'array 
      sono coprimi tra loro: la proprieta' deve essere vera per ogni 
      coppia di elementi e quindi i risultati delle chiamate alla 
      funzione vanno messi in AND */
      ret = ret && coprimi(vett[i], vett[j]);
    }
  }
  return ret;
}

#define LUNG 4

/* un semplice main che crea due array, li stampa a video 
e li passa alla funzione, stampando i risultati sulla base 
dei valori ritornati */
main() {

  int vet[LUNG] = {5,18,17,7}; /* tutti coprimi */
  int vet2[LUNG] = {5,18,17,15}; /* non tutti coprimi */ 
  int i;

  printf("vet = [ ");
  for(i=0; i<LUNG; i++)
    printf("%d ", vet[i]);
  printf("]\n");

  if(arrayDiCoprimi(vet, LUNG))
    printf("L'array contiene solo numeri coprimi\n");
  else
    printf("L'array non contiene solo numeri coprimi\n" );

  printf("vet2 = [ ");
  for(i=0; i<LUNG; i++)
    printf("%d ", vet2[i]);
  printf("]\n");

  if(arrayDiCoprimi(vet2, LUNG))
    printf("L'array contiene solo numeri coprimi\n");
  else
    printf("L'array non contiene solo numeri coprimi\n" );

}
