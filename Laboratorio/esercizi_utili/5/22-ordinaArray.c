#include <stdio.h>

/*
22) Scrivere una procedura che, ricevuto un array di interi, ordini l'array. 
Non ci sono restrizioni di efficienza, potete usare un qualsiasi algoritmo di 
ordinamento (ad esempio la ricerca del minimo nel sotto-array residuo da spostare
nella prima posizione del sotto-array).
----------

In questo esercizio la procedura ordina fa tutto il lavoro e quando termina
l'array passato e' ordinato.
Per farlo la procedura ricerca prima il minimo assoluto e lo sposta in prima 
posizione. Quindi il secondo minimo (il minimo assoluto del sotto-array senza
la prima posizione) e lo piazza in seconda posizione. E cosi' via, fino all'ultimo 
elemento, che e' sicuramente il massimo.

*/

void ordina(int vett[], int dim) {
  int act, cur, min, pos;
  
  for(act=0; act<dim; act++) { 
    /* ad ogni iterazione, l'elemento attuale e' il minimo iniziale */
    min = vett[act];
    pos = act;
    
    /* a partire dalla posizione successiva a quella attuale, cerco il minimo */ 
    for(cur=act+1; cur<dim; cur++) {
      if(vett[cur]<min) {
        min = vett[cur];
        pos = cur;
      }
    }
    
    /* se ho trovato un elemento minimo diverso da quello attuale, lo sposto 
    al posto di quello attuale. Notate che lo swap si puo' eseguire senza 
    variabile temporanea aggiuntiva perche' ho il valore di min salvato
    in precedenza. */
    if(pos>act) {
      vett[pos]=vett[act];
      vett[act]=min;
    }
  }
}

#define LUNG 11

/* un semplice main che crea un array, lo stampa a video 
lo passa alla procedura e quindi stampa a video l'array modificato */
main() {

  int vet[LUNG] = {5,6,1,3,4,5,8,2,1,1,6};
  int i;
  
  printf("vet = [ ");
  for(i=0; i<LUNG; i++)
    printf("%d ", vet[i]);
  printf("]\n");
  
  ordina(vet, LUNG);

  printf("vet = [ ");
  for(i=0; i<LUNG; i++)
    printf("%d ", vet[i]);
  printf("]\n");


}
