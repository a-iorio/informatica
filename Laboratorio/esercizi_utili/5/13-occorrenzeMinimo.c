#include <stdio.h>

/*
13) Dato un array di interi non vuoto, scrivere una funzione che ritorni il numero 
di occorrenze del valore minimo (scorrendo l'array una volta sola).
----------
Una semplice funzione di ricerca in un vettore.
Per ogni valore del vettore, devo distinguere tre casi: 
 trovo un nuovo minimo -> aggiorno il minimo attuale e resetto le occorrenze
 trovo lo stesso valore del minimo -> aggiorno le occorrenze del minimo
 trovo un altro valore -> vado avanti senza fare nulla

*/

int occorrenzeMinimo(int vett[], int dim) {
  int ret=1, i, min;
  min = vett[0]; /* sempre possibile perche' vett e' garantito non vuoto */
  for(i=0; i<dim; i++) {
    if(vett[i]<min) { /* ho trovato un nuovo minimo */
      min = vett[i]; /* aggiorno il minimo */
      ret = 1; /* resetto le occorrenze */
    }
    else {
      if(vett[i]==min) /* se ho trovato lo stesso minimo di prima */
        ret++; /* aggiorno le occorrenze */
    }
  }
  return ret;
}

#define LUNG 11

/* un semplice main che crea un array, lo stampa a video 
e lo passa alla funzione, stampando il valore ritornato */
main() {

  int vet[LUNG] = {5,6,1,3,4,5,8,2,1,1,6};
  int i;
  
  printf("vet = [ ");
  for(i=0; i<LUNG; i++)
    printf("%d ", vet[i]);
  printf("]\n");

  printf("Il numero di occorrenze del minimo e': %d\n", occorrenzeMinimo(vet, LUNG));

}
