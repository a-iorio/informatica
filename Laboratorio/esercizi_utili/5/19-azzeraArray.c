#include <stdio.h>

/*
19) Scrivere una funzione che riceva una matrice bidimensionale di interi e azzeri 
ogni elemento il cui contenuto e' maggiore o uguale alla somma delle sue coordinate.
void azzera(int mat[][5], int numRighe);
----------

Altro semplice esercizio di modifica di una matrice.
La matrice va scorsa con due contatori e dove l'elemento verifica 
la condizione, va modificata.

Notate che per come e' stato dato l'esercizio, il numero di colonne (5) deve 
essere messo esplicitamente nel codice.

*/

void azzera(int mat[][5], int numRighe) {
  int i, j;
  for(i=0; i<numRighe; i++) {
    for(j=0; j<5; j++) {
      if( mat[i][j] >= (i+j) ) /* questa e' la condizione data nell'esercizio */
        mat[i][j]=0;
    }
  }
}

#define LUNG 3

/* un semplice main che crea una matrice, la stampa a video 
la passa alla procedura e quindi stampa a video la matrice modificata */
main() {

  int matr[LUNG][5] = {{5,6,1,3,4},{5,8,2,1,1},{6,4,9,34,66}};
  int i,j;
  
  printf("matr = [\n");
  for(i=0; i<LUNG; i++) {
    printf("[ ");
    for(j=0; j<5; j++)
      printf("%d ", matr[i][j]);
    printf("]\n");
  }
  printf("]\n");

  azzera(matr, LUNG);
  
  printf("matr = [\n");
  for(i=0; i<LUNG; i++) {
    printf("[ ");
    for(j=0; j<5; j++)
      printf("%d ", matr[i][j]);
    printf("]\n");
  }
  printf("]\n");

}
