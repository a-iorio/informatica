#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SASSO     's'
#define CARTA     'c'
#define FORBICE   'f'

/*
  In questo programma, il computer e il giocatore umano giocano alla morra cinese.
  Entrambi scelgono uno dei simboli, il computer sceglie tirando un dado da 0 a 2.
  Una volta che i due simboli sono noti, il computer calcola il risultato del gioco
  secondo le regole classiche: sasso batte forbice, forbice batte carta, carta batte sasso.
*/

main () {

  char computer;
  char umano;
  int ret, tirodado;
  
  printf("Inserisci %c, %c o %c\n", SASSO, CARTA, FORBICE);
  ret=scanf(" %c", &umano);
  if(ret!=1) { printf("Errore!\n"); return; } /* In caso di errore il programma termina */

  /* se il giocatore ha inserito un simbolo non valido, perde di default. */
  if(umano!=SASSO && umano!=CARTA && umano!=FORBICE) {
    printf("Simbolo sbagliato: perdi automaticamente!\n");
    return;
  }
  
  /* Il computer tira un dado per ottenere un numero fra 0 e 2 e lo usa per scegliere un simbolo */
  srand(time(NULL));
  tirodado=rand()%3;
  switch(tirodado) {
    case 0: 
      computer=SASSO; 
      break;
    case 1: 
      computer=CARTA;
      break;
    case 2:
      computer=FORBICE;
      break;
    default:
      printf("Anche i computer sbagliano?\n");
      return;
  }
  
  /* Quando entrambi i simboli sono noti, il computer decide chi vince secondo le regole classiche della morra */
  if(umano==computer) 
    printf("Pareggiamo: Tu:%c, Io:%c\n", umano, computer);
  else if(umano==SASSO) {
    if(computer==CARTA)
      printf("Ho vinto io: Tu:%c, Io:%c\n", umano, computer);
    else
      printf("Hai vinto tu: Tu:%c, Io:%c\n", umano, computer);
  }
  else if(umano==FORBICE) {
    if(computer==CARTA)
      printf("Hai vinto tu: Tu:%c, Io:%c\n", umano, computer);
    else
      printf("Ho vinto io: Tu:%c, Io:%c\n", umano, computer);
  }
  else /* umano==CARTA */ {
    if(computer==SASSO)
      printf("Hai vinto tu: Tu:%c, Io:%c\n", umano, computer);
    else
      printf("Ho vinto io: Tu:%c, Io:%c\n", umano, computer);
  }
  
  printf("Ciao\n");

}

