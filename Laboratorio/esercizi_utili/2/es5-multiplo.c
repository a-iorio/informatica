#include <stdio.h>

/*
  In questo semplice programma chiediamo all'utente un numero e dobbiamo verificare 
  se sia un multiplo di 3, di 5 o di 7.
  Anche se il codice sembra molto complesso, in effetti sono solamente 3 if annidati
  in modo da coprire tutte le combinazioni possibili.
  Notate che il programma esegue solamente 3 operazioni matematiche (3 moduli)
  e solamente 3 confronti (anche se ci sono molti if, ad ogni esecuzione 
  solamente 3 di questi vengono eseguiti)!
  Anche se il codice non ha un bell'aspetto, il programma e' il piu' efficiente possibile
  (il che significa che programmi diversi possono solamente essere altrettanto efficienti
  ma non migliori).
*/
main() {
  
  int numero;
  int r3, r5, r7;
  int ret;
  
  printf("Inserisci un numero: ");
  ret = scanf("%d", &numero);
  if(ret!=1) { printf("Errore!\n"); return; } /* In caso di errore il programma termina */
  
  r3 = numero % 3;
  r5 = numero % 5;
  r7 = numero % 7;
  
  if(r3==0) {
    if(r5==0) {
      if(r7==0) 
        printf("Multiplo di 3, 5 e 7\n");
      else 
        printf("Multiplo di 3 e 5\n");
    }
    else {
      if(r7==0) 
        printf("Multiplo di 3 e 7\n");
      else 
        printf("Multiplo di 3\n");
    }

  }
  else {
    if(r5==0) {
      if(r7==0) 
        printf("Multiplo di 5 e 7\n");
      else 
        printf("Multiplo di 5\n");
    }
    else {
      if(r7==0) 
        printf("Multiplo di 7\n");
      else 
        printf("Non multiplo di 3, di 5 o di 7\n");
    }
  }
  
}
