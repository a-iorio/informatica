#include <stdio.h>

/*
  In questo semplice programma dobbiamo ordinare tre numeri.
  Come nel programma dell'esercizio 5, anche qui dobbiamo eseguire il minimo 
  numero di confronti (che e' 3).
  Per farlo ricorriamo di nuovo ad if annidati: il codice e' apparentemente complesso
  e contiene molti if ma ad ogni esecuzione ne vengono eseguiti solamente al massimo tre
  e quindi questo e' il codice piu' efficiente possibile (nel senso che ogni altro
  programma diverso potra' solamente essere altrettanto efficiente e non migliore).
  
  Notare che qui esistono casi in cui eseguiamo solo due confronti mentre nell'esercizio 5
  eravamo costretti ad eseguirne sempre almeno 3: questo dipende dal fatto che
  la relazione di minore (o maggiore) e' transitiva quindi se a<b e b<c sappiamo 
  gia' (senza bisogno di eseguire il confronto esplicito) che a<c.
*/
main () {

  int a,b,c;
  int ret;
  
  printf("Inserisci tre numeri\n");
  ret=scanf("%d %d %d", &a, &b, &c);
  if(ret!=3) { printf("Errore!\n"); return; } /* In caso di errore il programma termina */
  
  printf("I numeri in ordine descrescente sono: ");
  if(a<b) {
    if(b<c)
      printf("%d %d %d\n", c, b, a);
    else {
      if(a<c)
        printf("%d %d %d\n", b, c, a);
      else
        printf("%d %d %d\n", b, a, c);
    }
  }
  else {
    if(a<c)
      printf("%d %d %d\n", c, a, b);
    else {
      if(b<c)
        printf("%d %d %d\n", a, c, b);
      else
        printf("%d %d %d\n", a, b, c);
    }
  }

}
