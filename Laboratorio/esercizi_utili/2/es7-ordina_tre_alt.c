#include <stdio.h>

/*
  In questa soluzione alternativa dell'ordinamento di tre numeri
  si e' scelto di riordinare i numeri spostando i valori dalle 
  variabili in cui sono inizialmente salvati per rispettare 
  l'ordine decrescente. In questo modo alla fine avremo il piu'
  grande in a, il secondo in b e il piu' piccolo in c.
  Notare che questa implementazione esegue solamente tre confronti
  come l'altra ma e' meno efficiente per via degli (eventuali) scambi
  di valore fra variabili: nel caso peggiore ne esegue addirittura nove.
  Notare anche il classico algoritmo per lo swap di due variabili 
  usandone una terza come temporanea.
*/
main () {

  int a,b,c;
  int temp;
  int ret;
  
  printf("Inserisci tre numeri\n");
  ret=scanf("%d %d %d", &a, &b, &c);
  if(ret!=3) { printf("Errore!\n"); return; } /* In caso di errore il programma termina */
  
  /* Prima confronto a e b e metto il piu' grande in a e il piu' piccolo in b */
  if(a<b) {
    temp=a;
    a=b;
    b=temp;
  }

  /* poi confronto b e c e metto il piu' grande in b e il piu' piccolo in c.
     A questo punto in c c'e' sicuramente il piu' piccolo dei tre.
  */  
  if(b<c) {
    temp=b;
    b=c;
    c=temp;
  }

  /* Infine confronto di nuovo a e b, mettendo il piu' grande in a. 
     In questo modo, in a c'e' sicuramente il piu' grande dei tre e in b l'intermedio.
  */    
  if(a<b) {
    temp=a;
    a=b;
    b=temp;
  }
  
  /* A questo punto quindi i tre numeri sono in ordine! */
  printf("I numeri in ordine descrescente sono: %d %d %d\n", a, b, c);

}
