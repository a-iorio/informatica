#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SASSO     's'
#define CARTA     'c'
#define FORBICE   'f'

/*
  In questa versione della morra, il computer bara per vincere i 2/3 delle partite.
  
  Una soluzione ingenua potrebbe essere che il computer vince se l'umano gioca 
  (per esempio) sasso e carta e perde se l'umano gioca forbice.
  Ma questo funziona solo se l'utente gioca a caso mentre un giocatore umano qualsiasi 
  si accorgerebbe dopo poco del pattern e vincerebbe quindi ogni scontro (in questo esempio, 
  giocando sempre forbice).
  
  In questa implementazione, invece, il computer decide volta per volta con quale 
  simbolo l'umano vincera' e con quali perdera', prima ancora che l'umano giochi, 
  e quindi decide il proprio simbolo di conseguenza per ottenere il risultato voluto.
  Notate che l'uso del tiro casuale e' spostato all'inizio per decidere quale 
  sara' il simbolo vincente mentre la giocata del computer e' ora determinata di conseguenza.

  Da notare anche che in questa implementazione si e' scelto di ignorare i pareggi per
  quanto riguarda il rapporto fra vittorie/sconfitte e quindi qui i pareggi non avvengono mai.
*/

main () {

  char computer;
  char umano;
  char vincente;
  int ret, tirodado;

  /* l'umano vincera' solo se gioca il simbolo vincente, altrimenti perde 
  (e quindi perde in media i 2/3 dei casi) */
  srand(time(NULL));
  tirodado=rand()%3;
  switch(tirodado) {
    case 0: 
      vincente=SASSO; 
      break;
    case 1: 
      vincente=CARTA;
      break;
    case 2:
      vincente=FORBICE;
      break;
    default:
      printf("Anche i computer sbagliano?\n");
      return;
  }

  printf("Inserisci %c, %c o %c\n", SASSO, CARTA, FORBICE);
  ret=scanf(" %c", &umano);
  if(ret!=1) { printf("Errore!\n"); return; } /* In caso di errore il programma termina */
  
  /*
    una volta che l'umano ha giocato, il computer verifica se ha giocato o meno il simbolo vincente
    e quindi adatta la propria giocata per ottenere il risultato voluto.
  */
  switch(umano) {
    case SASSO: 
      if(umano==vincente)
        computer=FORBICE; 
      else
        computer=CARTA;
      break;
    case CARTA: 
      if(umano==vincente)
        computer=SASSO; 
      else
        computer=FORBICE;      
      break;
    case FORBICE:
      if(umano==vincente)
        computer=CARTA; 
      else
        computer=SASSO;      
      break;
    default:
      printf("Simbolo sbagliato: perdi automaticamente!\n");
      return;
  }
  
  /* da qui e' uguale all'esercizio 11. Ovviamente il pareggio non avverra' mai qui. */
  if(umano==computer) 
    printf("Pareggiamo: Tu:%c, Io:%c\n", umano, computer);
  else if(umano==SASSO) {
    if(computer==CARTA)
      printf("Ho vinto io: Tu:%c, Io:%c\n", umano, computer);
    else
      printf("Hai vinto tu: Tu:%c, Io:%c\n", umano, computer);
  }
  else if(umano==FORBICE) {
    if(computer==CARTA)
      printf("Hai vinto tu: Tu:%c, Io:%c\n", umano, computer);
    else
      printf("Ho vinto io: Tu:%c, Io:%c\n", umano, computer);
  }
  else /* umano==CARTA */ {
    if(computer==SASSO)
      printf("Hai vinto tu: Tu:%c, Io:%c\n", umano, computer);
    else
      printf("Ho vinto io: Tu:%c, Io:%c\n", umano, computer);
  }
  
  printf("Ciao\n");

}

