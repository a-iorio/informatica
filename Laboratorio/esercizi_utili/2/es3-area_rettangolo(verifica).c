#include <stdio.h>
#include <math.h>

/*
  In questo semplice programma, chiedo la base e l'altezza del rettangolo iniziale all'utente
  poi effettuo i calcoli come richiesto.
  Comunque prima di ogni passo controllo che il dato appena inserito sia un numero legale:
  faccio i due controlli separatamente (che sia un numero e che sia legale) perche' voglio
  messaggi di errore diversi nei due casi.
  Inoltre controllo i due dati appena vengono inseriti perche' voglio interrompere subito
  il processo appena ricevo un dato invalido.
*/
main() {
  
  int base, altezza, perimetro, area, ret;
  double nuovo_perimetro, nuova_area;
  
  /* chiedo la base all'utente */
  printf("Inserire la base del rettangolo: ");
  ret = scanf("%d", &base);
  if(ret==1) { /* controllo che l'utente abbia effettivamente inserito un numero */
    if(base>0) { /* controllo che la base sia valida */
      
      /* chiedo l'altezza all'utente */
      printf("Inserire l'altezza del rettangolo: ");
      ret = scanf("%d", &altezza);
      if(ret==1) { /* controllo che l'utente abbia effettivamente inserito un numero */
        if(altezza>0) { /* controllo che l'altezza sia valida */
        
          /* se siamo arrivati qui, tutti i dati inseriti sono validi quindi facciamo i conti */
          perimetro = (base+altezza)*2;
          area = base*altezza;
          printf("Il perimetro del rettangolo e' : %d\n", perimetro);
          printf("L'area del rettangolo e' : %d\n", area);
          
          /* i vecchi valori erano interi mentre qui ho bisogno senza dubbio di numeri reali
             questo mi costringe ad effettuare il cast per evitare che il compilatore esegua
             operazioni fra interi (scartando il resto della divisione, per esempio).
          */
          nuova_area = pow( ((double)perimetro / 4), 2.0);
          nuovo_perimetro = sqrt((double)area) * 4.0;
          printf("L'area di un quadrato avente lo stesso perimetro e' : %.2f\n", nuova_area);
          printf("Il perimetro  di un quadrato avente la stessa area e' : %.2f\n", nuovo_perimetro);
        }
        else printf("Errore: altezza deve essere maggiore di 0!\n");
      }
      else printf("Errore: inserire un numero!\n");
    }
    else printf("Errore: base deve essere maggiore di 0!\n");
  }
  else printf("Errore: inserire un numero!\n");
  
  
}
