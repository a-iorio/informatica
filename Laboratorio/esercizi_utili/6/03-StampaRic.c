#include <stdio.h>

/*
3) Scrivere una procedura ricorsiva che riceva un array a lo stampi a video.
Scrivere la procedura con i due metodi: 
- con l'aritmetica dei puntatori
    void stampa1 (int vet[], int dim);
- con un indice
    void stampa2 (int vet[], int i, int dim);

----------------------
    
Stampa1 ricorre sul sotto-vettore che inizia dal secondo elemento 
(vet+1, aritmetica dei puntatori) e che ovviamente ha dimensione (dim-1).

Stampa2 invece fa avanzare l'indice i, ricorrendo sullo stesso vettore. 
E' quindi l'indice i stesso a segnalare la terminazione della ricorsione, 
quando raggiunge la dimensione del vettore.
    
*/


void stampa1 (int vet[], int dim) {
  if(dim==0) {
    printf("\n");
    return;
  }
  
  printf("%d ", vet[0]);
  stampa1(vet+1, dim-1);
}


void stampa2 (int vet[], int i, int dim) {
  if(i==dim) {
    printf("\n");
    return;
  }
  
  printf("%d ", vet[i]);
  stampa2(vet, i+1, dim);
}


/*
Un semplice main che crea un array e lo passa alle funzioni appena definite, 
stampando i risultati.
*/
main() {
  int vett[] = {1,2,3,4,5,6,7,8,9,10};
  
  stampa1(vett, 10);
  stampa2(vett, 0, 10);
}
