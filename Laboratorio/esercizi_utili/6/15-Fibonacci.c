#include <stdio.h>

/*
15) Riuscite a scrivere la funzione fibonacci ottimizzando
(riducendo al minimo) il numero di chiamate ricorsive?
Suggerimento: Per verificare quante chiamate vengono eseguite, usate 
una variabile globale che viene incrementata ad ogni chiamata.
Suggerimento 2: potete sfruttare il fatto che per calcolate F(n-1)
dovete sapere F(n-2)?
Suggerimento 3: una funzione in C non puo' ritornare piu' di un valore ma
se riceve dei puntatori puo' modificare i valori delle variabili originali.
---------------

In questo esercizio avanzato, si richiedeva di linearizzare il calcolo 
dell'ennesimo numero di Fibonacci.
Infatti, come si puo' notare dall'implementazione classica, il numero di
chiamate ricorsive del calcolo classico e' quadratico rispetto a n.
Per vederlo, provate a calcolare F(5) poi F(10) poi F(20) e poi F(40).

Le due implementazioni ottimizzate differiscono solo in come passano 
i parametri al chiamante ma entrambe sfruttano il fatto che, calcolando 
F(n-1) devi aver calcolato gia' F(n-2) e che quindi ricalcolarlo sia una
perdita di tempo (se riesci a mantenere l'informazione da qualche parte).
Oppure, equivalentemente, che una volta calcolato F(n-2), per calcolare F(n-1) 
basta un'unica operazione.

*/

/* variabile globale per verificare il numero di chiamate ricorsive */
int counter = 0;

int classic_fibonacci(int n) {
  if(n==0) return 0;
  if(n==1) return 1;
  
  counter++;
  
  /* classica definizione dell'n-esimo numero di fibonacci */
  return classic_fibonacci(n-1) + classic_fibonacci(n-2); 
}

/* 
Calcolo dell'n-esimo numero di fibonacci usando p come parametro di uscita
con il quale la funzione passa al chiamante anche l'(n-1)-esimo numero,
cosi' da rendere lineare con n il numero di chiamate ricorsive.
In questo caso si parte da n e si riduce il valore ad ogni chiamata (da cui
il fatto che sia calcolato "all'indietro) come avveniva nel caso classico.
La ricorsione viene quindi chiusa quando n raggiunge 0 o 1 ritornando
il rispettivo numero di fibonacci (0 o 1).
*/
int optimized_fiboacci_backward_worker(int n, int* p) {
  int q,r;
  
  if(n==0) {
    *p=0;
    return 0;
  }
  
  if(n==1) {
    *p=0;
    return 1;
  }
  
  counter++;
  
  /* q e' l'(n-1)-esimo numero di fibonacci e nel parametro di entrata r 
  ricevo l'(n-2)-esimo */
  q = optimized_fiboacci_backward_worker(n-1, &r);
  
  /* il parametro di uscita p ora quindi diventa q, ossia l'(n-1)-esimo numero  */
  *p = q;
  
  /* e ritorno l'ennesimo, calcolato con i valori ricevuti */
  return q+r;
}

/* 
Interfaccia per rendere il prototipo della funzione ottimizzata identica 
a quella della funzione classica
 */
int optimized_fiboacci_backward(int n) {
  int r; /* r verra' impostato sull'(n-1)-esimo numero di fibonacci, che pero' 
  qui non mi interessa e quindi ignoro */
  return optimized_fiboacci_backward_worker(n, &r);
}



/* 
Calcolo dell'n-esimo numero di fibonacci "in avanti" ossia
portandosi dietro a e b, rispettivamente l'(n-1) e l'(n-2)-esimo.
Stavolta parto da i primi due numeri di fibonacci (0 e 1) e calcolo ogni 
volta un'interazione successiva (da cui il fatto che sia calcolato "in avanti)
come avviene nell'algoritmo matematico originale. Ad ogni modo n diminuisce e
anche in questo caso, quindi, la ricorsione viene chiusa quando n raggiunge
0 o 1. Solo che in questi casi viene ritornato l'ultimo (o il penultimo) numero
di fibonacci calcolato.
*/
int optimized_fiboacci_forward_worker(int n, int a, int b) {
  int t;
  counter++;
  
  if(n==0)
    return b;
    
  if(n==1)
    return a;
  
  t = a+b; /* a e b sono rispettivamente l'ultimo e il penultimo numero di fibonacci
  quindi t diventa il successivo   */
  
  /* e quindi la chiamata avviene "dimenticandosi" di b, scalando a come penultimo
  e impostando t come ultimo numero di fibonacci calcolato */
  return optimized_fiboacci_forward_worker(n-1, t, a);

}

/* 
Interfaccia per rendere il prototipo della funzione ottimizzata identica 
a quella della funzione classica
 */
int optimized_fiboacci_forward(int n) {
  return optimized_fiboacci_forward_worker(n, 1, 0);
}


/*
Un semplice main che crea un array e lo passa alla funzione appena definita, 
stampando i risultati.
*/
int main(void) {
  int n;
  
  printf("Inserisci un numero: ");
  scanf("%d", &n);
  
  counter = 0;
  printf("Fibonacci classico.\n");
  printf("Il numero di Fibonacci corrispondente � %d\n", classic_fibonacci(n));
  printf("Numero di chiamate ricorsive = %d\n\n", counter);
  
  counter = 0;
  printf("Fibonacci ottimizzato (all'indietro).\n");
  printf("Il numero di Fibonacci corrispondente � %d\n", optimized_fiboacci_backward(n));
  printf("Numero di chiamate ricorsive = %d\n\n", counter);

  counter = 0;
  printf("Fibonacci ottimizzato (in avanti).\n");
  printf("Il numero di Fibonacci corrispondente � %d\n", optimized_fiboacci_forward(n));
  printf("Numero di chiamate ricorsive = %d\n\n", counter);

  return 0;
}


