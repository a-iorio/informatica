#include <stdio.h>
/*
4) Modificare le procedure ricorsive dell'esercizio precedente per stampare 
l'array in senso inverso.
void stampaInverso1 (int vet[], int dim);
void stampaInverso2 (int vet[], int i, int dim);
----------------------

In questo esercizio, stampiamo i vettori all'inverso nel modo piu' ovvio,
ossia stampando ogni volta l'ultimo elemento del sotto-vettore invece del 
primo come facevamo nell'esercizio precedente.
Ovviamente, nel secondo caso l'ultimo elemento del sotto-vettore e' individuato
dall'indice i mentre nel primo e' la dimensione che si accorcia e l'inizio che 
slitta che esegue il trucco.
    
stampaInverso1 ricorre sul sotto-vettore che inizia sempre dal primo elemento
ma che ovviamente ha dimensione (dim-1), perdendo quindi l'ultimo elemento.

stampaInverso2 invece fa avanzare l'indice i, ricorrendo sullo stesso vettore. 
E' quindi l'indice i stesso a segnalare la terminazione della ricorsione, 
quando raggiunge la dimensione del vettore.

*/

void stampaInverso1 (int vet[], int dim) {
  
  if(dim==0) {
    printf("\n");
    return;
  }
  
  printf("%d ", vet[dim-1]);
  stampaInverso1(vet, dim-1);
  
}

void stampaInverso2 (int vet[], int i, int dim) {
  
  if(i==dim) {
    printf("\n");
    return;
  }
  
  printf("%d ", vet[dim-(i+1)]);
  stampaInverso2(vet, i+1, dim);
  
}


/*
Un semplice main che crea un array e lo passa alle funzioni appena definite, 
stampando i risultati.
*/
main () {
  int vett[] = {1,2,3,4,5,6,7,8,9,10};
  
  stampaInverso1(vett,10);
  
  stampaInverso2(vett,0,10);
}
