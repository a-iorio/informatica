#include <stdio.h>

/*
16) Scrivere una funzione che ricevuto un array di interi, calcoli e 
stampi ricorsivamente tutte le permutazioni dell'array. 

---------------

In questo esercizio avanzato vediamo l'uso di un ciclo for all'interno di una 
funzione ricorsiva. In questo caso, infatti, la ricorsione avviene sul sotto-array 
definito dal contatore first, ma ad ogni chiamata, avvengono un numero di chiamate 
pari al numero di elementi rimasti da first alla fine. 
Ognuna di queste chiamate ricorsive riceve un array diverso, in cui il primo 
elemento viene scambiato con uno dei successivi.
Il risultato e' che ogni sotto-array viene iterato completamente prima che la 
funzione ritorni e quindi, nel complesso tutto l'array viene iterato.

*/

int counter=0;


void permuta (int arr[], int first, int dim) {
  int temp, k, i;
  
  counter++;

  if (first == dim-1) {
    /* il sotto-array ha un elemento solo, non ci sono altre permutazioni possibili,
    stampiamo tutto l'array. */
    printf("[ ");
    for (i=0; i < dim; i++)
      printf ("%d ", arr[i]);
    printf ("]\n");  
  }
  else {
    
    /* ricorro sul sotto-array immutato a partire dall'elemento successivo a first */
    permuta (arr, first + 1, dim);
    
    /* poi scambio l'elemento first con ciascuno dei successivi 
    e ogni volta ricorro sul sotto-array a partire da quello dopo */
    for (k = first+1; k < dim; k++) {
      temp = arr[k];
      arr[k] = arr[first];
      arr[first] = temp;

      permuta (arr, first + 1, dim);

      temp = arr[k];
      arr[k] = arr[first];
      arr[first] = temp;
    }
  }
    
}

/*
Un semplice main che crea un array e lo passa alla funzione appena definita, 
stampando i risultati.
*/
int main (void) {
  int arr[4] = {1,2,3,4};

  permuta (arr, 0, 4);
  printf("Numero di chiamate ricorsive effettuate: %d\n", counter);

  return 0;
}


