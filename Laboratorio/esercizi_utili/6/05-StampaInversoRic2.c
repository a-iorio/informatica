#include <stdio.h>
/*
5) Svolgere l'esercizio precedente stampando sempre e solo il primo elemento 
dell'array passato (oppure, se cosi avete risolto l'esercizio 4, sempre e solo 
l'ultimo elemento dell'array passato).
Suggerimento: che succede se la funzione ricorre prima di stampare?
----------------------

Qui l'inversione della stampa non viene dal fatto che stampiamo ogni volta 
l'ultimo elemento del vettore come nel precedente esercizio, infatti 
guardando bene, stampiamo ogni volta il primo elemento del sotto array.
L'inversione deriva dal fatto che eseguiamo la ricorsione prima di aver
stampato qualunque cosa e di conseguenza la prima istruzione di printf 
ad essere eseguita e' in effetti quella eseguita sull'ultimo sotto array
acceduto prima della chiusura della ricorsione, ossia il sotto array che 
contiene solo l'ultimo elemento. Da li' in poi, usciamo da ogni chiamata di 
funzione ed eseguiamo la stampa della chiamata precedente, stampando in effetti
i caratteri in senso inverso.
    
stampaInverso1 ricorre sul sotto-vettore che inizia dal secondo elemento 
(vet+1, aritmetica dei puntatori) e che ovviamente ha dimensione (dim-1).

stampaInverso2 invece fa avanzare l'indice i, ricorrendo sullo stesso vettore. 
E' quindi l'indice i stesso a segnalare la terminazione della ricorsione, quando 
raggiunge la dimensione del vettore.

*/

void stampaInverso1 (int vet[], int dim) {
  
  if(dim==0) {
    printf("\n");
    return;
  }
  
  stampaInverso1(vet+1, dim-1);
  printf("%d ", vet[0]);
  
}

void stampaInverso2 (int vet[], int i, int dim) {
  
  if(i==dim) {
    printf("\n");
    return;
  }
  
  stampaInverso2(vet, i+1, dim);
  printf("%d ", vet[i]);
  
}


/*
Un semplice main che crea un array e lo passa alle funzioni appena definite, 
stampando i risultati.
*/
main () {
  int vett[] = {1,2,3,4,5,6,7,8,9,10};
  
  stampaInverso1(vett,10);  
  stampaInverso2(vett,0,10);
  printf("\n");

}
