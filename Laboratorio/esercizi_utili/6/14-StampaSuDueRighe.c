#include <stdio.h>
/*
14) Scrivere una procedura ricorsiva che riceva un array di caratteri e stampi 
la prima meta' su una riga e la seconda meta' su una riga diversa. 
Se l'array ha un numero dispari di elementi, la lettera centrale dovra' 
essere stampata su una riga a parte, tra la prima meta' e la seconda meta'.
-------------

In questo esercizio dobbiamo distinguere *ricorsivamente* se un vettore ha numero
di elementi pari o dispari. Inoltre dobbiamo dividere il vettore in due parti
che vanno stampate in modi diversi (non proprio, ma sarebbe possibile).

Per farlo, stampiamo due elementi per volta, uno dalla testa e uno dalla coda.
Pero' dobbiamo stampare quello dalla coda solo dopo aver raggiunto la meta', cioe'
dopo che la ricorsione ha iniziato a chiudere: infatti *mangiando* un elemento 
dalla testa e uno dalla coda, la ricorsione si chiude nel mezzo. 
Quindi le due printf sono una prima e una dopo la chiamata ricorsiva.

Inoltre abbiamo due casi base: se infatti il numero di elementi e' pari, la dimensione 
dell'ultimo sotto-vettore sara' 0 (non c'e' elemento centrale), se invece il numero di
elementi e' dispari, la dimensione dell'ultimo sotto-vettore sara' 1 (cioe' c'e' un 
elemento centrale che va quindi stampato su riga a parte).

*/


void stampaSuDueRighe(char vet[], int dim) {
  
  if(dim==0) {
    printf("\n");
    return;
  }
  
  if(dim==1) {
    printf("\n%c\n", vet[0]);
    return;
  }
  
  printf("%c ", vet[0]);
  stampaSuDueRighe(vet+1, dim-2);
  printf("%c ", vet[dim-1]);

}

/*
Un semplice main che crea due array e li passa alla funzione appena definita, 
stampando i risultati.
Notate le dimensioni diverse dei due array per verificare entrambi i casi,
numero di elementi pari e numero di elementi dispari.
*/
main() {

  char arr1[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'l', 'm' };
  char arr2[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'l' };
  
  stampaSuDueRighe(arr1, 11);
  printf("\n---\n");
  stampaSuDueRighe(arr2, 10);
  printf("\n---\n");

}
