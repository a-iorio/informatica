#include <stdio.h>

/*
9) Scrivere una funzione ricorsiva che azzeri tutti gli elementi pari, raddoppi gli 
elementi dispari e ritorni il minimo elemento di un array passato.
--------------
Questa strana funzione deve fare tre operazioni distinte, due sono modifiche agli 
elementi del vettore e la terza e' una ricerca.

Poiche' dobbiamo cercare il minimo prima di modificare il vettore, postponiamo
le modifiche a dopo la chiusura della ricorsione. 

Ovviamente siccome ogni chiamata deve venir chiusa (e ognuna opera su un elemento
diverso) tutti i valori verranno comunque modificati, ancorche' in ordine inverso 
(ma questo non ci crea problemi).

Siccome dobbiamo cercare il minimo, il vettore non puo' essere vuoto. Quindi chiudiamo
la ricorsione quando la dimensione e' uguale a 1, invece come al solito quando la 
dimensione e' uguale a 0.

Poi eseguiamo le modifiche e ritorniamo il valore del minimo del sotto-vettore 
prima calcolato.
*/

int azzeraRaddoppiaTrovaMinimo(int vet[], int dim) {
  int res;
  
  if(dim==1) { 
    res = vet[0];
  }
  else {
    res = azzeraRaddoppiaTrovaMinimo(vet+1, dim-1);
    if(vet[0]<res) res=vet[0];
  }
  
  if(vet[0]%2==0)  
    vet[0] = 0;
  else
    vet[0] *= 2;
  
  return res;
}

/*
Un semplice main che crea un array e lo passa alla funzione appena definita, 
stampando i risultati.
*/
main() {
  int vett[] = {2,12,3,4,5,6,1,7,8,9};
  int res, i;

  printf("L'array e': [ ");
  for(i=0; i<10; i++)   
    printf("%d ", vett[i]);
  printf("]\n");
  
  
  res = azzeraRaddoppiaTrovaMinimo(vett, 10);
  printf("Il minimo e': %d\n", res);
  
  printf("L'array e': [ ");
  for(i=0; i<10; i++)   
    printf("%d ", vett[i]);
  printf("]\n");
  
  
}
