#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*

17) Scrivere un programma che crei casualmente un array di 20 interi, lo stampi 
a video, lo ordini con ordinamento crescente non stretto e quindi lo stampi a 
video nuovamente.
----------


In questo esercizio, per la prima volta, ci sono due difficolta': la prima e' 
ideare un algoritmo per risolvere il problema dato, la seconda e' quella classica 
di tradurre questo algoritmo in un programma funzionante.

In questo caso il problema dato non faceva nessuna richiesta di efficienza alla 
soluzione e quindi possiamo permetterci di trovare ed implementare l'algoritmo 
piu' ovvio e banale che esista.
Comunque il problema dell'ordinamento di un vettore ha molte soluzioni assai piu' 
efficienti di questa ed e' un problema classico della scienza algoritmica.

Notate che una buona specifica dell'algoritmo astratto (cioe' aver molto chiaro 
cosa deve essere fatto e quando) permette di scrivere la sua implementazione (il
codice vero e proprio) con grande facilita'. 

*/

#define DIM 20

main() {

  int i,j;
  int vettore[DIM];
  
  int min;
  int pos;
  
  
  srand(time(NULL)); /*inizializza il generatore di numeri*/
  
  
  /* Crea casualmente il vettore di interi */ 
  for(i=0; i<DIM; i++)  
    vettore[i] = rand()%1000;
  
  /* e lo stampo */
  printf("Il vettore generato e': ");
  for(i=0; i<DIM; i++)  
    printf("%d ", vettore[i]);
  printf("\n\n");
  
  
  /* 
    Strategia di ordinamento banale, con complessita O(n^2)
    Ad ogni iterazione, a partire dall'inizio :
    1) trova l'elemento minore, a partire dall'elemento corrente verso destra
    2) lo scambia con l'elemento corrente
    3) avanza la posizione corrente
  */
  for(i=0; i<DIM; i++) {
    /* il nuovo minimo temporaneo del sottoarray che parte dall'elemento corrente 
    verso destra e' proprio l'elemento corrente */
    min = vettore[i];
    pos = i;
    
    /* cerco il nuovo minimo a partire dall'elemento successivo a quello corrente */
    for(j=i+1; j<DIM; j++) {
      if(vettore[j] < min) {
        min = vettore[j];
        pos = j;
      }
    }
    
    /* eseguo lo scambio solo se ho trovato un nuovo minimo */ 
    if(pos>i) {
      vettore[pos]=vettore[i];
      vettore[i]=min;
    }
    
    /* il for stesso si occupa di avanzare l'elemento corrente */
  }
  
  
  /* Alla fine stampo il vettore ordinato */
  printf("Il vettore ordinato e': ");
  for(i=0; i<DIM; i++)  
    printf("%d ", vettore[i]);
  printf("\n\n");
  

}
