#include <stdio.h>

/*

6) Scrivere un programma che inizializzato un array di 7 interi con valori 
inseriti dall'utente, stampi la somma degli elementi pari e il prodotto degli 
elementi dispari.
Nota: elementi pari o dispari, non "di posizione" pari o dispari.
----------


In questo semplice esercizio chiediamo all'utente di inserire un numero 
predeterminato di interi (da cui l'uso del for e del commento nel printf).

Solo dopo che sono stati inseriti tutti eseguiamo la computazione.
Ovviamente poteva essere fatto insieme, in questo caso particolare, ma non 
e' necessariamente sempre cosi'.

*/

#define DIM 7

main() {

  int arr[DIM];
  int i, ret;
  int somma=0;
  double prodotto=1.0;
  
  /* prima chiediamo tutti i valori */
  for(i=0; i<DIM; i++) {
    printf("Inserisci un intero (ancora %d): ", (DIM-i));
    ret=scanf("%d", &arr[i]);
    if(ret!=1) { printf("Errore!\n"); return; }
  }
  
  /* e solo dopo facciamo i nostri calcoli sull'array */
  for(i=0; i<DIM; i++) {
    if(arr[i]%2==0)
      somma += arr[i];
    else
      prodotto *= arr[i];
  }
  
  printf("Somma degli elementi pari: %d\n", somma);
  printf("Prodotto degli elementi dispari: %.0f\n", prodotto);

}
