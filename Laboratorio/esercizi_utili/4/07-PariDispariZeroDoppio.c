#include <stdio.h>

/*

7) Scrivere un programma che letto un array di 7 interi dall'utente, azzeri 
i valori nelle posizioni pari, raddoppi i valori nelle posizioni dispari e poi 
stampi l'array a video.
Nota: elementi "di posizione" pari o dispari, non elementi pari o dispari.
----------


Esercizio molto simile al precedente, con l'unica differenza che dobbiamo verificare
la posizione (variabile i) e non gli elementi (arr[i]).
Notate anche qui come abbia messo la stampa dell'array in un for separato, per 
semplicita' e correttezza.

*/

#define DIM 7

main() {

  int arr[DIM];
  int i, ret;
  
  /* prima chiedo tutti i valori */
  for(i=0; i<DIM; i++) {
    printf("Inserisci un intero (ancora %d): ", (DIM-i));
    ret=scanf("%d", &arr[i]);
    if(ret!=1) { printf("Errore!\n"); return; }
  }
  
  /* poi faccio i miei calcoli/modifiche */
  for(i=0; i<DIM; i++) {
    if(i%2==0) /* controllo l'indice e non l'elemento! */
      arr[i]=0;
    else
      arr[i] *= 2;
  }
  
  /* e solo alla fine stampo l'array */
  printf("Array: ");
  for(i=0; i<DIM; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n\n");


}
