#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*

15) Scrivere un programma che crei casualmente un array di 100 interi 
compresi fra 1 e 1000. Il programma dovra' quindi chiedere un numero 
all'utente, cercare tutti gli elementi multipli di quel numero e salvare
i puntatori a tali elementi in un array ausiliario (stavolta di puntatori 
ad interi), precedentemente preparato ed inizializzato a NULL.
A quel punto il programma dovra' stampare tutti gli elementi trovati
scorrendo solamente questo array ausiliario.
----------


Questo e' un semplice esercizio di ricerca in un array. 
La difficolta' sta nel fatto che quando trovo un elemento (in questo caso 
un multiplo del numero inserito dall'utente) devo salvare un riferimento
a questo elemento in un array ausiliario di puntatori.
Inoltre devo stare attento a tenere traccia di quanti elementi ho gia' 
inserito nell'array ausiliario, per non sovrascriverli e non dereferenziare
elementi NULL quando poi stampo i risultati.

*/

#define DIM 100

main() {

  int arr[DIM];
  int i, ret, num, j;
  int* auxarr[DIM];
  
  srand(time(NULL));
  
  /* inizializzo i due array, arr con i numeri casuali, auxarr con NULL */
  for(i=0; i<DIM; i++) {
    arr[i] = rand()%1000 + 1;
    auxarr[i] = NULL;
  }
  
  printf("Inserisci un numero: ");
  ret=scanf("%d", &num);
  if(ret!=1) { printf("Errore!\n"); return; }
  
  
  /* 
    Il for scorre l'array di numeri. Quando trovo un elemento multiplo di num
    lo devo "riportare" nell'array ausiliario, copiandoci il suo puntatore,
    nella prima posizione libera. 
    Di questa prima posizione libera e' tenuta traccia da j, inizialmente 0, che
    aumenta ogni volta che salviamo un nuovo elemento nell'array ausiliario.
  */
  j=0;
  for(i=0; i<DIM; i++) {
    if(arr[i]%num == 0)
      auxarr[j++] = &arr[i];
  }
  
  /* 
    alla fine stampo tutti gli elementi multipli di num semplicemente accedendo 
    all'array ausiliario (in cui so, per costruzione, che ci sono j elementi.
  */
  for(i=0; i<j; i++) 
    printf("%d ", *auxarr[i]);
  
  

}
