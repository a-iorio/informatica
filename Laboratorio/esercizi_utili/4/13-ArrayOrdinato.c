#include <stdio.h>

/*

13) Scrivere un programma che crei un array di 7 interi forniti
dall'utente, controlli se l'array e' ordinato in ordine strettamente
descrescente e stampi a video sia l'array che il risultato ("E'
ordinato" oppure "Non e' ordinato: X" dove X e' la posizione del
primo elemento fuori ordine).
----------


Controllare se un array e' ordinato significa dover scorrere l'array
e confrontare ogni elemento con il precedente (o il successivo) per
verificare che, a due a due, rispettino l'ordinamento.
L'unica difficolta' in questo esercizio quindi e' stare attenti alle 
condizioni di terminazione: dovendo confrontare elementi a due a due,
il classico ciclo da 0 a DIM non va piu' bene.

*/

#define DIM 7

main() {

  int arr[DIM];
  int i;
  int pos;

  for(i=0; i<DIM; i++) {
    printf("Inserisci un intero (ancora %d): ", (DIM-i));
    ret=scanf("%d", &arr[i]);
    if(ret!=1) { printf("Errore!\n"); return; }
  }
  
  /* 
    pos scorre l'array. Poiche' deve confrontare un elemento con quello 
    precedente, pos deve partire dal secondo (pos=1). 
    Notare come nel corpo dello while non faccio quasi nulla, avanzo solo il cursore.
    Questo perche' mi interessa solo il punto dove il controllo fallisce.
    Se il controllo non fallisce mai (array ordinato) lo while esce perche' il
    cursore e' arrivato in fondo all'array.
  */
  pos = 1;
  while(pos<DIM && arr[pos-1]>arr[pos]) {
    pos++;
  }

  /* alla fine stampo l'array */
  printf("Array: ");
  for(i=0; i<DIM; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n\n");

  /* 
    Stampo il risultato: se il controllo e' fallito prima della fine, l'array non e' ordinato. 
    Se invece e' arrivato in fondo, e' ordinato.
  */
  if(pos<DIM)
    printf("Non ordinato: %d\n", pos);
  else
    printf("Ordinato.\n");
  

}
