#include <stdio.h>

/*

3) Scrivere un programma che definisce un array di 10 interi e stampa a video
la somma e il prodotto degli elementi e poi il massimo ed il minimo dell'array
(con le loro posizioni).
----------


In questo semplice esercizio dobbiamo scorrere un array (per adesso predeterminato ma 
il procedimento e' identico) per cercare alcuni elementi (massimo e minimo) e tenere 
traccia di alcune proprieta' complessive (somma e prodotto).
Notate la variabile prodotto definita come double (visto quanto sale velocemente il 
risultato).

*/

main() {

  int arr[] = { 25, 6, 8, 42, 5, 34, 21, 13, 7, 4 };
  int somma=0;
  double prodotto=1.0;
  int max=-1, maxpos=-1;
  int min=-1, minpos=-1;
  int i;
  
  /* in un solo ciclo facciamo tutte le operazioni richieste, una per volta*/
  for(i=0; i<10; i++) {
  
    somma += arr[i];
    
    prodotto *= arr[i];
    
    /* come in precedenza, se e' la prima iterazione, l'elemento e' sicuramente il massimo */
    if(max==-1 || arr[i]>max) {
      max = arr[i];
      maxpos = i;
    }
    
    if(min==-1 || arr[i]<min) {
      min = arr[i];
      minpos = i;
    }
    
  }

  /* alla fine stampo l'array */
  printf("Array: ");
  for(i=0; i<10; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n\n");
  
  printf("Somma degli elementi: %d\n", somma);
  printf("Prodotto degli elementi: %.0f\n", prodotto);
  printf("Massimo: %d in posizione %d\n", max, maxpos);
  printf("Minimo: %d in posizione %d\n", min, minpos);

}
