#include <stdio.h>

/*

9) Scrivere un programma che crei e inizializzi un array di interi con dimensione 
DIM = 10 e quindi lo scorra con un ciclo for e lo stampi usando solo un puntatore 
ad interi e l'aritmetica dei puntatori (ossia che non usi nessuna variabile indice 
come di solito viene fatto per scorrere un array).
----------


Esercizio del tutto ovvio, se non per l'uso dei puntatori.
Guardiamo nel dettaglio il for:
1) p=&arr[0]
   inizializzo il puntatore (cursore) all'indirizzo del primo elemento dell'array
   
2) p != &arr[0]+DIM
   per il controllo, prendo come barriera il confine dell'array calcolato con
   l'aritmetica dei puntatori, sommando DIM all'indirizzo del primo elemento dell'array

3) p++
   siccome p e' un puntatore, sommandoci 1 si fa scorrere il puntatore all'elemento 
   successivo dell'array (aritmetica dei puntatori).
   
Ovviamente poi nel printf devo stampare l'elemento puntato dal mio cursore (che siccome
scorre sull'array, cambia ogni volta).

*/

#define DIM 10

main() {

  int arr[DIM] = { 25, 6, 8, 42, 5, 34, 21, 13, 7, 4 };
  int* p;
  
  printf("Array: ");
  for( p=&arr[0]; p!=&arr[0]+DIM; p++) {
    printf("%d ", *p);
  }
  printf("\n");


}
