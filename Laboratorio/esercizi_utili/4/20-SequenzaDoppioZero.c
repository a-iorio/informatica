#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*

20) Scrivere un programma che chiede all'utente di inserire una sequenza di 
valori fra 0 e 9 e terminata da due 0 consecutivi (che non fanno parte della 
sequenza).
Il programma dovra' quindi stampare le frequenza con cui ciascuna cifra compare 
nella sequenza.
Suggerimento: preparare una array di 10 posizioni inizializzate a 0 che contengano
le frequenze delle varie cifre.
----------


In questo tipo di esercizi l'unica vera difficolta' e' gestire le condizioni di 
terminazione correttamente.
In questo caso, se ho un solo zero seguito da numeri diversi da zero, la sua frequenza
deve essere contata, mentre se ho due zero consecutivi devo terminare il ciclo
e non contare la loro frequenza.
In questa soluzione ho scelto di contare comunque la frequenza degli zero (per 
semplicita') e poi di sistemare la loro frequenza dopo (visto che i due zero
ci sono sempre).

*/


main() {

  /* array delle frequenze delle varie cifre */
  int arr[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int num, i;
  int flag = 0;
  
  /* il flag conta quanti zero consecutivi sono stati visti */
  while(flag<2) {
    printf("Inserisci un numero fra 0 e 9 (due zero per terminare): ");
    ret=scanf("%d", &num);
    if(ret!=1 || num<0 || num>9) { printf("Errore!\n"); return; }
    
    /* aggiorno comunque le frequenze, sistemo i due zero finali dopo */
    arr[num]++;
    
    /* se non ho letto uno zero, azzero il flag perche' nessuno zero e' stato visto */
    if(num!=0) 
      flag = 0;
    else 
      flag++;
  }
  
  /* 
    poiche' i due zero finali (che ci sono sempre) non fanno parte della sequenza 
    ma sono stati contati, adesso vanno tolti
  */
  arr[0] -= 2;
  
  /* Stampiamo le frequenze */
  for(i=0; i<10; i++) 
    printf("Frequenza di %d: %d\n", i, arr[i]);
  
  

}
