#include <stdio.h>

/*
10) Definire un nuovo tipo di dato capace di rappresentare una data.
Scrivere poi delle opportune funzioni/procedure che 
  - ricevuta una data la aggiorni al giorno successivo (ignorando gli anni 
    bisestili);
  - ricevute due date verifichino che la prima preceda la seconda;
  - ricevute due date, ritornino la differenza in anni fra la prima e la seconda.
-------------------

Il tipo di dato e' molto semplice e ovvio, anche se una ragionevole (ma non richiesta)
modifica potrebbe essere di avere mese come tipo enumerato.
Notate la funzione di utilita' per verificare se il mese e' di 30 giorni: poteva
essere fatto nella funzione principale ma cosi' e' molto piu' compatto.

*/

typedef struct date {
  int giorno;
  int mese; 
  int anno;
} Data;

int meseDiTrenta(int mese) {
  return (mese==4 || mese==6 || mese==9 || mese==11);
}

/*
la funzione aggiorna la data: ovviamente se il fine mese o il fine anno vengono
sforati, bisogna agire di conseguenza, resettare il giorno (o il mese) e aggiornare
il mese (o l'anno).
*/
Data giornoSuccessivo(Data d) {
  d.giorno++;
  
  if((d.mese==2 && d.giorno>28) ||
     (meseDiTrenta(d.mese) && d.giorno>30) ||
     (d.giorno>31))  
  {
    d.giorno=1;
    d.mese++;
    if(d.mese>12) {
      d.mese=1;
      d.anno++;
    }
  }
  return d;  
}

/*
precede ritorna:
  -1 se b precede a
   1 se a precede b
   0 se sono la stessa data.
   
   Notate come, se gli anni sono diversi, non sia necessario controllare mesi e 
   giorni (ovviamente): precede cioe' implementa un ordinamento lessicografico.
*/
int precede(Data a, Data b) {
  int res;
  if(a.anno<b.anno){
    res=1;
  }
  else if(a.anno>b.anno) {
    res=-1;
  }
  else {
    if(a.mese<b.mese) {
      res=1;
    }
    else if(a.mese>b.mese) {
      res=-1;
    }
    else {
      if(a.giorno<b.giorno) {
        res=1;
      }
      else if(a.giorno>b.giorno) {
        res=-1;
      }
      else 
        res=0;
    }
  }
  return res;
}

/* Voglio che la differenza in anni sia sempre positiva, indipendentemente
dall'ordine con cui le date sono passate */
int differenzaAnni(Data a, Data b) {
  if(a.anno>b.anno) 
    return a.anno-b.anno;
  else
    return b.anno-a.anno;
}

/* Funziona ausiliaria per stampare le date */
void stampaData(Data a) {
  printf("Data(%d, %d, %d)\n", a.giorno, a.mese, a.anno);
}

main(){
  int res;
  Data inizio_anno = {1,1,2012};
  Data fine_anno = {31,12,2012};
  Data inizio_secolo = {1,1,2001};
  
  printf("Giorno dopo capodanno: ");
  stampaData( giornoSuccessivo(inizio_anno) );
  printf("\n");
  
  printf("Giorno dopo l'ultimo dell'anno: ");
  stampaData( giornoSuccessivo(fine_anno) );
  printf("\n");
  
  printf("Quale viene prima?\n");
  stampaData( inizio_anno );
  stampaData( fine_anno );
  res = precede(inizio_anno, fine_anno);
  if(res==1) printf("Risposta: la prima\n");
  else if(res==-1) printf("Risposta: la seconda\n");
  else printf("Risposta: nessuna\n");
  printf("\n");
  
  printf("Differenza in anni:\n");
  stampaData( inizio_secolo );
  stampaData( inizio_anno );
  printf("Risposta: %d\n", differenzaAnni(inizio_anno, inizio_secolo));
  
}

