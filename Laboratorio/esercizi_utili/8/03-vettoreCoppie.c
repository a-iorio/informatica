#include <stdio.h>
#include <stdlib.h>

/*
3) Scrivere una funzione che riceva un array di coppie di interi e le inizializzi
con i primi n numeri dispari (n e' la lunghezza del vettore) e i loro quadrati.

*/

typedef struct coppia {
  int x;
  int y;
} Coppia;

/*
Dopo la typedef, Coppia e' un tipo di dato completamente legittimo e puo' essere
usato nello stesso modo con cui si usano i tipi standard.
*/
void initCoppie(Coppia arr[], int dim) {
  int i;
  for(i=0; i<dim; i++) {
    /* Ovviamente arr[i] ha tipo Coppia quindi va trattato come tale */
    arr[i].x = i+1;
    arr[i].y = (i+1)*(i+1);
  }
}

main(){
  int i;
  Coppia vett[10];
  
  initCoppie(vett,10);
  
  for(i=0; i<10; i++) 
    printf("(%d, %d)\n", vett[i].x, vett[i].y);
  
}

