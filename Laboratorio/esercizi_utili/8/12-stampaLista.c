#include <stdio.h>
#include <stdlib.h>

/*
12) Scrivere un programma che crei una lista di interi come nell'esercizio 7 e 
la stampi a video in questo modo:
1 -> 2 -> 3 -> 4 -> //
Ricordarsi di deallocare la lista prima di uscire dal main (e verificare con 
valgrind che questo sia avvenuto, vedi note).
-----------------

Dopo aver letto un intero dall'utente, il programma deve creare e inizializzare 
una lista con quel numero di elementi.
Per farlo, bisogna creare il primo separatamente (il primo e' la testa e va 
tenuta da parte) e collegare ogni altro elemento al precedente.
Ci serve quindi un puntatore ausiliario sul quale creeremo il nuovo nodo.
Alla fine, l'ultimo elemento viene fatto puntare a NULL (coda).

Da notare come si scorre una lista: non si contano gli elementi (visto che in 
generale non e' dato sapere quanti siano) ma si controlla se abbiamo raggiunto
la coda o meno! Ossia, l'unico fattore per dire che la lista e' terminata
e' il NULL nel campo next della coda!

*/

typedef struct El {
  int info;
  struct El *next;
} ElementoListaInt;
typedef ElementoListaInt* ListaDiInteri;


main() {

  int i, n, res;
  ListaDiInteri testa, aux;

  printf("Inserisci la lunghezza della lista: ");
  res=scanf("%d", &n);
  if(res<1) return;

  if(n>0) {
    testa = malloc(sizeof(ElementoListaInt));

    /* creazione ed inizializzazione */
    aux = testa;
    for(i=1; i<n; i++) {
      aux->info = i;
      aux->next = malloc(sizeof(ElementoListaInt));
      aux = aux->next;
    }
    aux->info=n;
    aux->next=NULL; /* la coda punta a null */

    /* scorriamo la lista dall'inizio per stamparla. La testa deve restare immutata */
    aux=testa;
    while(aux!=NULL) {
      printf("%d -> ", aux->info);
      aux = aux->next;
    }
    printf("//\n");

    /* Scorriamo la lista per deallocarla. La testa puo' cambiare */
    while(testa!=NULL) {
      aux = testa->next;
      free(testa);
      testa=aux;
    }
  }


}
