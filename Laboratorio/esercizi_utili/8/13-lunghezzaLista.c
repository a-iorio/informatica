#include <stdio.h>
#include <stdlib.h>
/*
13) Scrivere un programma che, creata una lista di interi come nell'esercizio
precedente, calcoli e stampi la sua lunghezza percorrendo la lista dall'inizio
alla fine.
Ricordarsi di deallocare la lista prima di uscire dal main (e verificare con 
valgrind che questo sia avvenuto, vedi note).
------------

Praticamente identico all'esercizio precedente.

Per contare gli elementi della lista usiamo lo stesso algoritmo usato per stamparla,
ossia la percorriamo dall'inizio (testa) fino alla fine (coda), fine che 
riconosciamo perche' punta a NULL.

*/

typedef struct El {
  int info;
  struct El *next;
} ElementoListaInt;
typedef ElementoListaInt* ListaDiInteri;


main() {

  int i, n, len;
  ListaDiInteri testa, aux;

  printf("Inserisci la lunghezza della lista: ");
  scanf("%d", &n);

  if(n>0) {
    testa = malloc(sizeof(ElementoListaInt));

    /* creazione ed inizializzazione */
    aux = testa;
    for(i=1; i<n; i++) {
      aux->info = i;
      aux->next = malloc(sizeof(ElementoListaInt));
      aux = aux->next;
    }
    aux->info=n;
    aux->next=NULL; /* la coda punta a null */

    /* scorriamo la lista dall'inizio per stamparla. La testa deve restare immutata */
    aux=testa;
    while(aux!=NULL) {
      printf("%d -> ", aux->info);
      aux = aux->next;
    }
    printf("//\n");
    
    /* scorriamo di nuovo la lista dall'inizio. Stavolta ci interessa solo contare
    quanti elementi ci sono prima della coda. */
    len=0;
    aux=testa;
    while(aux!=NULL) {
      len++;
      aux = aux->next;
    }
    printf("la lunghezza calcolata della lista e': %d\n", len);
    
    /* Scorriamo la lista per deallocarla. La testa puo' cambiare */
    while(testa!=NULL) {
      aux = testa->next;
      free(testa);
      testa=aux;
    }
  }


}
