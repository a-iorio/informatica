#include <stdio.h>
#include <stdlib.h>

/*
5) Scrivere un programma che crei dinamicamente una lista di 3 interi e li 
inizializzi ai primi tre naturali.
Il programma deve deallocare correttamente la lista prima di uscire, verificare 
con valgrind che questo sia avvenuto (vedi note).
----------

Da un punto di vista sintattico, una lista non e' nulla di piu' di un insieme 
di istanze di una struct con un campo puntatore al tipo della struct stessa.
Cio' che rende questo insieme una lista e':
 - esiste un elemento speciale chiamato testa
 - esiste un elemento speciale chiamato coda
 - ogni elemento, escluso la coda, e' collegato a quello successivo
 - ogni elemento, escluso la testa, e' raggiungibile solo attraverso questo
 collegamento
 - la coda ha il valore NULL nel campo puntatore.
 
 Notate che l'ultimo requisito e' essenziale per la definizione di una lista corretta.
 
*/

typedef struct El {
  int info;
  struct El *next;
} ElementoListaInt;
typedef ElementoListaInt* ListaDiInteri;


main(){
  ListaDiInteri a, b, c;

  a = malloc(sizeof(ElementoListaInt));
  b = malloc(sizeof(ElementoListaInt));
  c = malloc(sizeof(ElementoListaInt));

  /* una volta allocato lo spazio, i tre elementi non sono ancora una lista: 
  vanno collegati tra loro. */

  a->info=1;
  a->next=b;
  
  b->info=2;
  b->next=c;
  
  /* come dicevamo, l'ultimo elemento (coda) *DEVE* avere NULL nel campo next! */
  c->info=3;
  c->next=NULL;

  /* anche se in questo caso non e' importante, bisogna stare attenti all'ordine
  con cui una lista viene distrutta: e' infatti illegale accedere ai campi di
  un'area di memoria che sia stata liberata con free. 
  Il che significa che se non avessi da parte il puntatore di b e c, se eseguo
  la free(a) per prima, dopo non potrei raggiungere piu' b e c e quindi non 
  potrei distruggerli! */
  
  free(c);
  free(b);
  free(a);

}

