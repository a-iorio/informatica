#include <stdio.h>
#include <stdlib.h>

/*
2) Allocare dinamicamente tre istanze del tipo di dato coppia appena definito, 
inizializzarle con i valori dei primi tre numeri primi e dei loro quadrati e 
quindi dellaocarle.

*/

typedef struct coppia {
  int x;
  int y;
} Coppia;


main(){
  Coppia *a, *b ,*c;

  a = malloc(sizeof(Coppia));
  b = malloc(sizeof(Coppia));
  c = malloc(sizeof(Coppia));

  
  /* a,b e c sono puntatori, quindi prima di accedere ai campi della struct
  i puntatori vanno dereferenziati. 
  Notate le parentesi, necessarie perche' l'operatore di dereferenziazione (*) 
  ha priorita' minore dell'operatore accesso alla struttura (.) */
  (*a).x=1;
  (*a).y=1;

  /* oppure possiamo usare l'operatore freccia -> che esegue le due cose insieme
  e senza la necessita' di usare parentesi. */
  b->x=2;
  b->y=4;

  c->x=3;
  c->y=9;

  free(a);
  free(b);
  free(c);

}

