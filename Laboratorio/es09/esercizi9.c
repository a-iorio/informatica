/*
1)Realizzare un programma C che legge e memorizza in un VLA una
sequenza di double di lunghezza non nota a tempo di compilazione.
Richiedere all'utente di specificare la lunghezza prima di immettere la
sequenza.

#include <stdio.h>

int main(){
	int n, i=0;
	do{
		printf("Max lunghezza sequenza:");
		scanf("%d", &n);
	}
	while(!n);

	double sequence[n];
	do{
		printf("Inserisci %d numero: ", i+1);
		scanf("%lf", &sequence[i]);
		i++;
	}
	while(i<n);

	for(i=0; i<n; i++){
		printf("%lf\n", sequence[i]);
	}
	return 0;
}

2)Modificare la soluzione all'esercizio 1 in modo da utilizzare la funzione
malloc() per allocare l'array dopo aver letto la lunghezza.
Verificare la corretta allocazione, deallocare la struttura e gestire gli errori.

#include <stdio.h>
#include <stdlib.h>

int main(){
	int n, i=0;
	do{
		printf("Max lunghezza sequenza:");
		scanf("%d", &n);
	}
	while(!n);

	double *sequence;
	sequence = malloc(sizeof(double)*n);
	if(sequence == NULL ){
	    fprintf(stderr,"Errore fatale!\n");
    	exit(EXIT_FAILURE);
	}
	else{
		do{
			printf("Inserisci %d numero: ", i+1);
			scanf("%lf", &sequence[i]);
			i++;
		}
		while(i<n);

		for(i=0; i<n; i++){
			printf("%lf\n", sequence[i]);
		}
	}

	free(sequence);

	return 0;
}

3)Eseguire il seguente programma C
#include <stdio.h>
#include <stdlib.h>
#define SIZE 10000000000
int main (void) {
 double * h;
 h = malloc(SIZE*sizeof(double));
 if ( h == NULL ) {
 perror("malloc");
 exit(EXIT_FAILURE);
 }
 printf("Allocato h = %p\n",(void *) h);
 return 0;
}
cosa succede ? Cercate di capire che cosa succede quando va in esecuzione
perror() andando a verificare il manuale (sezione 3 man 3 perror)

4)Modificare la soluzione all'esercizio 2 in modo da utilizzare la funzione
realloc() per fare crescere dinamicamente l'array senza richiedere la
lunghezza della sequenza.
Verificare la corretta allocazione, deallocazione e gestire gli errori.

#include <stdio.h>
#include <stdlib.h>
typedef enum{false,true} bool;
int main(){
	int i=0;
	bool fine=false;
	double *sequence;
	sequence = malloc(sizeof(double));

	if(sequence == NULL ){
	    fprintf(stderr,"Errore fatale!\n");
    	exit(EXIT_FAILURE);
	}
	else{
		do{
			printf("Inserisci %d numero: ", i+1);
			sequence = realloc(sequence, sizeof(double));
			if(sequence == NULL ){
			    fprintf(stderr,"Errore fatale!\n");
		    	exit(EXIT_FAILURE);
			}
			scanf("%lf", &sequence[i]);
			if(sequence[i]==0.0) fine = true;
			i++;
		}
		while(!fine);
	}

	free(sequence);
	
	return 0;
}

5) Scrivere un nuovo tipo di dato coppia di interi.
Inizializzare tre istanze di coppie con i primi tre numeri naturali e i loro
doppi.

#include <stdio.h>
typedef struct coppie_interi{
	int primo;
	int secondo;
} coppia;

int main(){
	coppia coppie[3];
	int i;
	
	for(i=0; i<3; i++){
		coppie[i].primo = i+1;
		coppie[i].secondo = 2*(i+1);
		printf("%d %d \n", coppie[i].primo, coppie[i].secondo);
	}

	return 0;
}

6)Allocare dinamicamente tre istanze del tipo di dato coppia appena
definito, inizializzarle con i valori dei primi tre numeri primi e dei loro
quadrati e quindi dellaocarle.

#include <stdlib.h>
#include <stdio.h>
typedef struct coppie_interi{
	int primo;
	int secondo;
} coppia;

int main(){
	int i;
	coppia *coppie;
	coppie = malloc(sizeof(coppia)*3);
	for(i=0; i<3; i++){
		coppie[i].primo = i+1;
		coppie[i].secondo = (i+1)*(i+1);
	}
	free(coppie);
}	

7)Scrivere un programma che crei una lista di 3 interi e li inizializzi ai
primi tre naturali.

#include <stdlib.h>
#include <stdio.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

int main(){
	int i;
	ListaDiElementi aux, lista = NULL;

	for(i=0; i<3; i++){
		aux = malloc(sizeof(ElementoLista));
		aux->info = i+1; 
		aux->next = lista;
		lista = aux;
	}
}

8) Scrivere un programma che crei dinamicamente una lista di 3 interi e li
inizializzi con valori chiesti all'utente. Il programma deve deallocare
correttamente la lista prima di uscire, verificare con valgrind che questo sia
avvenuto (se correttamente installato sul vostro pc).

#include <stdlib.h>
#include <stdio.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

int main(){
	int i;
	ListaDiElementi aux, lista=NULL;

	for(i=3; i>0; i--){
		aux = malloc(sizeof(ElementoLista));
		printf("Inserisci %d numero: ", i);
		scanf("%d", &(aux->info));
		aux->next = lista;
		lista = aux;
	}

	while (lista != NULL) {
		aux = lista;
		lista = (lista)->next;
		free(aux);
	}

	return 0;
}


9) Scrivere un programma che chieda all'utente un numero N e crei una
lista con N elementi, inizializzata con i primi N naturali
Il programma deve deallocare correttamente la lista prima di
uscire,verificare con valgrind che questo sia avvenuto (se correttamente
installato sul vostro pc).

#include <stdio.h>
#include <stdlib.h>

typedef struct EL{
	int info;
	struct EL *next;
} ElementoLista;

typedef ElementoLista *ListaDiElementi;

int main(){
	int n=0, i;
	ListaDiElementi aux, lista=NULL;

	do{
		printf("Inserisci N: ");
		scanf("%d", &n);
	}
	while(n<=0);
	
	for(i=0; i<n; i++){
		aux = malloc(sizeof(ElementoLista));
		aux->info=i+1;
		aux->next=lista;
		lista=aux;
	}

	while(lista!=NULL){
		aux = lista;
		lista = lista->next;
		free(aux);
	}

	return 0;
}


10) Scrivere un programma che crei una lista di interi come nell'esercizio 8
e la stampi a video in questo modo:
1 -> 2 -> 3 -> 4 -> //
Ricordarsi di deallocare la lista prima di uscire dal main (e verificare con
valgrind che questo sia avvenuto, vedi note).
*/


/*
11) Scrivere un programma che, creata una lista di interi come
nell'esercizio precedente, calcoli e stampi la sua lunghezza percorrendo la
lista dall'inizio alla fine.
Ricordarsi di deallocare la lista prima di uscire dal main (e verificare con
valgrind che questo sia avvenuto, vedi note).
*/