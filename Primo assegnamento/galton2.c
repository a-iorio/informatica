#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void row(int, int, int, int); 
void box(int, int);
void galton2(int);

int main(){
	
	/* chiedo il numero ed effettuo un rapido check */
	int n;
	do{
		printf("Inserisci un intero positivo minore di 9: ");
		scanf("%d", &n);
	}
	while(n<0 || n>8);

	galton2(n);

}

/* 
Funzione Galton richiesta dall'assegnamento.
*/

void galton2(int n){

	/* inizializzo variabili e seme */
	int i, init=0, casual, position;
	srand(time(NULL));
	n++;

	/* header */
	row(n, 0, 0, 0);
	row(n, 1, 0, 0);

	box(n, 4);

	/* body */
	for(i=0; i<n; i++){ /* ciclo per le righe */

		if(init==0){
 			position = n; /* setto la posizione iniziale */
 			init = 1;
 		}
 		else{
 			if(i!=n-1){ 
 				casual = ((rand() % 2)); /* scelgo casual tra 0 ed 1 e se casual è nullo lo imposto a -1 */
 				if(casual==0) casual--;
 				position = position + casual;
 			}
 			else{
 				if(position % 2 == 0){ /* evito che la pallina cada in un divisore */
 					casual = ((rand() % 2));
 					if(casual==0) casual--;
 					position = position + casual;
 				}
 			}
 		}

		row(n, 2, i, position); /* stampo la riga */
	}

	/* footer */
	row(n, 1, 0, 0);
	row(n, 0, 0, 0);

	return;
}

/* 
Funzione che stampa il box di r-righe iniziale.
*/

void box(int n, int r){
	int i, j;

	for(i=0; i<=r; i++){ /* ciclo per righe */
		if(i==0){ /* stampo inizio contenitore */
			printf("  ");
			for(j=0; j<2*n+1; j++){printf("_");}
			printf("\n");
		}
		else if(i==r-3){ /* stampo riga con palline residue */
			printf("  |");
			for(j=0; j<2*n-1; j++){
				if((rand() % 3) == 0) printf("o");
				else printf(" ");
			}
			printf("|\n");
		}
		else if(i>r-3 && i<r){ /* stampo righe con tutte palline */
			printf("  |");
			for(j=0; j<2*n-1; j++){printf("o");}
			printf("|\n");
		}
		else if(i==r){ /* stampo fine contenitore */
			printf("  ");
			for(j=0; j<n-1; j++){printf("‾");}
			printf("|o|");
			for(j=0; j<n-1; j++){printf("‾");}
		}
		else{ /* stampo righe vuote */
			printf("  |");
			for(j=0; j<2*n-1; j++){printf(" ");}
			printf("|\n");
		}

		}
		/* stampo pallina che cade */
		printf("\n\n");
		for(j=0; j<n+2; j++){printf(" ");}
		printf("o\n\n");
}

/* 
Funzione che stampa ogni riga nel modo opportuno.
I parametri accettati dalla row sono: 
- [n] n+1 il numero di righe da stampare;
- [type] il tipo di riga (0, 1 per stampare l'header 2 per stampare il body);
- [row] il numero di riga in cui ci si trova (da 0-n in body);
- [position] la posizione attuale in cui si trova la pallina.
Nota: [row] e [position] saranno ininfluenti in header e footer e sarà assegnato il valore 0. 
*/

void row(int n, int type, int row, int position){ 
	int i, space=0, set=0;
	switch(type){
		case 0: /* riga numerazione */
			printf("...");
			for(i=1; i<n+1; i++){
				printf("%d.", i);
			}
			break;

		case 1:	/* riga divisore */
			for(i=1; i<=2*n+3; i++){
				printf("=");
			}
			break;

		case 2: /* riga pallina */
			printf(" %d|", row);
			if(row != n-1){ /* se non siamo nell'ultima riga stampo la riga con la pallina al posizione corrente */

				for(i=1; i<=2*n; i++){
					if(i!=2*n && i!=position){ /* stampo le righe con i chiodini */
						if(row!=0){
							if((i<=n-row || i>=n+row)) printf(" ");
							else{ /* se lo spazio=0, stampo il chiodino e attivo lo spazio */
								if(space==0){
									printf(".");
									space = 1;
								}
								else{ /* se lo spazio è attivo, lo stampo e ritorno come prima  */
									printf(" ");
									space = 0;
								}
							}
						}
						else{ /* se siamo nella prima riga nessun chiodino */
							if((i<position || i > position)) printf(" ");
						}
					}
					if(i==position){ /* se siamo nella posizione della pallina */
						printf("o"); /* la stampo e attivo lo spazio solo se la pallina non cade prima di ogni chiodino */
						if(space==0 && set==1){ space = 1; }
						else{ space = 0; }
						set = 1;
					}
					if(i==2*n) printf("|");
				}
			}
			else{ /* se siamo nell'ultima riga stampo i divisori */
				for(i=1; i<=2*n; i++){
					if(i % 2 == 0) printf("|");
					else{
						if(i==position) printf("o");
						else printf(" ");
					}
				}
			}
	}

	printf("\n");
	return;	
}