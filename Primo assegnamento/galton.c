/** 
\file galton.c
\author Andrea Iorio
Si dichiara che il contenuto di questo file e' in ogni sua parte opera originale dell' autore.  */

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void row(int, int, int, int); 
void galton(int);


int main(){
	
	/* chiedo il numero ed effettuo un rapido check */
	int n;
	do{
		printf("Inserisci un intero positivo minore di 9: ");
		scanf("%d", &n);
	}
	while(n<0 || n>8);

	galton(n);

}


/* Funzione Galton richiesta dall'assegnamento. */

void galton(int n){

	/* inizializzo variabili e seme */
	int i, init=0, casual, position;
	srand(time(NULL));
	n++;

	/* header */
	row(n, 0, 0, 0); 
	row(n, 1, 0, 0);

	/* body */
	for(i=0; i<n; i++){ /* ciclo per le righe */

		if(init==0){ /* setto la posizione iniziale */
 			position = n; 
 			init = 1;
 		}
 		else{
 			if(i!=n-1){ /* se siamo in una qualunque riga tranne che nell'ultima */
 				casual = ((rand() % 2)); /* scelgo casual tra 0 ed 1 e se casual è nullo lo imposto a -1 */
 				if(casual==0) casual--;
 				position = position + casual; /* e aggiorno la posizione scalandola dunque di 1 */
 			}
 			else{
 				if(position % 2 == 0){ /* se siamo nell'ultima riga evito che la pallina cada in un divisore */
 					casual = ((rand() % 2));
 					if(casual==0) casual--;
 					position = position + casual;
 				}
 			}
 		}

		row(n, 2, i, position); /* stampo la riga */
	}

	/* footer */
	row(n, 1, 0, 0);
	row(n, 0, 0, 0);

	return;
}

/* 
Funzione che stampa ogni riga nel modo opportuno.
I parametri accettati dalla row sono: 
- [n] n+1 il numero di righe da stampare;
- [type] il tipo di riga (0, 1 per stampare l'header 2 per stampare il body);
- [row] il numero di riga in cui ci si trova (da 0-n in body);
- [position] la posizione attuale in cui si trova la pallina.
Nota: [row] e [position] saranno ininfluenti in header e footer e sarà assegnato il valore 0. 
*/

void row(int n, int type, int row, int position){ 
	int i;
	switch(type){
		case 0: /* riga numerazione */
			printf("...");
			for(i=1; i<n+1; i++){
				printf("%d.", i);
			}
			break;

		case 1:	 /* riga divisore */
			for(i=1; i<=2*n+3; i++){
				printf("=");
			}
			break;

		case 2: /* riga pallina */
			printf(" %d|", row);
			if(row != n-1){ /* se non siamo nell'ultima riga stampo la riga con la pallina al posizione corrente */
				for(i=1; i<=2*n; i++){
					if((i<position || i > position) && i!=2*n) printf(".");
					if(i==position) printf("o");
					if(i==2*n) printf("|");
				}
			}
			else{ /* se siamo nell'ultima riga stampo i divisori */
				for(i=1; i<=2*n; i++){
					if(i % 2 == 0) printf("|");
					else{
						if(i==position) printf("o");
						else printf(" ");
					}
				}
			}
	}

	printf("\n");
	return;	
}
